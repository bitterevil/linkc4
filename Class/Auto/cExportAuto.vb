﻿Imports System.Text
Public Class cExportAuto

    Public Sub C_CALxProcess()

        Try
            W_GENxCrtMapping()
            Dim tW_DateExport As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim oExpData As New wExportData
            Dim oExpSale As New wExportSale
            Dim oExpCst As New wExportCst
            Dim oExpGrpCst As New wExportGrpCst
            Dim oUrsGP As New cUrsGP
            oExpData.C_CALxFindTable()
            wSettingEDC.W_GENxTableMapping()
            oUrsGP.C_GENxTableMapping()

            oExpSale.W_SEToColumnSale()  'สร้าง Column Sale
            oExpSale.W_SEToColumnReturn() 'สร้าง Column Return
            oExpSale.W_GETxDataSale(Date.Now)  'Load Column Sale
            oExpSale.W_GETxDataReturn(Date.Now) 'Load Column Return
            oExpSale.W_EXPxExportDate(Date.Now)  'Export Process
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success

            oExpCst.W_SEToColumnCustomer()  'สร้าง Column ลูกค้า
            oExpCst.W_GETdataGridCst("")    'โหลดข้อมูลลูกค้าลง Grid
            oExpCst.W_EXPxExportDataCustomer() 'Export ข้อมูลลูกค้า
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success

            oExpGrpCst.W_SEToColumnGroupCustomer()  'สร้าง Column กลุ่มลูกค้า
            oExpGrpCst.W_GETdataGridGrpCst("")  'โหลดข้อมูลกลุ่มลูกค้าลง Grid
            oExpGrpCst.W_EXPxExportDataGroupCustomer() 'Export ข้อมูลกลุ่มลูกค้า
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success

            'oExp.W_SEToColumnPaymentSale()
            'oExp.W_SEToColumnPaymentReturn()

            'oExp.W_GETDataGrid(tW_DateExport)
            'oExp.W_GETDataGridReturn(tW_DateExport)

            'Dim oExp As New wExports
            'oExp.W_EXPxProcessApi()
            'oExp.W_SETxWriteSuccess()

            wExportData.nC_CreateFileCst = 0
            wExportData.nC_CreateFileGrp = 0
            wExportData.nC_CreateFileSale = 0

        Catch ex As Exception

        End Try
    End Sub

    Public Sub C_CALxFindTable()
        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtRunning') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TCNTPdtRunning]")
            oSQL.AppendLine("([FNNum] [int] IDENTITY(1,1) NOT NULL,")
            oSQL.AppendLine("[FTRunningNumber] [varchar](50) NULL")
            oSQL.AppendLine(")")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)


        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_GENxCrtMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingBranch') AND type in (N'U'))")
            oSQL.AppendLine(" BEGIN ")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode] [VARCHAR](50) NULL,")
            oSQL.AppendLine(" [FTBchName] [VARCHAR](150) NULL,")
            oSQL.AppendLine(" [FTDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingBranch] ")
            oSQL.AppendLine(" ([FTBchCode], ")
            oSQL.AppendLine(" [FTBchName],")
            oSQL.AppendLine(" [FTDefaultValue],")
            oSQL.AppendLine(" [FTUrsValue]) ")
            oSQL.AppendLine(" SELECT FTBchCode, FTBchName, ")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02'")
            oSQL.AppendLine(" ELSE '01' END) AS FTDefaultValue ,")
            oSQL.AppendLine(" (CASE  WHEN FTBchCode = '001'  THEN '01' ")
            oSQL.AppendLine(" WHEN FTBchCode = '002' THEN '03' ")
            oSQL.AppendLine(" WHEN FTBchCode = '003' THEN '04' ")
            oSQL.AppendLine(" WHEN FTBchCode = '004' THEN '02' ")
            oSQL.AppendLine(" ELSE '01' END) AS FTUrsValue ")
            oSQL.AppendLine(" FROM TCNMBranch")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)

            W_GenxTriggerforBranch()
            'W_GENxCrtPayMapping()

        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Create Table การชำระเงิน
    ''' </summary>
    Public Sub W_GENxCrtPayMapping()
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oSQL As New StringBuilder
            oSQL.Clear()
            oSQL.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects ")
            oSQL.AppendLine(" WHERE object_id = OBJECT_ID(N'TLNKMappingPayment') AND type in (N'U')) ")
            oSQL.AppendLine(" BEGIN")
            oSQL.AppendLine(" CREATE TABLE [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode] [VARCHAR](50) NULL, ")
            oSQL.AppendLine(" [FTRcvName] [VARCHAR](150) NULL, ")
            oSQL.AppendLine(" [FTPayDefaultValue] [VARCHAR](20) NULL,")
            oSQL.AppendLine(" [FTPayUrsValue] [VARCHAR](20) NULL) ")
            oSQL.AppendLine(" INSERT [dbo].[TLNKMappingPayment] ")
            oSQL.AppendLine(" ([FTRcvCode],[FTRcvName],[FTPayDefaultValue],[FTPayUrsValue]) ")
            oSQL.AppendLine(" SELECT FTRcvCode, FTRcvName,")
            oSQL.AppendLine(" (CASE WHEN FTRcvCode = '001' THEN 'CASH' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT' ")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER' ")
            oSQL.AppendLine("  ELSE '' End) As FTPayDefaultValue,")
            oSQL.AppendLine("  (CASE   WHEN FTRcvCode = '001' THEN 'CASH'")
            oSQL.AppendLine("  WHEN FTRcvCode = '002' THEN 'CREDIT'")
            oSQL.AppendLine("  WHEN FTRcvCode = '005' THEN 'TRANSFER'")
            oSQL.AppendLine("  ELSE '' End) As FTPayUrsValue FROM TPSMRcv")
            oSQL.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_GenxTriggerforBranch()
        Dim oSql As New StringBuilder()
        Dim oDatabase As New cDatabaseLocal()
        Try
            oSql.AppendLine(" IF NOT EXISTS (SELECT * FROM sys.objects ")
            oSql.AppendLine(" WHERE object_id = OBJECT_ID(N'UpdateTblBranchmapping') AND type in (N'U')) ")
            oSql.AppendLine(" CREATE TRIGGER [dbo].[UpdateTblBranchmapping] ")
            oSql.AppendLine(" ON  [dbo].[TCNMBranch] ")
            oSql.AppendLine(" AFTER UPDATE ")
            oSql.AppendLine(" AS")
            oSql.AppendLine(" BEGIN ")
            oSql.AppendLine(" SET NOCOUNT ON; ")
            oSql.AppendLine(" DECLARE @FTBchCode VARCHAR(5) ")
            oSql.AppendLine(" DECLARE @FTBchName VARCHAR(100) ")
            oSql.AppendLine(" SELECT @FTBchCode = INSERTED.FTBchCode , @FTBchName = INSERTED.FTBchName ")
            oSql.AppendLine(" FROM INSERTED ")
            oSql.AppendLine(" UPDATE TLNKMappingBranch WITH(ROWLOCK) ")
            oSql.AppendLine(" SET TLNKMappingBranch.FTBchCode = @FTBchCode, ")
            oSql.AppendLine(" TLNKMappingBranch.FTBchName = @FTBchName ")
            oSql.AppendLine(" WHERE (TLNKMappingBranch.FTBchCode = @FTBchCode) ")
            oSql.AppendLine(" END ")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString)
        Catch ex As Exception
        End Try
    End Sub

End Class
