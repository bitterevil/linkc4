﻿Imports System.Text

Public Class wEditEDC
    Private Sub ocmSave_Click(sender As Object, e As EventArgs) Handles ocmSave.Click

        Dim tValue As String = ""
        If ordCrd1.Checked Then
            tValue = ordCrd1.Text
        ElseIf ordCrd2.Checked Then
            tValue = ordCrd2.Text
        ElseIf ordCrd3.Checked Then
            tValue = ordCrd3.Text
        ElseIf ordCrdOther.Checked Then
            tValue = ordCrdOther.Text
        End If
        If W_DATbSaveExport(tValue) = True Then
            Me.DialogResult = Windows.Forms.DialogResult.OK
        Else
            If cCNVB.nVB_CutLng = 1 Then
                MsgBox(tValue + " รายการนี้ถูกใช้งานไปแล้ว", MsgBoxStyle.Information)
            Else
                MsgBox(tValue + " has been used", MsgBoxStyle.Information)
            End If

        End If

    End Sub

    Public Function W_DATbSaveExport(ByVal ptMappingVal As String) As Boolean
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Dim oDbTbl As New DataTable
        Try
            oSql = New StringBuilder()
            oSql.AppendLine(" SELECT TOP(1)  FTEdcCode, FTBnkCode, FTBnkName, FTBnkMapping ")
            oSql.AppendLine(" FROM    TLNKMappingEDC")
            oSql.AppendLine(" WHERE FTBnkMapping = '" + ptMappingVal + "' ")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString())
            If oDbTbl.Rows.Count > 0 Then
                If oDbTbl.Rows(0).Item("FTBnkMapping") = olaEDC.Text Then
                    Return True
                ElseIf oDbTbl.Rows(0).Item("FTBnkMapping") <> "Othersum" Then
                    Return False

                End If

            End If

            oSql = New StringBuilder()
            oSql.AppendLine(" UPDATE TLNKMappingEDC")
            oSql.AppendLine(" SET ")
            oSql.AppendLine(" FTBnkMapping = '" + ptMappingVal + "' ")
            oSql.AppendLine(" WHERE  FTEdcCode = '" + olaKey.Text + "' ")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
            Return True

        Catch ex As Exception

        End Try
    End Function

    Private Sub wEditEDC_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub
End Class