﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wSettingSharing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ListViewItem1 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"GP"}, 0, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte)))
        Dim ListViewItem2 As System.Windows.Forms.ListViewItem = New System.Windows.Forms.ListViewItem(New String() {"Sharing Code"}, 0, System.Drawing.Color.Empty, System.Drawing.Color.Empty, New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte)))
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wSettingSharing))
        Me.opnFormDes = New System.Windows.Forms.Panel()
        Me.olaDesForm = New System.Windows.Forms.Label()
        Me.opnright = New System.Windows.Forms.Panel()
        Me.opnbottom = New System.Windows.Forms.Panel()
        Me.opnButton = New System.Windows.Forms.Panel()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaTitle = New System.Windows.Forms.Label()
        Me.opnLeft = New System.Windows.Forms.Panel()
        Me.olvMenu = New System.Windows.Forms.ListView()
        Me.oimForm = New System.Windows.Forms.ImageList(Me.components)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.opnTop = New System.Windows.Forms.Panel()
        Me.opnForm = New System.Windows.Forms.Panel()
        Me.opnFormDes.SuspendLayout()
        Me.opnbottom.SuspendLayout()
        Me.opnButton.SuspendLayout()
        Me.opnLeft.SuspendLayout()
        Me.opnTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnFormDes
        '
        Me.opnFormDes.BackColor = System.Drawing.Color.Transparent
        Me.opnFormDes.Controls.Add(Me.olaDesForm)
        Me.opnFormDes.Dock = System.Windows.Forms.DockStyle.Top
        Me.opnFormDes.Location = New System.Drawing.Point(180, 65)
        Me.opnFormDes.Name = "opnFormDes"
        Me.opnFormDes.Size = New System.Drawing.Size(847, 34)
        Me.opnFormDes.TabIndex = 11
        '
        'olaDesForm
        '
        Me.olaDesForm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaDesForm.Location = New System.Drawing.Point(14, 3)
        Me.olaDesForm.Name = "olaDesForm"
        Me.olaDesForm.Size = New System.Drawing.Size(455, 31)
        Me.olaDesForm.TabIndex = 0
        '
        'opnright
        '
        Me.opnright.BackColor = System.Drawing.Color.Transparent
        Me.opnright.Dock = System.Windows.Forms.DockStyle.Right
        Me.opnright.Location = New System.Drawing.Point(1027, 65)
        Me.opnright.Name = "opnright"
        Me.opnright.Size = New System.Drawing.Size(20, 511)
        Me.opnright.TabIndex = 13
        '
        'opnbottom
        '
        Me.opnbottom.BackColor = System.Drawing.Color.Transparent
        Me.opnbottom.Controls.Add(Me.opnButton)
        Me.opnbottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.opnbottom.Location = New System.Drawing.Point(180, 576)
        Me.opnbottom.Name = "opnbottom"
        Me.opnbottom.Size = New System.Drawing.Size(867, 77)
        Me.opnbottom.TabIndex = 14
        '
        'opnButton
        '
        Me.opnButton.Controls.Add(Me.ocmClose)
        Me.opnButton.Dock = System.Windows.Forms.DockStyle.Right
        Me.opnButton.Location = New System.Drawing.Point(605, 0)
        Me.opnButton.Name = "opnButton"
        Me.opnButton.Size = New System.Drawing.Size(262, 77)
        Me.opnButton.TabIndex = 1
        '
        'ocmClose
        '
        Me.ocmClose.DialogResult = System.Windows.Forms.DialogResult.No
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(106, 23)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(136, 32)
        Me.ocmClose.TabIndex = 1
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaTitle
        '
        Me.olaTitle.AutoSize = True
        Me.olaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olaTitle.Location = New System.Drawing.Point(17, 18)
        Me.olaTitle.Name = "olaTitle"
        Me.olaTitle.Size = New System.Drawing.Size(251, 20)
        Me.olaTitle.TabIndex = 0
        Me.olaTitle.Tag = "2;ตั้งค่า การส่งออกข้อมูล GP Sharing; Setting GP Sharing for export"
        Me.olaTitle.Text = "ตั้งค่า การส่งออกข้อมูล GP Sharing"
        '
        'opnLeft
        '
        Me.opnLeft.BackColor = System.Drawing.Color.Transparent
        Me.opnLeft.Controls.Add(Me.olvMenu)
        Me.opnLeft.Controls.Add(Me.Panel2)
        Me.opnLeft.Controls.Add(Me.Panel1)
        Me.opnLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.opnLeft.Location = New System.Drawing.Point(0, 65)
        Me.opnLeft.Name = "opnLeft"
        Me.opnLeft.Size = New System.Drawing.Size(180, 588)
        Me.opnLeft.TabIndex = 9
        '
        'olvMenu
        '
        Me.olvMenu.Dock = System.Windows.Forms.DockStyle.Fill
        Me.olvMenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.olvMenu.Items.AddRange(New System.Windows.Forms.ListViewItem() {ListViewItem1, ListViewItem2})
        Me.olvMenu.Location = New System.Drawing.Point(24, 0)
        Me.olvMenu.Margin = New System.Windows.Forms.Padding(0, 2, 2, 2)
        Me.olvMenu.MultiSelect = False
        Me.olvMenu.Name = "olvMenu"
        Me.olvMenu.Size = New System.Drawing.Size(156, 511)
        Me.olvMenu.SmallImageList = Me.oimForm
        Me.olvMenu.TabIndex = 13
        Me.olvMenu.UseCompatibleStateImageBehavior = False
        Me.olvMenu.View = System.Windows.Forms.View.List
        '
        'oimForm
        '
        Me.oimForm.ImageStream = CType(resources.GetObject("oimForm.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.oimForm.TransparentColor = System.Drawing.Color.Transparent
        Me.oimForm.Images.SetKeyName(0, "Properties-icon.png")
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(24, 511)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(156, 77)
        Me.Panel2.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(24, 588)
        Me.Panel1.TabIndex = 0
        '
        'opnTop
        '
        Me.opnTop.BackColor = System.Drawing.Color.Transparent
        Me.opnTop.Controls.Add(Me.olaTitle)
        Me.opnTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.opnTop.Location = New System.Drawing.Point(0, 0)
        Me.opnTop.Name = "opnTop"
        Me.opnTop.Size = New System.Drawing.Size(1047, 65)
        Me.opnTop.TabIndex = 8
        '
        'opnForm
        '
        Me.opnForm.BackColor = System.Drawing.Color.Transparent
        Me.opnForm.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnForm.Location = New System.Drawing.Point(180, 99)
        Me.opnForm.Margin = New System.Windows.Forms.Padding(0)
        Me.opnForm.Name = "opnForm"
        Me.opnForm.Size = New System.Drawing.Size(847, 477)
        Me.opnForm.TabIndex = 12
        '
        'wSettingSharing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1047, 653)
        Me.Controls.Add(Me.opnForm)
        Me.Controls.Add(Me.opnFormDes)
        Me.Controls.Add(Me.opnright)
        Me.Controls.Add(Me.opnbottom)
        Me.Controls.Add(Me.opnLeft)
        Me.Controls.Add(Me.opnTop)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wSettingSharing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "GP Sharing"
        Me.opnFormDes.ResumeLayout(False)
        Me.opnbottom.ResumeLayout(False)
        Me.opnButton.ResumeLayout(False)
        Me.opnLeft.ResumeLayout(False)
        Me.opnTop.ResumeLayout(False)
        Me.opnTop.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents opnFormDes As Panel
    Friend WithEvents olaDesForm As Label
    Friend WithEvents opnright As Panel
    Friend WithEvents opnbottom As Panel
    Friend WithEvents opnButton As Panel
    Friend WithEvents ocmClose As Button
    Friend WithEvents olaTitle As Label
    Friend WithEvents opnLeft As Panel
    Friend WithEvents opnTop As Panel
    Friend WithEvents oimForm As ImageList
    Friend WithEvents Panel1 As Panel
    Friend WithEvents opnForm As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents olvMenu As ListView
End Class
