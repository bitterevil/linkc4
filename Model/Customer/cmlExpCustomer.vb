﻿Imports Newtonsoft.Json
Public Class cmlExpCustomer

    <JsonProperty("first_name")>
    Property tFirstName As String

    <JsonProperty("last_name")>
    Property tLastname As String

    <JsonProperty("phone")>
    Property tPhone As String

    <JsonProperty("email_id")>
    Property tEmailID As String

    <JsonProperty("fax")>
    Property tFax As String

    <JsonProperty("mobile_phone")>
    Property tMoblile As String

    <JsonProperty("primary_currency")>
    Property tPriCurent As String

    <JsonProperty("customer_id")>
    Property tCusID As String

    <JsonProperty("idcard_no")>
    Property tIDCard As String

    <JsonProperty("valid_date")>
    Property tValidDate As String

    <JsonProperty("branch_code")>
    Property tBchCode As String

    <JsonProperty("gender")>
    Property tGender As String

    <JsonProperty("birthday")>
    Property tBirthDay As String

    <JsonProperty("member_point")>
    Property tMemberPoint As String

    <JsonProperty("member_class")>
    Property tMemberClass As String

    <JsonProperty("memberdetail")>
    Property oMemberDetail() As List(Of cmlExpCustomerInfo)
End Class
