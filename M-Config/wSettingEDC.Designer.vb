﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wSettingEDC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wSettingEDC))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.olaTitle = New System.Windows.Forms.Label()
        Me.ogbMain = New System.Windows.Forms.GroupBox()
        Me.ogdEDC = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.opnMain.SuspendLayout()
        Me.ogbMain.SuspendLayout()
        CType(Me.ogdEDC, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 3
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.354753!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 94.64525!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.opnMain.Controls.Add(Me.olaTitle, 1, 1)
        Me.opnMain.Controls.Add(Me.ogbMain, 1, 2)
        Me.opnMain.Controls.Add(Me.Panel1, 1, 3)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 6
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 236.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.116385!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 253.0!))
        Me.opnMain.Size = New System.Drawing.Size(858, 348)
        Me.opnMain.TabIndex = 0
        '
        'olaTitle
        '
        Me.olaTitle.AutoSize = True
        Me.olaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaTitle.Location = New System.Drawing.Point(48, 20)
        Me.olaTitle.Name = "olaTitle"
        Me.olaTitle.Size = New System.Drawing.Size(348, 20)
        Me.olaTitle.TabIndex = 0
        Me.olaTitle.Tag = "2;ตั้งค่าการส่งออกข้อมูลการรับชำระผ่านเครื่องรูดบัตร;Setting Information EDC for " &
    "export"
        Me.olaTitle.Text = "ตั้งค่าการส่งออกข้อมูลการรับชำระผ่านเครื่องรูดบัตร"
        '
        'ogbMain
        '
        Me.ogbMain.Controls.Add(Me.ogdEDC)
        Me.ogbMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbMain.Location = New System.Drawing.Point(48, 57)
        Me.ogbMain.Name = "ogbMain"
        Me.ogbMain.Size = New System.Drawing.Size(761, 222)
        Me.ogbMain.TabIndex = 1
        Me.ogbMain.TabStop = False
        Me.ogbMain.Tag = "2;เครื่องรูดบัตร;EDC"
        Me.ogbMain.Text = "เครื่องรูดบัตร"
        '
        'ogdEDC
        '
        Me.ogdEDC.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdEDC.Location = New System.Drawing.Point(14, 22)
        Me.ogdEDC.Name = "ogdEDC"
        Me.ogdEDC.RowTemplate.Height = 24
        Me.ogdEDC.Size = New System.Drawing.Size(741, 180)
        Me.ogdEDC.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ocmClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(48, 293)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(796, 41)
        Me.Panel1.TabIndex = 2
        '
        'ocmClose
        '
        Me.ocmClose.Location = New System.Drawing.Point(644, 3)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(117, 30)
        Me.ocmClose.TabIndex = 1
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "ปิด"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'wSettingEDC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(858, 348)
        Me.Controls.Add(Me.opnMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wSettingEDC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;เครื่องรูดบัตร;EDC"
        Me.Text = "เครื่องรูดบัตร"
        Me.opnMain.ResumeLayout(False)
        Me.opnMain.PerformLayout()
        Me.ogbMain.ResumeLayout(False)
        CType(Me.ogdEDC, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents olaTitle As Label
    Friend WithEvents ogbMain As GroupBox
    Friend WithEvents ogdEDC As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ocmClose As Button
End Class
