﻿Option Explicit On

Public Class cCNVB
    Public Shared tVB_CNAppTitle As String
    'app
    Public Shared tVB_AppTitle As String              'program name + program version

    'cutting
    Public Shared nVB_CutLng As Integer               'cut language Tha=1, Eng=2
    Public Shared nVB_CutMsg As Integer               'cut message Tha=1, Eng=2
    Public Shared nVB_CutTag As Integer               'cut tag
    Public Shared nVB_CutConst As Integer            'cut constant

    Public Shared tVB_CNPwdAdaUsrP As String 'password user

    Public Shared tVB_AppProgram As String 'Name of Application เปลี่ยนชื่อโปรแกรม [Ake 12/06/2012]
    Public Shared tVB_AppVersion As String 'Version of Application [Ake 12/06/2012]

    Public Shared oVB_PdtReuse As List(Of cPdtReuse) 'Product Reuse '*CH 25-11-2014
    'Public Shared oVB_PdtMaster As List(Of cPdtMaster) 'Product Master '*CH 26-11-2014
    Public Shared tVB_CompName As String = Environment.MachineName 'Computer Name '*CH 26-11-2014

    '*CH 05-12-2014
    Public Shared bVB_Auto As Boolean = False
    Public Shared tVB_FmtCondition As String = "and branch {0} to {1}"
    Public Shared tVB_FmtConditionThai As String = " และ สาขา {0} ถึง สาขา {1}"
    Public Shared tVB_FmtConditionSel As String = " and branch {0}"
    Public Shared tVB_FmtConditionSelThai As String = " และ สาขา {0}"
    Public Shared tVB_GrpCondition As String = "เงื่อนไขสาขา;Condition branch"
    Public Shared tVB_GrpCode As String = "สาขา;Branch"
    Public Shared tVB_CaptionFilter As String = "Code,Name"
    Public Shared tVB_CaptionFilterThai As String = "รหัส,ชื่อ"

    '*CH 08-12-2014 Status Check Doc not complete
    Public Shared bVB_DocNotComplete As Boolean = False
    Public Shared tVB_MsgExportSale As String = ""

    '*CH 09-12-2014 Status Export Error
    Public Shared bVB_StaCheckExpContinue As Boolean = False
    Public Shared bVB_ExportContinue As Boolean = False
    Public Shared tVB_CfgAlwCalCpn As String = ""

    Public Shared tVB_PurgeDay As String = "30" 'Purge '*CH 20-01-2015
    Public Shared tVB_UserCode As String = "" '*CH 18-07-2017
    Public Shared tVB_UserName As String = "AdaLinkBCC"
    Public Shared tVB_PathInBox As String = ""
    Public Shared tVB_PathBackUp As String = ""
    Public Shared tVB_PathLog As String = ""
    Public Shared tVB_PathOutBox As String = ""
    Public Shared bVB_Chk As Boolean = False
    Public Shared bVB_ChkSave As Boolean = False
    Public Shared tVB_PathList As String = ""
    Public Shared oDBInst As DataTable
    Public Shared tVB_Export As String = ""
    Public Shared tVB_Pronocode As String = ""
    Public Shared tVB_Pronocode2 As String = ""
    Public Shared tVB_Pronocode3 As String = ""
    Public Shared tVB_PriType As String = ""

    Public Shared oDTTCNMPdt As New DataTable
    Public Shared oDTOPrice As New DataTable
    Public Shared oDBBchMap As New DataTable
    '*EYE 59-03-01 number line in temp TLNKPdt
    Public Shared nStkRuning As Integer = 0
    '-------PAN 2017-03-28 FullFileList
    Public Shared tVB_FullFileList As String = ""
    Public Shared nVB_Line As Integer = 0
    Public Shared tVB_PathControl As String = ""
    Public Shared tVB_CountNo As String = "0"
    Public Shared oDBGrp As New DataTable
    Public Shared bVB_PdtData As Boolean = False

    Public Shared tVB_Log As String = ""

    Public Shared tVB_SuccessImp As String = "นำเข้าข้อมูลสำเร็จ"
    Public Shared bVB_BchHQ As Boolean = False
    Public Shared tVB_BchCode As String = ""
    Public Shared cVB_CmpVatAmt As Double = 0.0

    'Export
    Public Shared tVB_ZSDINT005 As String = "ZSDINT005" 'Sale
    Public Shared tVB_ZSDINT006 As String = "ZSDINT006" 'Reconcile
    ''' <summary>Expense</summary>
    Public Shared tVB_ZGLINT001 As String = "ZGLINT001"
    ''' <summary>Cash Transfer</summary>
    Public Shared tVB_ZGLINT002 As String = "ZGLINT002"

    'Pos Open
    Public Shared oVB_PosCode As New List(Of String)

    'Mapping
    Public Shared tVB_StoreNo As String = ""
    Public Shared tVB_SaleCode As String = ""
    Public Shared tVB_ReturnCode As String = ""
    Public Shared tVB_SaleItemCode As String = ""
    Public Shared tVB_SaleFreeItemCode As String = ""
    Public Shared tVB_SaleServiceItemCode As String = ""
    Public Shared tVB_ReturnItemCode As String = ""
    Public Shared tVB_ReturnFreeItemCode As String = ""
    Public Shared tVB_ReturnServiceItemCode As String = ""
    Public Shared nVB_DecShw As Integer = 2
    Public Shared nVB_PurgeDay As Integer = 30

    Public Shared tVB_FTPthDocNo As String = ""   ' เลขที่เอกสาร Tnf In
    Public Shared nVB_CNPdtSet As Integer = 0   ' รับเข้าสินค้าชุด

    Public Shared nVB_StaImportFailed = 0    ' ตรวจสอบสถานะข้อผิดขณะ import

    Public Shared nVB_ImportPdt = 0
    Public Shared nVB_ImportAjp = 0
    Public Shared nVB_ImportTnf = 0

End Class
