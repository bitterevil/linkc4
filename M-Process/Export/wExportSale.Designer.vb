﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wExportSale
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExportSale))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ocmSelect = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ogbSale = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.odpDate = New System.Windows.Forms.DateTimePicker()
        Me.olaSale = New System.Windows.Forms.Label()
        Me.ocmSearch = New System.Windows.Forms.Button()
        Me.otcMain = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ogdSale = New System.Windows.Forms.DataGridView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ogdReturn = New System.Windows.Forms.DataGridView()
        Me.opnMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ogbSale.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.otcMain.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.ogdSale, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.ogdReturn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 3
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.4!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.6!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.opnMain.Controls.Add(Me.Panel1, 1, 3)
        Me.opnMain.Controls.Add(Me.ogbSale, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 4
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 335.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.opnMain.Size = New System.Drawing.Size(1082, 444)
        Me.opnMain.TabIndex = 2
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ocmSelect)
        Me.Panel1.Controls.Add(Me.ocmClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(28, 355)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1036, 86)
        Me.Panel1.TabIndex = 1
        '
        'ocmSelect
        '
        Me.ocmSelect.Location = New System.Drawing.Point(766, 20)
        Me.ocmSelect.Name = "ocmSelect"
        Me.ocmSelect.Size = New System.Drawing.Size(125, 34)
        Me.ocmSelect.TabIndex = 3
        Me.ocmSelect.Tag = "2;เลือก;Select"
        Me.ocmSelect.Text = "Select"
        Me.ocmSelect.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.Location = New System.Drawing.Point(897, 20)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(119, 34)
        Me.ocmClose.TabIndex = 2
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ogbSale
        '
        Me.ogbSale.Controls.Add(Me.TableLayoutPanel2)
        Me.ogbSale.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbSale.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbSale.Location = New System.Drawing.Point(28, 20)
        Me.ogbSale.Name = "ogbSale"
        Me.ogbSale.Size = New System.Drawing.Size(1036, 329)
        Me.ogbSale.TabIndex = 2
        Me.ogbSale.TabStop = False
        Me.ogbSale.Text = "เลือกข้อมูลการขาย/คืนที่ต้องการส่งออก"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.669759!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.33024!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel4, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.otcMain, 1, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 22)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 301.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1030, 304)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.odpDate)
        Me.Panel4.Controls.Add(Me.olaSale)
        Me.Panel4.Controls.Add(Me.ocmSearch)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(20, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1007, 41)
        Me.Panel4.TabIndex = 0
        '
        'odpDate
        '
        Me.odpDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odpDate.Location = New System.Drawing.Point(53, 7)
        Me.odpDate.Name = "odpDate"
        Me.odpDate.Size = New System.Drawing.Size(143, 26)
        Me.odpDate.TabIndex = 7
        '
        'olaSale
        '
        Me.olaSale.AutoSize = True
        Me.olaSale.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaSale.Location = New System.Drawing.Point(7, 9)
        Me.olaSale.Name = "olaSale"
        Me.olaSale.Size = New System.Drawing.Size(53, 20)
        Me.olaSale.TabIndex = 6
        Me.olaSale.Tag = "2;วันที่ :;Date :"
        Me.olaSale.Text = "วันที่ : "
        '
        'ocmSearch
        '
        Me.ocmSearch.Location = New System.Drawing.Point(202, 7)
        Me.ocmSearch.Name = "ocmSearch"
        Me.ocmSearch.Size = New System.Drawing.Size(104, 27)
        Me.ocmSearch.TabIndex = 5
        Me.ocmSearch.Text = "ค้นหา"
        Me.ocmSearch.UseVisualStyleBackColor = True
        '
        'otcMain
        '
        Me.otcMain.Controls.Add(Me.TabPage1)
        Me.otcMain.Controls.Add(Me.TabPage2)
        Me.otcMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.otcMain.Location = New System.Drawing.Point(20, 50)
        Me.otcMain.Name = "otcMain"
        Me.otcMain.SelectedIndex = 0
        Me.otcMain.Size = New System.Drawing.Size(1007, 250)
        Me.otcMain.TabIndex = 1
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.ogdSale)
        Me.TabPage1.Location = New System.Drawing.Point(4, 29)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(999, 217)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Tag = ";ขาย;Sale"
        Me.TabPage1.Text = "ขาย"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'ogdSale
        '
        Me.ogdSale.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdSale.Location = New System.Drawing.Point(3, 3)
        Me.ogdSale.Name = "ogdSale"
        Me.ogdSale.RowTemplate.Height = 24
        Me.ogdSale.Size = New System.Drawing.Size(990, 211)
        Me.ogdSale.TabIndex = 0
        Me.ogdSale.Tag = "2;ค้นหา;Search"
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.ogdReturn)
        Me.TabPage2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.TabPage2.Location = New System.Drawing.Point(4, 29)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(999, 217)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Tag = "2;คืน;Return"
        Me.TabPage2.Text = "คืน"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'ogdReturn
        '
        Me.ogdReturn.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdReturn.Location = New System.Drawing.Point(3, 3)
        Me.ogdReturn.Name = "ogdReturn"
        Me.ogdReturn.RowTemplate.Height = 24
        Me.ogdReturn.Size = New System.Drawing.Size(990, 211)
        Me.ogdReturn.TabIndex = 0
        '
        'wExportSale
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1082, 444)
        Me.Controls.Add(Me.opnMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExportSale"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออกข้อมูลขาย/คืน;Export Sale/Return"
        Me.Text = "ส่งออกข้อมูลขาย/คืน"
        Me.opnMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ogbSale.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.otcMain.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.ogdSale, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.ogdReturn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ocmClose As Button
    Friend WithEvents ogbSale As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents odpDate As DateTimePicker
    Friend WithEvents olaSale As Label
    Friend WithEvents ocmSearch As Button
    Friend WithEvents otcMain As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents ogdSale As DataGridView
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents ogdReturn As DataGridView
    Friend WithEvents ocmSelect As Button
End Class
