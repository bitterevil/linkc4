﻿Imports System.IO
Imports System.Text

Public Class cLog

    Private Shared dDateNow As Date = Date.Now

    Public Shared Sub C_CALxWriteLog(ptMsg As String)

        Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\AdaLog" 'เพิ่ม Folder AdaLog *CH 02-12-2014 

        If Not IO.Directory.Exists(tPathLog) Then
            IO.Directory.CreateDirectory(tPathLog)
        End If
        tPathLog &= "\" & Format(dDateNow, "yyyyMMdd") & "_" & My.Application.Info.ProductName.ToArray & ".txt"

        If File.Exists(tPathLog) = False Then
            Using oSw As StreamWriter = File.CreateText(tPathLog)
                oSw.Close()
            End Using
        End If

        Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
            With oStreamWriter
                .WriteLine(AdaConfig.cConfig.oApplication.tUser & " - " & Format(Date.Now, "HH:mm:ss.fff") & ": " & ptMsg)
                .Flush()
                .Close()
            End With
        End Using
    End Sub

    Public Shared Sub C_CALxWriteLogAuto(ptMsg As String)

        Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\AdaLog" 'เพิ่ม Folder AdaLog *CH 02-12-2014

        If Not IO.Directory.Exists(tPathLog) Then
            IO.Directory.CreateDirectory(tPathLog)
        End If
        tPathLog &= "\" & Format(dDateNow, "yyyyMMdd") & "_" & My.Application.Info.ProductName.ToArray & "_Auto.txt"

        If File.Exists(tPathLog) = False Then
            Using oSw As StreamWriter = File.CreateText(tPathLog)
                oSw.Close()
            End Using
        End If

        Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
            With oStreamWriter
                .WriteLine("Auto - " & Format(Date.Now, "HH:mm:ss.fff") & ": " & ptMsg)
                .Flush()
                .Close()
            End With
        End Using
    End Sub


    ''' <summary>
    ''' Write Log
    ''' </summary>
    ''' <param name="ptMsg">Message</param>
    ''' <param name="ptDocExp"> Document No</param>
    ''' <param name="ptExptype"> Type Export </param>
    ''' <param name="ptPoint"> Point for customer </param>
    Public Shared Sub C_CALxWriteLogExport(ptMsg As String, ptCode As String,
                                           ptDocExp As String, ptExptype As String,
                                           ByVal ptPoint As String)

        Try
            Dim oHDfile As New StringBuilder
            Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\Log\Process" 'เพิ่ม Folder AdaLog *CH 02-12-2014 
            Dim tSuccess As String = "1"
            Dim tFail As String = "0"
            Dim tRef As String = ""

            If Not IO.Directory.Exists(tPathLog) Then
                IO.Directory.CreateDirectory(tPathLog)
            End If

            If File.Exists(tPathLog) = False Then
                If wExportData.nC_CreateFileCst = 1 Or wExportData.nC_CreateFileGrp = 1 _
                    Or wExportData.nC_CreateFileSale = 1 Then

                    Select Case ptExptype
                        'Or "Return" Or "ReturnAll"
                        Case "Customer"
                            tPathLog &= "\" & "Member_" & Format(Date.Now, "yyyyMMdd") & "_" & Format(Date.Now, "HHmmss") & ".txt"
                        Case "GrpCustomer"
                            tPathLog &= "\" & "MemberGrp_" & Format(Date.Now, "yyyyMMdd") & "_" & Format(Date.Now, "HHmmss") & ".txt"
                    End Select


                    Using oSw As StreamWriter = File.CreateText(tPathLog)
                        oSw.Close()
                    End Using
                Else
                    Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log\Process")
                    Dim oGetFile As FileInfo() = tPath.GetFiles()

                    For Each oFile As FileInfo In oGetFile
                        tPathLog &= "\" & oFile.Name
                    Next

                End If
            End If

            Select Case ptExptype
                Case "Customer"
                    If wExportData.nC_CreateFileCst = 1 Then
                        oHDfile.Append(Environment.NewLine + Environment.NewLine)
                        oHDfile.Append("[Time]" + vbTab + vbTab)
                        oHDfile.Append("[CustomerCode]" + vbTab + vbTab)
                        oHDfile.Append("[Point]" + vbTab + vbTab)
                        oHDfile.Append("[Response Status]" + vbTab)
                        oHDfile.Append(Environment.NewLine)
                        oHDfile.Append(Format(Date.Now, "HH: mm : ss") + vbTab + vbTab)
                        oHDfile.Append(ptDocExp + vbTab + vbTab)
                        oHDfile.Append(ptPoint + vbTab + vbTab)
                        oHDfile.Append(ptCode)
                    Else
                        oHDfile.Append(Format(Date.Now, "HH: mm : ss") + vbTab + vbTab)
                        oHDfile.Append(ptDocExp + vbTab + vbTab)
                        oHDfile.Append(ptPoint + vbTab + vbTab)
                        oHDfile.Append(ptCode)
                    End If
                Case "GrpCustomer"
                    If wExportData.nC_CreateFileGrp = 1 Then
                        oHDfile.Append(Environment.NewLine + Environment.NewLine)
                        oHDfile.Append("[Time]" + vbTab + vbTab)
                        oHDfile.Append("[CustomerGroup]" + vbTab + vbTab)
                        oHDfile.Append("[Response Status]" + vbTab + vbTab)
                        oHDfile.Append(Environment.NewLine)
                        oHDfile.Append(Format(Date.Now, "HH:mm:ss") + vbTab + vbTab)
                        oHDfile.Append(ptDocExp + vbTab + vbTab + vbTab)
                        oHDfile.Append(ptCode)
                    Else
                        oHDfile.Append(Format(Date.Now, "HH:mm:ss") + vbTab + vbTab)
                        oHDfile.Append(ptDocExp + vbTab + vbTab + vbTab)
                        oHDfile.Append(ptCode)
                    End If


            End Select

            Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
                With oStreamWriter
                    .WriteLine(oHDfile)
                    .Flush()
                    .Close()
                End With
            End Using

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub C_CALxWriteLogExportSale(ByVal ptCode As String, ByVal ptMsg As String,
                                                ByVal ptDocExp As String,
                                                ByVal ptCstCode As String,
                                                ByVal ptBchCode As String,
                                                ByVal ptPosCode As String,
                                                ByVal ptSaleAmt As String
                                               )

        Try
            Dim oHDfile As New StringBuilder
            Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\Log\Process" 'เพิ่ม Folder AdaLog *CH 02-12-2014 
            Dim tSuccess As String = "1"
            Dim tFail As String = "0"
            Dim tRef As String = ""

            If Not IO.Directory.Exists(tPathLog) Then
                IO.Directory.CreateDirectory(tPathLog)
            End If

            If File.Exists(tPathLog) = False Then
                If wExportData.nC_CreateFileSale = 1 Then

                    tPathLog &= "\" & "SaleTransaction_" & Format(Date.Now, "yyyyMMdd") & "_" & Format(Date.Now, "HHmmss") & ".txt"
                    Using oSw As StreamWriter = File.CreateText(tPathLog)
                        oSw.Close()
                    End Using
                Else
                    Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log\Process")
                    Dim oGetFile As FileInfo() = tPath.GetFiles()

                    For Each oFile As FileInfo In oGetFile
                        tPathLog &= "\" & oFile.Name
                    Next

                End If
            End If

            If wExportData.nC_CreateFileSale = 1 Then
                oHDfile.Append(Environment.NewLine + Environment.NewLine)
                oHDfile.Append("[Time]" + vbTab + vbTab)
                oHDfile.Append("[DocNo]" + vbTab + vbTab + vbTab)
                oHDfile.Append("[Customer Code]" + vbTab + vbTab)
                oHDfile.Append("[Branch]" + vbTab + vbTab)
                oHDfile.Append("[POS]" + vbTab + vbTab)
                oHDfile.Append("[Sale Amount]" + vbTab + vbTab)
                oHDfile.Append("[Response Status]" + vbTab + vbTab)
                oHDfile.Append(Environment.NewLine)
                oHDfile.Append(Format(Date.Now, "HH:mm:ss") + vbTab)
                oHDfile.Append(ptDocExp + vbTab + vbTab)
                oHDfile.Append(ptCstCode + vbTab + vbTab)
                oHDfile.Append(ptBchCode + vbTab + vbTab + vbTab)
                oHDfile.Append(ptPosCode + vbTab + vbTab)
                oHDfile.Append(ptSaleAmt + vbTab + vbTab + vbTab)
                oHDfile.Append(ptMsg + vbTab + vbTab)
            Else
                oHDfile.Append(Format(Date.Now, "HH:mm:ss") + vbTab)
                oHDfile.Append(ptDocExp + vbTab + vbTab)
                oHDfile.Append(ptCstCode + vbTab + vbTab)
                oHDfile.Append(ptBchCode + vbTab + vbTab + vbTab)
                oHDfile.Append(ptPosCode + vbTab + vbTab)
                oHDfile.Append(ptSaleAmt + vbTab + vbTab + vbTab)
                oHDfile.Append(ptMsg + vbTab + vbTab)
            End If



            Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
                With oStreamWriter
                    .WriteLine(oHDfile)
                    .Flush()
                    .Close()
                End With
            End Using

        Catch ex As Exception

        End Try

    End Sub

    Public Shared Sub C_CALxWriteLogFail(ptMsg As String)

        Try
            Dim oHDfile As New StringBuilder
            Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\Log" 'เพิ่ม Folder AdaLog *CH 02-12-2014 
            Dim tSuccess As String = "1"
            Dim tFail As String = "0"
            Dim tRef As String = ""

            If Not IO.Directory.Exists(tPathLog) Then
                IO.Directory.CreateDirectory(tPathLog)
            End If



            If File.Exists(tPathLog) = False Then
                tPathLog &= "\" & "Log_ExportSale_" & Format(Date.Now, "yyyyMMddHHmm") & ".txt"
                Using oSw As StreamWriter = File.CreateText(tPathLog)
                    oSw.Close()
                End Using
            End If

            If ptMsg = "NotFoundApi" Then
                oHDfile.Append(Environment.NewLine)
                oHDfile.Append("Success:(0) Fail:(1)" & Environment.NewLine & Environment.NewLine)
                oHDfile.Append("[Export Date] " + Format(Date.Now, "dd/MM/yyyy") + vbTab + "[Export Time] " + Format(Date.Now, "HH:mm:ss") + vbTab + "")
                oHDfile.Append("[Export By] " + Environment.MachineName + Environment.NewLine + "")
                oHDfile.Append("[Fail]" + vbTab + "1" + Environment.NewLine + "")
                oHDfile.Append("[Description]" + "เชื่อมต่อ API ไม่สำเร็จ !! ")

            End If

            Using oStreamWriter As StreamWriter = File.AppendText(tPathLog)
                With oStreamWriter
                    .WriteLine(oHDfile)
                    .Flush()
                    .Close()
                End With
            End Using

        Catch ex As Exception

        End Try

    End Sub

    'Public Shared Sub C_CALxDelete()

    '    Dim tPath As String = AdaConfig.cConfig.tAppPath
    'If AdaConfig.cConfig.oAdaSync.tActive = "1" Then
    '    If Directory.Exists(AdaConfig.cConfig.oAdaSync.tPathLog) Then
    '        tPath = AdaConfig.cConfig.oAdaSync.tPathLog
    '    End If
    'End If

    '    Dim oDirInfoInbox As New DirectoryInfo(tPath)
    '    Dim oFiles As FileInfo() = oDirInfoInbox.GetFiles("*.txt")
    '    For Each oItem In oFiles.Where(Function(c) c.Name.Length > 8).Where(Function(c) c.Name.Substring(0, 8) < Format(Date.Now.AddDays(-7), "yyyyMMdd"))
    '        Try
    '            If File.Exists(oItem.FullName) Then
    '                File.Delete(oItem.FullName)
    '            End If
    '        Catch ex As Exception
    '        End Try
    '    Next
    '    oFiles = Nothing
    '    oDirInfoInbox = Nothing
    'End Sub

End Class
