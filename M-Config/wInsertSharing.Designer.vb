﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wInsertSharing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wInsertSharing))
        Me.olaCode = New System.Windows.Forms.Label()
        Me.olaDetail = New System.Windows.Forms.Label()
        Me.otbCode = New System.Windows.Forms.TextBox()
        Me.otbDetail = New System.Windows.Forms.TextBox()
        Me.ocmInsert = New System.Windows.Forms.Button()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.olaStatus = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'olaCode
        '
        Me.olaCode.AutoSize = True
        Me.olaCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaCode.Location = New System.Drawing.Point(21, 34)
        Me.olaCode.Name = "olaCode"
        Me.olaCode.Size = New System.Drawing.Size(46, 20)
        Me.olaCode.TabIndex = 0
        Me.olaCode.Tag = "2;รหัส;Code"
        Me.olaCode.Text = "รหัส :"
        '
        'olaDetail
        '
        Me.olaDetail.AutoSize = True
        Me.olaDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaDetail.Location = New System.Drawing.Point(21, 81)
        Me.olaDetail.Name = "olaDetail"
        Me.olaDetail.Size = New System.Drawing.Size(95, 20)
        Me.olaDetail.TabIndex = 1
        Me.olaDetail.Tag = "2;รายละเอียด : ;Descrpiption :"
        Me.olaDetail.Text = "รายละเอียด :"
        '
        'otbCode
        '
        Me.otbCode.Enabled = False
        Me.otbCode.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.otbCode.Location = New System.Drawing.Point(127, 34)
        Me.otbCode.Name = "otbCode"
        Me.otbCode.Size = New System.Drawing.Size(82, 26)
        Me.otbCode.TabIndex = 2
        '
        'otbDetail
        '
        Me.otbDetail.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.otbDetail.Location = New System.Drawing.Point(127, 75)
        Me.otbDetail.Name = "otbDetail"
        Me.otbDetail.Size = New System.Drawing.Size(268, 26)
        Me.otbDetail.TabIndex = 3
        '
        'ocmInsert
        '
        Me.ocmInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocmInsert.Location = New System.Drawing.Point(179, 121)
        Me.ocmInsert.Name = "ocmInsert"
        Me.ocmInsert.Size = New System.Drawing.Size(104, 34)
        Me.ocmInsert.TabIndex = 4
        Me.ocmInsert.Tag = "2;บันทึก;Save"
        Me.ocmInsert.Text = "บันทึก"
        Me.ocmInsert.UseVisualStyleBackColor = True
        '
        'ocmClose
        '
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocmClose.Location = New System.Drawing.Point(290, 121)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(105, 34)
        Me.ocmClose.TabIndex = 5
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "ปิด"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'olaStatus
        '
        Me.olaStatus.AutoSize = True
        Me.olaStatus.Location = New System.Drawing.Point(344, 9)
        Me.olaStatus.Name = "olaStatus"
        Me.olaStatus.Size = New System.Drawing.Size(48, 17)
        Me.olaStatus.TabIndex = 6
        Me.olaStatus.Text = "Status"
        Me.olaStatus.Visible = False
        '
        'wInsertSharing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(420, 179)
        Me.Controls.Add(Me.olaStatus)
        Me.Controls.Add(Me.ocmClose)
        Me.Controls.Add(Me.ocmInsert)
        Me.Controls.Add(Me.otbDetail)
        Me.Controls.Add(Me.otbCode)
        Me.Controls.Add(Me.olaDetail)
        Me.Controls.Add(Me.olaCode)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "wInsertSharing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Sharing Code"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents olaCode As Label
    Friend WithEvents olaDetail As Label
    Friend WithEvents otbCode As TextBox
    Friend WithEvents otbDetail As TextBox
    Friend WithEvents ocmInsert As Button
    Friend WithEvents ocmClose As Button
    Friend WithEvents olaStatus As Label
End Class
