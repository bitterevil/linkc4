﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExportCst
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExportCst))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ocmSelect = New System.Windows.Forms.Button()
        Me.olaCstNo = New System.Windows.Forms.Label()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.ogbCst = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.ocmSearch = New System.Windows.Forms.Button()
        Me.otbCst = New System.Windows.Forms.TextBox()
        Me.olaGrpCst = New System.Windows.Forms.Label()
        Me.ogdCst = New System.Windows.Forms.DataGridView()
        Me.opnMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.ogbCst.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.ogdCst, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 3
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.4!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.6!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.opnMain.Controls.Add(Me.Panel1, 1, 3)
        Me.opnMain.Controls.Add(Me.Panel3, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 4
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 326.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.opnMain.Size = New System.Drawing.Size(941, 431)
        Me.opnMain.TabIndex = 0
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ocmSelect)
        Me.Panel1.Controls.Add(Me.olaCstNo)
        Me.Panel1.Controls.Add(Me.ocmClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(25, 345)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(898, 83)
        Me.Panel1.TabIndex = 1
        '
        'ocmSelect
        '
        Me.ocmSelect.Location = New System.Drawing.Point(671, 21)
        Me.ocmSelect.Name = "ocmSelect"
        Me.ocmSelect.Size = New System.Drawing.Size(108, 29)
        Me.ocmSelect.TabIndex = 5
        Me.ocmSelect.Tag = "2;เลือก;Select"
        Me.ocmSelect.Text = "เลือก"
        Me.ocmSelect.UseVisualStyleBackColor = True
        '
        'olaCstNo
        '
        Me.olaCstNo.AutoSize = True
        Me.olaCstNo.Location = New System.Drawing.Point(82, 10)
        Me.olaCstNo.Name = "olaCstNo"
        Me.olaCstNo.Size = New System.Drawing.Size(0, 17)
        Me.olaCstNo.TabIndex = 4
        '
        'ocmClose
        '
        Me.ocmClose.Location = New System.Drawing.Point(785, 21)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(107, 29)
        Me.ocmClose.TabIndex = 2
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.ogbCst)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(25, 19)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(898, 320)
        Me.Panel3.TabIndex = 3
        '
        'ogbCst
        '
        Me.ogbCst.Controls.Add(Me.TableLayoutPanel2)
        Me.ogbCst.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbCst.Location = New System.Drawing.Point(0, 0)
        Me.ogbCst.Name = "ogbCst"
        Me.ogbCst.Size = New System.Drawing.Size(898, 320)
        Me.ogbCst.TabIndex = 0
        Me.ogbCst.TabStop = False
        Me.ogbCst.Tag = "2;เลือกข้อมูลลูกค้าที่ต้องการส่งออก;Select Infomation customer for export"
        Me.ogbCst.Text = "เลือกข้อมูลลูกค้าที่ต้องการส่งออก"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.669759!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.33024!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel4, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ogdCst, 1, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 22)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 301.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(892, 295)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.ocmSearch)
        Me.Panel4.Controls.Add(Me.otbCst)
        Me.Panel4.Controls.Add(Me.olaGrpCst)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(17, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(872, 41)
        Me.Panel4.TabIndex = 0
        '
        'ocmSearch
        '
        Me.ocmSearch.Location = New System.Drawing.Point(298, 6)
        Me.ocmSearch.Name = "ocmSearch"
        Me.ocmSearch.Size = New System.Drawing.Size(100, 28)
        Me.ocmSearch.TabIndex = 5
        Me.ocmSearch.Tag = "2;ค้นหา;Search"
        Me.ocmSearch.Text = "ค้นหา"
        Me.ocmSearch.UseVisualStyleBackColor = True
        '
        'otbCst
        '
        Me.otbCst.Location = New System.Drawing.Point(140, 7)
        Me.otbCst.Name = "otbCst"
        Me.otbCst.Size = New System.Drawing.Size(152, 26)
        Me.otbCst.TabIndex = 4
        '
        'olaGrpCst
        '
        Me.olaGrpCst.AutoSize = True
        Me.olaGrpCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaGrpCst.Location = New System.Drawing.Point(3, 10)
        Me.olaGrpCst.Name = "olaGrpCst"
        Me.olaGrpCst.Size = New System.Drawing.Size(88, 20)
        Me.olaGrpCst.TabIndex = 3
        Me.olaGrpCst.Tag = "2;      รหัสลูกค้า :;CustomerCode :"
        Me.olaGrpCst.Text = "รหัสลูกค้า : "
        '
        'ogdCst
        '
        Me.ogdCst.AllowUserToAddRows = False
        Me.ogdCst.AllowUserToDeleteRows = False
        Me.ogdCst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdCst.Location = New System.Drawing.Point(17, 50)
        Me.ogdCst.Name = "ogdCst"
        Me.ogdCst.RowTemplate.Height = 24
        Me.ogdCst.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.ogdCst.Size = New System.Drawing.Size(872, 241)
        Me.ogdCst.TabIndex = 1
        '
        'wExportCst
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(941, 431)
        Me.Controls.Add(Me.opnMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExportCst"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออกข้อมูลลูกค้า;Export Customer"
        Me.Text = "ส่งออกข้อมูลลูกค้า"
        Me.opnMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.ogbCst.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.ogdCst, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel3 As Panel
    Friend WithEvents ogbCst As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents ocmSearch As Button
    Friend WithEvents otbCst As TextBox
    Friend WithEvents olaGrpCst As Label
    Friend WithEvents ogdCst As DataGridView
    Friend WithEvents olaCstNo As Label
    Friend WithEvents ocmSelect As Button
    Friend WithEvents ocmClose As Button
End Class
