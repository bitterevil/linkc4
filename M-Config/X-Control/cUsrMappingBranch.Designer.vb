﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class cUsrMappingBranch
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(cUsrMappingBranch))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.olaBranch = New System.Windows.Forms.Label()
        Me.opnBranch = New System.Windows.Forms.Panel()
        Me.ogdBranch = New C1.Win.C1FlexGrid.C1FlexGrid()
        Me.opnMain.SuspendLayout()
        Me.opnBranch.SuspendLayout()
        CType(Me.ogdBranch, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 6.790123!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 93.20988!))
        Me.opnMain.Controls.Add(Me.olaBranch, 1, 1)
        Me.opnMain.Controls.Add(Me.opnBranch, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Margin = New System.Windows.Forms.Padding(4)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 5
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.14286!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.85714!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 410.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13.0!))
        Me.opnMain.Size = New System.Drawing.Size(648, 485)
        Me.opnMain.TabIndex = 2
        '
        'olaBranch
        '
        Me.olaBranch.AutoSize = True
        Me.olaBranch.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaBranch.Location = New System.Drawing.Point(47, 13)
        Me.olaBranch.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaBranch.Name = "olaBranch"
        Me.olaBranch.Size = New System.Drawing.Size(73, 20)
        Me.olaBranch.TabIndex = 0
        Me.olaBranch.Text = "Branch :"
        '
        'opnBranch
        '
        Me.opnBranch.Controls.Add(Me.ogdBranch)
        Me.opnBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnBranch.Location = New System.Drawing.Point(47, 44)
        Me.opnBranch.Margin = New System.Windows.Forms.Padding(4)
        Me.opnBranch.Name = "opnBranch"
        Me.opnBranch.Size = New System.Drawing.Size(597, 402)
        Me.opnBranch.TabIndex = 1
        '
        'ogdBranch
        '
        Me.ogdBranch.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
        Me.ogdBranch.BorderStyle = C1.Win.C1FlexGrid.Util.BaseControls.BorderStyleEnum.XpThemes
        Me.ogdBranch.ColumnInfo = resources.GetString("ogdBranch.ColumnInfo")
        Me.ogdBranch.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdBranch.HighLight = C1.Win.C1FlexGrid.HighLightEnum.WithFocus
        Me.ogdBranch.Location = New System.Drawing.Point(0, 0)
        Me.ogdBranch.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdBranch.Name = "ogdBranch"
        Me.ogdBranch.Rows.DefaultSize = 25
        Me.ogdBranch.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
        Me.ogdBranch.Size = New System.Drawing.Size(597, 402)
        Me.ogdBranch.StyleInfo = resources.GetString("ogdBranch.StyleInfo")
        Me.ogdBranch.TabIndex = 0
        Me.ogdBranch.VisualStyle = C1.Win.C1FlexGrid.VisualStyle.Office2007Blue
        '
        'cUsrMappingBranch
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.Name = "cUsrMappingBranch"
        Me.Size = New System.Drawing.Size(648, 485)
        Me.opnMain.ResumeLayout(False)
        Me.opnMain.PerformLayout()
        Me.opnBranch.ResumeLayout(False)
        CType(Me.ogdBranch, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents olaBranch As Label
    Friend WithEvents opnBranch As Panel
    Friend WithEvents ogdBranch As C1.Win.C1FlexGrid.C1FlexGrid
End Class
