﻿Public Class cControlGP
    Private oW_ListCtl As New Hashtable
    Private oW_DesCtl As New Hashtable
    Private tW_CurDesClt As String = ""

    Public Shared Property tAppPath As String = Application.StartupPath

    Public Enum eType
        [GP] = 1
        [SharingCode] = 2
    End Enum

    Public Sub New()
        oW_ListCtl.Add(eType.GP, New cUrsGP)
        oW_ListCtl.Add(eType.SharingCode, New cUsrSharingCode)
    End Sub

    Private Sub W_SetxCurDes(ByVal ptDes As String)
        Me.tW_CurDesClt = ptDes
    End Sub

    Public Function W_GETtCurDes() As String
        Return Me.tW_CurDesClt
    End Function

    Public Sub W_GETxCurCtl(ByVal poControl As Control, ByVal pnIndex As eType)
        If Not Me.oW_ListCtl(pnIndex) Is Nothing Then
            poControl.Controls.Clear()
            poControl.Controls.Add(Me.oW_ListCtl(pnIndex))
            Me.W_SetxCurDes(Me.oW_DesCtl(pnIndex))
        End If
    End Sub

End Class
