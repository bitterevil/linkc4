﻿Public Class cUsrConfiguration

    Public Property W_DATtKey() As String
        Get
            Return otbKey.Text.Trim
        End Get
        Set(value As String)
            Me.otbKey.Text = value
        End Set
    End Property

    Public Property W_DATtMethod() As String
        Get
            Return otbMethod.Text.Trim
        End Get
        Set(value As String)
            Me.otbMethod.Text = value
        End Set
    End Property

    Public Property W_DATtContentType() As String
        Get
            Return otbContent.Text.Trim
        End Get
        Set(value As String)
            Me.otbContent.Text = value
        End Set
    End Property

    Public Property W_DATtMember() As String
        Get
            Return otbMember.Text.Trim
        End Get
        Set(value As String)
            Me.otbMember.Text = value
        End Set
    End Property

    Public Property W_DATtMemberGrp() As String
        Get
            Return otbMemGrp.Text.Trim
        End Get
        Set(value As String)
            Me.otbMemGrp.Text = value
        End Set
    End Property

    Public Property W_DATtSaleTran() As String
        Get
            Return otbSaleTran.Text.Trim
        End Get
        Set(value As String)
            Me.otbSaleTran.Text = value
        End Set
    End Property

    Public Property W_DATtStatusExpCst() As String
        Get
            Return Me.olaStatusExp.Text
        End Get
        Set(ByVal value As String)
            Me.olaStatusExp.Text = 0
        End Set
    End Property

    Public Property W_DATtStatusExpGrp() As String
        Get
            Return Me.olaGrp.Text
        End Get
        Set(ByVal value As String)
            Me.olaGrp.Text = 0
        End Set
    End Property

    Public Property W_DATtStatusExpSale() As String
        Get
            Return Me.olaSale.Text
        End Get
        Set(ByVal value As String)
            Me.olaSale.Text = 0
        End Set
    End Property

    Public Property W_DATtCusKey() As String
        Get
            Return Me.otbCusKey.Text
        End Get
        Set(ByVal value As String)
            Me.otbCusKey.Text = value
        End Set
    End Property

    Public Property W_DATtCusSecret() As String
        Get
            Return Me.otbCusSecret.Text
        End Get
        Set(ByVal value As String)
            Me.otbCusSecret.Text = value
        End Set
    End Property

    Public Property W_DATtTokenSecret() As String
        Get
            Return Me.otbTokenSecret.Text
        End Get
        Set(ByVal value As String)
            Me.otbTokenSecret.Text = value
        End Set
    End Property


End Class
