﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wEditEDC
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wEditEDC))
        Me.ordCrd1 = New System.Windows.Forms.RadioButton()
        Me.ordCrd2 = New System.Windows.Forms.RadioButton()
        Me.ordCrd3 = New System.Windows.Forms.RadioButton()
        Me.ordCrdOther = New System.Windows.Forms.RadioButton()
        Me.ocmSave = New System.Windows.Forms.Button()
        Me.olaKey = New System.Windows.Forms.Label()
        Me.olaEDC = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'ordCrd1
        '
        Me.ordCrd1.AutoSize = True
        Me.ordCrd1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ordCrd1.Location = New System.Drawing.Point(22, 29)
        Me.ordCrd1.Name = "ordCrd1"
        Me.ordCrd1.Size = New System.Drawing.Size(152, 24)
        Me.ordCrd1.TabIndex = 0
        Me.ordCrd1.TabStop = True
        Me.ordCrd1.Text = "CreditsumCard1"
        Me.ordCrd1.UseVisualStyleBackColor = True
        '
        'ordCrd2
        '
        Me.ordCrd2.AutoSize = True
        Me.ordCrd2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ordCrd2.Location = New System.Drawing.Point(22, 59)
        Me.ordCrd2.Name = "ordCrd2"
        Me.ordCrd2.Size = New System.Drawing.Size(152, 24)
        Me.ordCrd2.TabIndex = 1
        Me.ordCrd2.TabStop = True
        Me.ordCrd2.Text = "CreditsumCard2"
        Me.ordCrd2.UseVisualStyleBackColor = True
        '
        'ordCrd3
        '
        Me.ordCrd3.AutoSize = True
        Me.ordCrd3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ordCrd3.Location = New System.Drawing.Point(22, 89)
        Me.ordCrd3.Name = "ordCrd3"
        Me.ordCrd3.Size = New System.Drawing.Size(152, 24)
        Me.ordCrd3.TabIndex = 2
        Me.ordCrd3.TabStop = True
        Me.ordCrd3.Text = "CreditsumCard3"
        Me.ordCrd3.UseVisualStyleBackColor = True
        '
        'ordCrdOther
        '
        Me.ordCrdOther.AutoSize = True
        Me.ordCrdOther.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ordCrdOther.Location = New System.Drawing.Point(22, 119)
        Me.ordCrdOther.Name = "ordCrdOther"
        Me.ordCrdOther.Size = New System.Drawing.Size(104, 24)
        Me.ordCrdOther.TabIndex = 3
        Me.ordCrdOther.TabStop = True
        Me.ordCrdOther.Text = "Othersum"
        Me.ordCrdOther.UseVisualStyleBackColor = True
        '
        'ocmSave
        '
        Me.ocmSave.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ocmSave.Location = New System.Drawing.Point(22, 177)
        Me.ocmSave.Name = "ocmSave"
        Me.ocmSave.Size = New System.Drawing.Size(162, 34)
        Me.ocmSave.TabIndex = 4
        Me.ocmSave.Tag = "2;เลือกรายการ;Select"
        Me.ocmSave.Text = "เลือกรายการ"
        Me.ocmSave.UseVisualStyleBackColor = True
        '
        'olaKey
        '
        Me.olaKey.AutoSize = True
        Me.olaKey.Location = New System.Drawing.Point(19, 157)
        Me.olaKey.Name = "olaKey"
        Me.olaKey.Size = New System.Drawing.Size(32, 17)
        Me.olaKey.TabIndex = 5
        Me.olaKey.Text = "Key"
        Me.olaKey.Visible = False
        '
        'olaEDC
        '
        Me.olaEDC.AutoSize = True
        Me.olaEDC.Location = New System.Drawing.Point(57, 157)
        Me.olaEDC.Name = "olaEDC"
        Me.olaEDC.Size = New System.Drawing.Size(36, 17)
        Me.olaEDC.TabIndex = 6
        Me.olaEDC.Text = "EDC"
        Me.olaEDC.Visible = False
        '
        'wEditEDC
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(208, 223)
        Me.Controls.Add(Me.olaEDC)
        Me.Controls.Add(Me.olaKey)
        Me.Controls.Add(Me.ocmSave)
        Me.Controls.Add(Me.ordCrdOther)
        Me.Controls.Add(Me.ordCrd3)
        Me.Controls.Add(Me.ordCrd2)
        Me.Controls.Add(Me.ordCrd1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wEditEDC"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EDC"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ordCrd1 As RadioButton
    Friend WithEvents ordCrd2 As RadioButton
    Friend WithEvents ordCrd3 As RadioButton
    Friend WithEvents ordCrdOther As RadioButton
    Friend WithEvents ocmSave As Button
    Friend WithEvents olaKey As Label
    Friend WithEvents olaEDC As Label
End Class
