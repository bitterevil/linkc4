﻿Imports Newtonsoft.Json

Public Class cmlExpSalDT

    <JsonProperty("item_id")>
    Property FTItemID As String

    <JsonProperty("item_quantity")>
    Property FTItemQty As String

    <JsonProperty("item_rate")>
    Property FTItemRate As String

    <JsonProperty("amount")>
    Property FTAmoute As String

    <JsonProperty("tax_rate")>
    Property FTTaxRate As String

    <JsonProperty("tax_amount")>
    Property FTTaxAmt As String

    <JsonProperty("price")>
    Property FTPrice As String

    <JsonProperty("promotion_code")>
    Property FTPmtCode As String

    <JsonProperty("totalbeforediscount")>
    Property FTTotalBeforeDiscount As String

    <JsonProperty("totaldiscount")>
    Property FTTotalDiscount As String

    <JsonProperty("GP")>
    Property FTGp As String

    <JsonProperty("GP_Promotion")>
    Property FTGpPmt As String

End Class
