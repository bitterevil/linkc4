﻿Imports Newtonsoft.Json
Public Class cmlExpCustomerInfo
    <JsonProperty("country_id")>
    Property tCountryID As String

    <JsonProperty("address1")>
    Property tAddr1 As String

    <JsonProperty("zip_code")>
    Property tZipcode As String

End Class
