﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class wExportData
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExportData))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.olaTitle = New System.Windows.Forms.Label()
        Me.ogbExport = New System.Windows.Forms.GroupBox()
        Me.ogdExport = New System.Windows.Forms.DataGridView()
        Me.opnMid = New System.Windows.Forms.Panel()
        Me.olaProcess = New System.Windows.Forms.Label()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ocmExport = New System.Windows.Forms.Button()
        Me.opgProcess = New System.Windows.Forms.ProgressBar()
        Me.olaStatus = New System.Windows.Forms.Label()
        Me.obgWorker = New System.ComponentModel.BackgroundWorker()
        Me.opnMain.SuspendLayout()
        Me.ogbExport.SuspendLayout()
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnMid.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 3
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.828523!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 91.17148!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.opnMain.Controls.Add(Me.olaTitle, 1, 1)
        Me.opnMain.Controls.Add(Me.ogbExport, 1, 2)
        Me.opnMain.Controls.Add(Me.opnMid, 1, 3)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 4
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 383.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.Size = New System.Drawing.Size(845, 499)
        Me.opnMain.TabIndex = 0
        '
        'olaTitle
        '
        Me.olaTitle.AutoSize = True
        Me.olaTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaTitle.Location = New System.Drawing.Point(72, 9)
        Me.olaTitle.Name = "olaTitle"
        Me.olaTitle.Size = New System.Drawing.Size(0, 8)
        Me.olaTitle.TabIndex = 0
        '
        'ogbExport
        '
        Me.ogbExport.Controls.Add(Me.ogdExport)
        Me.ogbExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbExport.Location = New System.Drawing.Point(72, 20)
        Me.ogbExport.Name = "ogbExport"
        Me.ogbExport.Size = New System.Drawing.Size(708, 377)
        Me.ogbExport.TabIndex = 1
        Me.ogbExport.TabStop = False
        Me.ogbExport.Tag = "2;เลือกข้อมูลที่ต้องการส่งออก;Select Information for export"
        Me.ogbExport.Text = "เลือกข้อมูลที่ต้องการส่งออก"
        '
        'ogdExport
        '
        Me.ogdExport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.ogdExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdExport.Location = New System.Drawing.Point(3, 22)
        Me.ogdExport.Name = "ogdExport"
        Me.ogdExport.RowTemplate.Height = 24
        Me.ogdExport.Size = New System.Drawing.Size(702, 352)
        Me.ogdExport.TabIndex = 2
        '
        'opnMid
        '
        Me.opnMid.Controls.Add(Me.olaProcess)
        Me.opnMid.Controls.Add(Me.ocmClose)
        Me.opnMid.Controls.Add(Me.ocmExport)
        Me.opnMid.Controls.Add(Me.opgProcess)
        Me.opnMid.Controls.Add(Me.olaStatus)
        Me.opnMid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMid.Location = New System.Drawing.Point(72, 403)
        Me.opnMid.Name = "opnMid"
        Me.opnMid.Size = New System.Drawing.Size(708, 93)
        Me.opnMid.TabIndex = 2
        '
        'olaProcess
        '
        Me.olaProcess.AutoSize = True
        Me.olaProcess.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaProcess.Location = New System.Drawing.Point(74, 0)
        Me.olaProcess.Name = "olaProcess"
        Me.olaProcess.Size = New System.Drawing.Size(0, 20)
        Me.olaProcess.TabIndex = 3
        '
        'ocmClose
        '
        Me.ocmClose.Location = New System.Drawing.Point(597, 52)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(109, 32)
        Me.ocmClose.TabIndex = 2
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ocmExport
        '
        Me.ocmExport.Location = New System.Drawing.Point(482, 52)
        Me.ocmExport.Name = "ocmExport"
        Me.ocmExport.Size = New System.Drawing.Size(109, 32)
        Me.ocmExport.TabIndex = 1
        Me.ocmExport.Tag = "2;ดำเนินการ;Process"
        Me.ocmExport.Text = "Process"
        Me.ocmExport.UseVisualStyleBackColor = True
        '
        'opgProcess
        '
        Me.opgProcess.Location = New System.Drawing.Point(4, 23)
        Me.opgProcess.Name = "opgProcess"
        Me.opgProcess.Size = New System.Drawing.Size(702, 23)
        Me.opgProcess.TabIndex = 1
        '
        'olaStatus
        '
        Me.olaStatus.AutoSize = True
        Me.olaStatus.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaStatus.Location = New System.Drawing.Point(3, 0)
        Me.olaStatus.Name = "olaStatus"
        Me.olaStatus.Size = New System.Drawing.Size(64, 20)
        Me.olaStatus.TabIndex = 0
        Me.olaStatus.Tag = "2;สถานะ :;Status :"
        Me.olaStatus.Text = "สถานะ :"
        '
        'obgWorker
        '
        Me.obgWorker.WorkerReportsProgress = True
        '
        'wExportData
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 499)
        Me.Controls.Add(Me.opnMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExportData"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออก;Export"
        Me.Text = "ส่งออก"
        Me.opnMain.ResumeLayout(False)
        Me.opnMain.PerformLayout()
        Me.ogbExport.ResumeLayout(False)
        CType(Me.ogdExport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnMid.ResumeLayout(False)
        Me.opnMid.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents olaTitle As Label
    Friend WithEvents ogbExport As GroupBox
    Friend WithEvents ogdExport As DataGridView
    Friend WithEvents opnMid As Panel
    Friend WithEvents olaStatus As Label
    Friend WithEvents opgProcess As ProgressBar
    Friend WithEvents obgWorker As System.ComponentModel.BackgroundWorker
    Friend WithEvents ocmClose As Button
    Friend WithEvents ocmExport As Button
    Friend WithEvents olaProcess As Label
End Class
