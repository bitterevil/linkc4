﻿Imports System.Collections.Generic
Imports AdaLinkNetSuiteForC4

Public Class QueryParameterComparer
    Implements IComparer(Of QueryParameter)
    'Public Function Compare(ByVal x As QueryParameter, ByVal y As QueryParameter) As Integer
    '    If x.Name = y.Name Then
    '        Return String.Compare(x.Value, y.Value)
    '    Else
    '        Return String.Compare(x.Name, y.Name)
    '    End If
    'End Function

    Private Function Compare(x As QueryParameter, y As QueryParameter) As Integer Implements IComparer(Of QueryParameter).Compare
        Try
            If x.Name = y.Name Then
                Return String.Compare(x.Value, y.Value)
            Else
                Return String.Compare(x.Name, y.Name)
            End If
        Catch ex As Exception
            Throw New NotImplementedException()
        End Try
    End Function
End Class
