﻿Imports System.Text

Public Class wSettingEDC
    Dim oDbTblEDC As DataTable
    Dim bRefresh As Boolean = False
    Public Function C_GENoColumnEDC() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTEdcCode", GetType(String))
            oDbtbl.Columns.Add("FTBnkName", GetType(String))
            oDbtbl.Columns.Add("FTBnkMapping", GetType(String))
            oDbtbl.Columns.Add("FTStatus", GetType(Boolean))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub W_GENxDataEDC()
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Dim oDbTbl As DataTable
        Try
            oDbTblEDC = New DataTable()
            oDbTbl = New DataTable()
            oSql = New StringBuilder()
            oSql.AppendLine(" SELECT EDC.FTEdcCode, EDC.FTBnkCode, EDC.FTBnkName, EDC.FTBnkMapping, POS.FTEdcStaUse ")
            oSql.AppendLine(" FROM     dbo.TLNKMappingEDC AS EDC INNER JOIN ")
            oSql.AppendLine(" dbo.TPSMPosEdc AS POS ON EDC.FTEdcCode = POS.FTEdcCode")
            'oSql.AppendLine(" WHERE POS.FTEdcStaUse = '1' ")
            oSql.AppendLine(" ORDER BY EDC.FTEdcCode ")
            oDbTbl = oDatabase.C_CALoExecuteReader(oSql.ToString())

            If oDbTbl.Rows.Count > 0 Then
                oDbTblEDC = C_GENoColumnEDC()
                For nRow As Integer = 0 To oDbTbl.Rows.Count - 1
                    Dim oRow As DataRow = oDbTblEDC.NewRow()
                    oRow("FTEdcCode") = oDbTbl.Rows(nRow)("FTEdcCode").ToString
                    oRow("FTBnkName") = oDbTbl.Rows(nRow)("FTBnkName").ToString
                    oRow("FTBnkMapping") = oDbTbl.Rows(nRow)("FTBnkMapping").ToString
                    If oDbTbl.Rows(nRow)("FTEdcStaUse").ToString = 1 Then
                        oRow("FTStatus") = True
                    Else
                        oRow("FTStatus") = False
                    End If
                    oDbTblEDC.Rows.Add(oRow)
                Next

                With ogdEDC
                    .DataSource = oDbTblEDC
                    .RowHeadersVisible = False
                    .AllowUserToAddRows = False

                    .Columns("FTBnkMapping").Width = 150

                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTEdcCode").HeaderText = "เครื่องรูดบัตร"
                        .Columns("FTBnkName").HeaderText = "ชื่อธนาคาร"
                        .Columns("FTBnkMapping").HeaderText = "ฟิลด์ที่ส่งออก"
                        .Columns("FTStatus").HeaderText = "สถานะใช้งาน"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTEdcCode").HeaderText = "EDC"
                        .Columns("FTBnkName").HeaderText = "Bank"
                        .Columns("FTBnkMapping").HeaderText = "Field for export"
                        .Columns("FTStatus").HeaderText = "Status"
                    End If

                    .Columns("FTEdcCode").ReadOnly = True
                    .Columns("FTBnkName").ReadOnly = True
                    .Columns("FTBnkMapping").ReadOnly = True
                    .Columns("FTStatus").ReadOnly = True


                    .Columns("FTBnkName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    If bRefresh = False Then
                        Dim obtn As New DataGridViewButtonColumn()
                        If .Columns("Edit") Is Nothing Then
                            .Columns.Add(obtn)
                        End If
                        obtn.HeaderText = "Edit"
                        obtn.Name = "Edit"
                        obtn.Width = 40
                    End If

                End With
            End If

        Catch ex As Exception

        End Try

    End Sub


    Public Sub W_GENxTableMapping()
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Try
            oSql = New StringBuilder()

            oSql.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSql.AppendLine("WHERE object_id = OBJECT_ID(N'TLNKMappingEDC') AND type in (N'U'))")
            oSql.AppendLine("BEGIN")
            oSql.AppendLine(" CREATE TABLE [dbo].[TLNKMappingEDC](")
            oSql.AppendLine(" [FTEdcCode] [varchar](3) NOT NULL, ")
            oSql.AppendLine(" [FTBnkCode] [varchar](50) NULL, ")
            oSql.AppendLine(" [FTBnkName] [varchar](150) NULL, ")
            oSql.AppendLine(" [FTBnkMapping] [varchar](50) NOT NULL, ")
            oSql.AppendLine("  CONSTRAINT [PK_TLNKMappingEDC] PRIMARY KEY CLUSTERED  ")
            oSql.AppendLine(" (")
            oSql.AppendLine(" [FTEdcCode] ASC, ")
            oSql.AppendLine(" [FTBnkMapping] ASC")
            oSql.AppendLine(" )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
            oSql.AppendLine(" ) ON [PRIMARY]")
            oSql.AppendLine("END")
            Dim nSucc As Integer = oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
            W_GENxDataMapping()
                W_UptxUpdateEDC()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub wSettingEDC_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_GENxTableMapping()
        W_GENxDataEDC()
    End Sub

    Public Sub W_GENxDataMapping()
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Try
            oSql = New StringBuilder()
            oSql.AppendLine(" INSERT INTO TLNKMappingEDC (FTEdcCode, FTBnkCode, FTBnkName, FTBnkMapping) ")
            oSql.AppendLine(" SELECT dbo.TPSMEdc.FTEdcCode, dbo.TCNMBank.FTBnkCode, dbo.TCNMBank.FTBnkName, ")
            oSql.AppendLine(" CASE dbo.TPSMEdc.FTEdcCode  WHEN '001' THEN 'CreditsumCard1' ")
            oSql.AppendLine(" WHEN '002' THEN 'CreditsumCard2' ")
            oSql.AppendLine(" WHEN '003' THEN 'CreditsumCard3' ")
            oSql.AppendLine(" ELSE 'Othersum' END AS FTEdc ")
            oSql.AppendLine(" FROM     dbo.TPSMEdc INNER JOIN ")
            oSql.AppendLine(" dbo.TCNMBank ON dbo.TPSMEdc.FTBnkCode = dbo.TCNMBank.FTBnkCode ")
            oSql.AppendLine(" INNER JOIN ")
            oSql.AppendLine(" dbo.TPSMPosEdc AS PEDC ON dbo.TPSMEdc.FTEdcCode = PEDC.FNEdcSeq ")
            oSql.AppendLine(" WHERE PEDC.FTEdcStaUse = '1' ")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
        Catch ex As Exception

        End Try

    End Sub

    Private Sub ogdEDC_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdEDC.CellContentClick
        If ogdEDC.Columns(e.ColumnIndex).Name = "Edit" AndAlso e.RowIndex >= 0 Then
            Dim oFrm As New wEditEDC()
            oFrm.olaKey.Text = ogdEDC.Rows(e.RowIndex).Cells("FTEdcCode").Value
            oFrm.olaEDC.Text = ogdEDC.Rows(e.RowIndex).Cells("FTBnkMapping").Value
            If oFrm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                bRefresh = True
                W_GENxDataEDC()
                oFrm.olaKey.Text = ""
            End If

            'End If
        End If
    End Sub

    Private Sub ogdEDC_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles ogdEDC.CellPainting
        If ogdEDC.Columns(e.ColumnIndex).Name = "Edit" AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)

            Dim img As Image = Image.FromFile(AdaConfig.cConfig.tAppPath & "\img\if_pen_1814074.ico")
            e.Graphics.DrawImage(img, CInt((e.CellBounds.Width / 2) - (img.Width / 2)) + e.CellBounds.X, CInt((e.CellBounds.Height / 2) - (img.Height / 2)) + e.CellBounds.Y)
            e.Handled = True
        End If
    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Public Sub W_UptxUpdateEDC()
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Dim oDbTbl As New DataTable()
        Try
            oSql = New StringBuilder()
            oSql.AppendLine(" SELECT dbo.TPSMEdc.FTEdcCode, dbo.TCNMBank.FTBnkCode, dbo.TCNMBank.FTBnkName,  ")
            oSql.AppendLine(" CASE dbo.TPSMEdc.FTEdcCode WHEN '001'  ")
            oSql.AppendLine(" THEN 'CreditsumCard1' WHEN '002' THEN 'CreditsumCard2' ")
            oSql.AppendLine(" WHEN '003' THEN 'CreditsumCard3' ELSE 'Othersum' END AS FTEdc")
            oSql.AppendLine(" FROM     dbo.TPSMEdc INNER JOIN ")
            oSql.AppendLine("  dbo.TCNMBank ON dbo.TPSMEdc.FTBnkCode = dbo.TCNMBank.FTBnkCode INNER JOIN ")
            oSql.AppendLine("  dbo.TPSMPosEdc AS PEDC ON dbo.TPSMEdc.FTEdcCode = PEDC.FNEdcSeq")
            oSql.AppendLine("  WHERE  NOT EXISTS ( ")
            oSql.AppendLine(" SELECT FTEdcCode ")
            oSql.AppendLine(" FROM TLNKMappingEDC MAP ")
            oSql.AppendLine("  WHERE dbo.TPSMEdc.FTEdcCode = MAP.FTEdcCode)   ")
            oSql.AppendLine(" AND PEDC.FTEdcStaUse = '1' ")
            oDbTbl = oDatabase.C_GEToTblSQL(oSql.ToString())
            For nRow As Integer = 0 To oDbTbl.Rows.Count - 1
                oSql.Clear()
                oSql.AppendLine(" INSERT ")
                oSql.AppendLine(" INTO TLNKMappingEDC(FTEdcCode, FTBnkCode, FTBnkName, FTBnkMapping)")
                oSql.AppendLine(" VALUES ('" + oDbTbl(nRow)("FTEdcCode").ToString() + "' , ")
                oSql.AppendLine(" '" + oDbTbl(nRow)("FTBnkCode").ToString() + "',")
                oSql.AppendLine(" '" + oDbTbl(nRow)("FTBnkName").ToString() + "',")
                oSql.AppendLine(" '" + oDbTbl(nRow)("FTEdc").ToString() + "')")
                oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
            Next

        Catch ex As Exception

        End Try
    End Sub
End Class