﻿Imports System.Text

Public Class wEditSharing

    Dim oC_oDbTblCmb As New DataTable()
    Dim oXmlCtgAPI As XElement
    Dim oFunc As New AdaConfig.cCNSP
    Private Sub ocmCancle_Click(sender As Object, e As EventArgs) Handles ocmCancle.Click
        Me.Close()
    End Sub

    Public Sub W_DATxLoadShareDiscount()

        Dim oSql As New StringBuilder()
        Dim oDatabase As New cDatabaseLocal()
        Dim oDbtblPmt As New DataTable()

        oSql.AppendLine(" SELECT FTLnkShrCode, FTLnkShrName ")
        oSql.AppendLine(" FROM    TLNKMappingGP  ")
        oSql.AppendLine(" WHERE FTPmhCode = '" + olaKey.Text + "' ")
        oDbtblPmt = oDatabase.C_CALoExecuteReader(oSql.ToString())

        ocbSharing.DataSource = C_DATxLoadShareDiscount()
        ocbSharing.DisplayMember = "Detail"
        ocbSharing.ValueMember = "ID"
        ocbSharing.SelectedIndex = oDbtblPmt.Rows(0)("FTLnkShrCode") - 1

    End Sub



    Public Function C_DATxLoadShareDiscount() As DataTable
        Dim oSql As New StringBuilder()
        Dim oDatabase As New cDatabaseLocal()
        Dim oDbtblPmt As New DataTable()

        Try

            oC_oDbTblCmb.Columns.Add("Detail", GetType(String))
            oC_oDbTblCmb.Columns.Add("ID", GetType(String))

            oXmlCtgAPI = XElement.Load(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")

            Dim oCompenSate = From c In oXmlCtgAPI.Elements("CompenSate") Select c
            For Each oItem In oCompenSate
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next


            Dim oShareDis = From c In oXmlCtgAPI.Elements("ShareDis") Select c
            For Each oItem In oShareDis
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnnet = From c In oXmlCtgAPI.Elements("MarginOnnet") Select c
            For Each oItem In oMarginOnnet
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnSale = From c In oXmlCtgAPI.Elements("MarginOnSale") Select c
            For Each oItem In oMarginOnSale
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount30 = From c In oXmlCtgAPI.Elements("ShareDiscount30") Select c
            For Each oItem In oShareDiscount30
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount40 = From c In oXmlCtgAPI.Elements("ShareDiscount40") Select c
            For Each oItem In oShareDiscount40
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Dim oShareDiscount40_1 = From c In oXmlCtgAPI.Elements("ShareDiscount40_1") Select c
            For Each oItem In oShareDiscount40_1
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_2 = From c In oXmlCtgAPI.Elements("ShareDiscount40_2") Select c
            For Each oItem In oShareDiscount40_2
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_3 = From c In oXmlCtgAPI.Elements("ShareDiscount40_3") Select c
            For Each oItem In oShareDiscount40_3
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_4 = From c In oXmlCtgAPI.Elements("ShareDiscount40_4") Select c
            For Each oItem In oShareDiscount40_4
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_5 = From c In oXmlCtgAPI.Elements("ShareDiscount40_5") Select c
            For Each oItem In oShareDiscount40_5
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Return oC_oDbTblCmb
        Catch ex As Exception

        End Try

    End Function

    Private Sub wEditSharing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_DATxLoadShareDiscount()
    End Sub

    Private Sub ocmSave_Click(sender As Object, e As EventArgs) Handles ocmSave.Click
        C_DATbSaveDataMapping()
        Me.DialogResult = Windows.Forms.DialogResult.OK
    End Sub

    Private Sub otbGp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles otbGp.KeyPress
        If oFunc.SP_SHWbInputNumberOnly(e.KeyChar) = True Then
            MsgBox("กรอกได้เฉพาะตัวเลข และ จุดทศนิยมเท่านั้น ", MsgBoxStyle.Critical)
            e.Handled = True
            otbGp.Text = ""
        End If
    End Sub

    Public Function C_DATbSaveDataMapping() As Boolean
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Dim cGP As Decimal = String.Format("{0:F2}", otbGp.Text)
        oSql = New StringBuilder()
        Try

            oSql.AppendLine(" UPDATE TLNKMappingGP ")
            oSql.AppendLine(" SET    FTLnkShrCode = '" + ocbSharing.SelectedValue + "' , ")
            oSql.AppendLine("  FTLnkShrName = '" + ocbSharing.Text + "',")
            oSql.AppendLine(" FCLnkGP = '" + CType(cGP, String) + "', ")
            oSql.AppendLine("  FDDateUpd =  CONVERT(VARCHAR(10), GETDATE(), 121),")
            oSql.AppendLine("  FTTimeUpd = Convert(VARCHAR(8), GETDATE(), 108) , ")
            oSql.AppendLine("   FTWhoUpd = 'AdaLink'")
            oSql.AppendLine(" WHERE FTPmhCode = '" + olaKey.Text + "'")

            Dim nSucc As Integer = oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
            If nSucc = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function

End Class