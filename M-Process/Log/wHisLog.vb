﻿Imports System.ComponentModel
Imports System.IO

Public Class wHisLog
    Public bW_LoadFirst As Boolean = False
    Private Sub wHisLog_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        odtDate.Font = New Font("Microsoft Sans Serif", 11, FontStyle.Regular)
        W_SETxLoadLogfile("")
    End Sub

    ''' <summary>
    ''' Spilt ค่า จาก Textfile มาใส่ใน Column Grid
    ''' </summary>
    Public Function W_SETxLoadLogfile(ByVal ptDate As String) As Boolean
        Try
            Dim tDes As String = ""
            Dim atPdtRow As New ArrayList
            Dim oDbTbl As New DataTable
            Dim tSplie() As String
            oDbTbl = W_SEToDrow()

            Dim tPathName As String = ""
            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log")
            Dim oGetFile As FileInfo() = tPath.GetFiles()

            For Each oFile As FileInfo In oGetFile
                tDes = ""
                Dim oDatarow As DataRow = oDbTbl.NewRow()
                Dim oArr As Array

                oArr = Split(oFile.Name, "_")
                oDatarow("Date") = W_CONdConverttoDate(oArr(1), "Date")
                oDatarow("FileName") = oFile.Name
                oDatarow("Refer") = W_CONdConverttoDate(oArr(2), "Time")

                If ptDate = "" Then
                    oDbTbl.Rows.Add(oDatarow)
                Else
                    Dim tDate As String = Convert.ToDateTime(ptDate).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    If W_CONdConverttoDate(oArr(1), "Date") = tDate Then
                        oDbTbl.Rows.Add(oDatarow)
                    End If
                End If

                'Clear ค่า ArrayList ไม่ให้จำค่าเดิม
                atPdtRow.Clear()
            Next

            ' End If
            With ogdLog

                If .Rows.Count >= 0 Then
                    .DataSource = oDbTbl

                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("Date").HeaderText = "วันที่"
                        .Columns("FileName").HeaderText = "ชื่อ"
                        .Columns("Refer").HeaderText = "เวลา"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("Date").HeaderText = "Date"
                        .Columns("FileName").HeaderText = "FileName"
                        .Columns("Refer").HeaderText = "เวลา"
                    End If
                    .Columns("Date").ReadOnly = True
                    .Columns("FileName").ReadOnly = True
                    .Columns("Refer").ReadOnly = True
                    .Sort(ogdLog.Columns(1), ListSortDirection.Descending)
                    .Columns("FileName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    ogdLog.RowHeadersVisible = False

                    'สร้าง Column Button
                    If bW_LoadFirst = False Then
                        Dim obtn As New DataGridViewButtonColumn()
                        If ogdLog.Columns("") Is Nothing Then
                            ogdLog.Columns.Add(obtn)
                        End If
                        obtn.Width = 20
                        obtn.Text = "..."
                        obtn.HeaderText = ""
                        obtn.Name = "File"
                        obtn.UseColumnTextForButtonValue = True
                    End If


                End If
            End With

        Catch ex As Exception

        End Try
    End Function

    Public Function W_SEToDrow() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("Date")
            oDbtbl.Columns.Add("Refer")
            oDbtbl.Columns.Add("FileName")
            'oDbtbl.Columns.Add("Description")

            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function W_CONdConverttoDate(ByVal tFile As String, ByVal ptType As String) As String
        Try
            Dim oArr As Array
            oArr = Split(tFile, ".")
            Dim tConFormat As String = ""
            Dim tDate As String = ""

            'แปลง String กลับเป็นวันที่ 
            If ptType = "Date" Then
                tConFormat = DateTime.ParseExact(tFile, "yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
                tDate = Convert.ToDateTime(tConFormat).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Else
                tConFormat = DateTime.ParseExact(oArr(0), "HHmmss", New System.Globalization.CultureInfo("en-US"))
                tDate = Convert.ToDateTime(tConFormat).ToString("HH:mm:ss", New System.Globalization.CultureInfo("en-US"))
            End If

            Return tDate

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Private Sub ogdLog_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdLog.CellContentClick
        If ogdLog.Columns(e.ColumnIndex).Name = "File" AndAlso e.RowIndex >= 0 Then
            Dim tPath As String = AdaConfig.cConfig.tAppPath & "\Log\" + ogdLog.Rows(e.RowIndex).Cells("FileName").Value

            If System.IO.File.Exists(tPath) = True Then
                Diagnostics.Process.Start(tPath)
            Else
                cCNSP.SP_MSGnShowing(e.ColumnIndex = 4, cCNEN.eEN_MSGStyle.nEN_MSGWarn, True)
            End If
        End If
        'If e.ColumnIndex = 3 Then

        '    Dim tPath As String = AdaConfig.cConfig.tAppPath & "\Log\" + ogdLog.Rows(e.RowIndex).Cells(2).Value

        '    If System.IO.File.Exists(tPath) = True Then
        '        Diagnostics.Process.Start(tPath)
        '    Else
        '        cCNSP.SP_MSGnShowing(e.ColumnIndex = 4, cCNEN.eEN_MSGStyle.nEN_MSGWarn, True)
        '    End If
        'End If

    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub



    Private Sub ocmClear_Click(sender As Object, e As EventArgs) Handles ocmClear.Click
        Try

            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log")
            Dim oGetFile As FileInfo() = tPath.GetFiles()
            For Each oFile As FileInfo In oGetFile
                Dim tDel As String = AdaConfig.cConfig.tAppPath & "\Log\" & oFile.Name
                System.IO.File.Delete(tDel)
                ogdLog.ClearSelection()
            Next
            W_SETxLoadLogfile("")
        Catch ex As Exception

        End Try
    End Sub

    Private Sub odtDate_ValueChanged(sender As Object, e As EventArgs) Handles odtDate.ValueChanged
        bW_LoadFirst = True
        W_SETxLoadLogfile(odtDate.Value)
    End Sub

End Class