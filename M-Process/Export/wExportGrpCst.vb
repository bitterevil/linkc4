﻿Imports System.IO
Imports System.Text

Public Class wExportGrpCst
    Dim oW_oDbtblTmpCgpCst As DataTable
    Public Shared oW_oDbtblTmpCgpCstExp As DataTable
    Dim tW_CstNo As String

    Private Sub wExportGrpCst_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_GETdataGridGrpCst("")

    End Sub
    ''' <summary>
    ''' สร้าง column สำหรับ Table กลุ่มลูกค้า
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnGroupCustomer() As DataTable
        Try
            Dim oDbTbl As New DataTable()
            oDbTbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbTbl.Columns.Add("FTCgpCode", GetType(String))
            oDbTbl.Columns.Add("FTCgpName", GetType(String))
            oDbTbl.Columns.Add("FDDateUpd", GetType(String))
            oDbTbl.Columns.Add("FTTimeUpd", GetType(String))
            Return oDbTbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Load ข้อมูลกลุ่มลูกค้าลง grid
    ''' </summary>

    Public Function W_GETdataGridGrpCst(ByVal ptGrp As String) As Boolean
        Try
            Dim oSql As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtbl As New DataTable
            Dim tExportFirst = AdaConfig.cConfig.oConfigAPI.tExportGrp

            oSql.AppendLine(" SELECT FTCgpCode,FTCgpName,FTTimeUpd,FDDateUpd FROM TCNMCstGrp ")

            If tExportFirst <> 0 Then
                oSql.AppendLine(" WHERE (FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121)) ")
            Else
                If tExportFirst = 0 And ptGrp <> "" Then
                    oSql.AppendLine(" WHERE FTCgpCode = '" + ptGrp + "'  ")
                ElseIf ptGrp <> "" Then
                    oSql.AppendLine(" AND FTCgpCode = '" + ptGrp + "'  ")
                End If
            End If




            oDbtbl = oDatabase.C_CALoExecuteReader(oSql.ToString())

            If oDbtbl.Rows.Count > 0 Then
                oW_oDbtblTmpCgpCst = W_SEToColumnGroupCustomer()
                For nRow As Integer = 0 To oDbtbl.Rows.Count - 1
                    Dim oRow As DataRow = oW_oDbtblTmpCgpCst.NewRow()
                    oRow("FTSelect") = True
                    oRow("FTCgpCode") = oDbtbl.Rows(nRow).Item("FTCgpCode")
                    oRow("FTCgpName") = oDbtbl.Rows(nRow).Item("FTCgpName")
                    oRow("FDDateUpd") = Convert.ToDateTime(oDbtbl.Rows(nRow).Item("FDDateUpd")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    oRow("FTTimeUpd") = oDbtbl.Rows(nRow).Item("FTTimeUpd")
                    oW_oDbtblTmpCgpCst.Rows.Add(oRow)
                Next
                ogdGrpCst.DataSource = oW_oDbtblTmpCgpCst

                With ogdGrpCst
                    .Columns("FTSelect").Width = 50
                    .Columns("FTSelect").HeaderText = ""
                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTCgpCode").HeaderText = "รหัส"
                        .Columns("FTCgpName").HeaderText = "ชื่อ - กลุ่มลูกค้า"
                        .Columns("FDDateUpd").HeaderText = "วันที่ Update ล่าสุด"
                        .Columns("FTTimeUpd").HeaderText = "เวลา Update ล่าสุด"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTCgpCode").HeaderText = "Group Code"
                        .Columns("FTCgpName").HeaderText = "Group CustomerName"
                        .Columns("FDDateUpd").HeaderText = "Date Update"
                        .Columns("FTTimeUpd").HeaderText = "Time Update"
                    End If

                    .Columns("FTCgpCode").ReadOnly = True
                    .Columns("FTCgpName").ReadOnly = True

                    ogdGrpCst.RowHeadersVisible = False
                    .Columns("FTCgpCode").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FTCgpName").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FDDateUpd").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FTTimeUpd").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill

                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .AllowUserToAddRows = False
                End With
                W_SETxCheckboxGrpCstAllow()
                Return True
            Else
                Return False
            End If

        Catch ex As Exception

        End Try
    End Function

    Public Sub W_EXPxExportDataGroupCustomer()
        Try


            Dim tDate As String = Convert.ToDateTime(Now).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
            Dim oExpSale As New cExportSale
            Dim tSelect As String = ""
            Dim tExportFirst As String = ""
            tExportFirst = AdaConfig.cConfig.oConfigAPI.tExportCst

            'Export first = 0 : ครั้งแรกต้อง Export แบบ Manual ทั้งหมดก่อน / ครั้งต่อไป Export เฉพาะข้อมูลที่มีการ Update
            Dim tCstCode As String = ""
            If oW_oDbtblTmpCgpCst.Rows.Count Then
                For nRowCst As Integer = 0 To oW_oDbtblTmpCgpCst.Rows.Count - 1
                    If ogdGrpCst.Rows(nRowCst).Cells(0).Value = True Then
                        tCstCode = ogdGrpCst.Rows(nRowCst).Cells(1).Value
                        oExpSale.W_SETxSaveStatusExpFirst("ExportGrpFirst")
                        tW_CstNo = tCstCode
                        If oExpSale.C_EXPbExportGrpCustomer(tCstCode) Then
                        End If
                    End If
                Next
            End If

        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmSearch_Click(sender As Object, e As EventArgs) Handles ocmSearch.Click
        W_GETdataGridGrpCst(otbCst.Text)
    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub ocmSelect_Click(sender As Object, e As EventArgs) Handles ocmSelect.Click

        Dim bChoose As Boolean = False
        For nrow As Integer = 0 To ogdGrpCst.Rows.Count - 1
            If ogdGrpCst.Rows(nrow).Cells(0).Value = True Then
                bChoose = True
            End If
        Next

        If bChoose = True Then
            wExportData.bW_StatusExport = True 'สถานะข้อมูลถูกเลือก

            oW_oDbtblTmpCgpCstExp = W_SEToColumnGroupCustomer()
            Try
                oW_oDbtblTmpCgpCstExp.NewRow()

                For nRow As Integer = 0 To oW_oDbtblTmpCgpCst.Rows.Count - 1
                    If ogdGrpCst.Rows(nRow).Cells(0).Value = True Then

                        Dim oRow As DataRow = oW_oDbtblTmpCgpCstExp.NewRow()
                        oRow("FTSelect") = True
                        oRow("FTCgpCode") = oW_oDbtblTmpCgpCst.Rows(nRow).Item("FTCgpCode")
                        oRow("FTCgpName") = oW_oDbtblTmpCgpCst.Rows(nRow).Item("FTCgpName")
                        oRow("FDDateUpd") = Convert.ToDateTime(oW_oDbtblTmpCgpCst.Rows(nRow).Item("FDDateUpd")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                        oRow("FTTimeUpd") = oW_oDbtblTmpCgpCst.Rows(nRow).Item("FTTimeUpd")
                        oW_oDbtblTmpCgpCstExp.Rows.Add(oRow)

                    End If
                Next

                If cCNVB.nVB_CutLng = 1 Then
                    MsgBox("ระบบทำรายการเลือกข้อมูลสำเร็จ", MsgBoxStyle.Information)
                Else
                    MsgBox("Select information success", MsgBoxStyle.Information)
                End If

                Me.Close()
            Catch ex As Exception

            End Try
        Else
            If cCNVB.nVB_CutLng = 1 Then
                MsgBox("กรุณาเลือกรายการที่ต้องการส่งออก", MsgBoxStyle.Information)
            Else
                MsgBox("Please select information for export", MsgBoxStyle.Information)
            End If
        End If

    End Sub

    Private headerBox As CheckBox
    Private Sub W_SETxCheckboxGrpCstAllow()
        Dim checkboxHeader As CheckBox = New CheckBox()
        Dim rect As Rectangle = ogdGrpCst.GetCellDisplayRectangle(4, -1, True)
        rect.X = 20
        rect.Y = 7
        With checkboxHeader
            .BackColor = Color.Transparent
        End With
        checkboxHeader.Name = "checkboxHeader"
        checkboxHeader.Size = New Size(14, 14)
        checkboxHeader.Location = rect.Location
        checkboxHeader.Checked = True
        AddHandler checkboxHeader.CheckedChanged, AddressOf checkboxHeader_CheckedChanged
        ogdGrpCst.Controls.Add(checkboxHeader)
    End Sub

    Private Sub checkboxHeader_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        headerBox = DirectCast(ogdGrpCst.Controls.Find("checkboxHeader", True)(0), CheckBox)
        For Each row As DataGridViewRow In ogdGrpCst.Rows
            row.Cells(0).Value = headerBox.Checked
        Next

        ogdGrpCst.RefreshEdit()
    End Sub

    Private Sub otbCst_KeyUp(sender As Object, e As KeyEventArgs) Handles otbCst.KeyUp
        If (e.KeyCode = Keys.Enter) Then
            If otbCst.Text <> "" Then
                W_GETdataGridGrpCst(otbCst.Text)
            Else
                W_GETdataGridGrpCst("")
            End If
        End If
    End Sub
End Class