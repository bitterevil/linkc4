﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class cUrsGP
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.omnTalert = New System.Windows.Forms.ToolTip(Me.components)
        Me.ogbPmt = New System.Windows.Forms.GroupBox()
        Me.ogdPmt = New System.Windows.Forms.DataGridView()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.olaSharCode = New System.Windows.Forms.Label()
        Me.olaID = New System.Windows.Forms.Label()
        Me.ocmSearch = New System.Windows.Forms.Button()
        Me.otbCode = New System.Windows.Forms.TextBox()
        Me.olaPmtCode = New System.Windows.Forms.Label()
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.ogbShareMapping = New System.Windows.Forms.GroupBox()
        Me.ogdMapping = New System.Windows.Forms.DataGridView()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.ocmMBtnSearch = New System.Windows.Forms.Button()
        Me.otbPmtMap = New System.Windows.Forms.TextBox()
        Me.olaPmtMap = New System.Windows.Forms.Label()
        Me.ogbPmt.SuspendLayout()
        CType(Me.ogdPmt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel1.SuspendLayout()
        Me.opnMain.SuspendLayout()
        Me.ogbShareMapping.SuspendLayout()
        CType(Me.ogdMapping, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ogbPmt
        '
        Me.ogbPmt.Controls.Add(Me.ogdPmt)
        Me.ogbPmt.Controls.Add(Me.Panel1)
        Me.ogbPmt.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbPmt.Location = New System.Drawing.Point(19, 11)
        Me.ogbPmt.Name = "ogbPmt"
        Me.ogbPmt.Size = New System.Drawing.Size(825, 238)
        Me.ogbPmt.TabIndex = 0
        Me.ogbPmt.TabStop = False
        Me.ogbPmt.Text = "Promotion"
        '
        'ogdPmt
        '
        Me.ogdPmt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdPmt.Location = New System.Drawing.Point(6, 62)
        Me.ogdPmt.Name = "ogdPmt"
        Me.ogdPmt.RowTemplate.Height = 24
        Me.ogdPmt.Size = New System.Drawing.Size(793, 170)
        Me.ogdPmt.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.olaSharCode)
        Me.Panel1.Controls.Add(Me.olaID)
        Me.Panel1.Controls.Add(Me.ocmSearch)
        Me.Panel1.Controls.Add(Me.otbCode)
        Me.Panel1.Controls.Add(Me.olaPmtCode)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(3, 22)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(819, 40)
        Me.Panel1.TabIndex = 0
        '
        'olaSharCode
        '
        Me.olaSharCode.AutoSize = True
        Me.olaSharCode.Location = New System.Drawing.Point(493, 17)
        Me.olaSharCode.Name = "olaSharCode"
        Me.olaSharCode.Size = New System.Drawing.Size(0, 20)
        Me.olaSharCode.TabIndex = 4
        Me.olaSharCode.Visible = False
        '
        'olaID
        '
        Me.olaID.AutoSize = True
        Me.olaID.Location = New System.Drawing.Point(493, 0)
        Me.olaID.Name = "olaID"
        Me.olaID.Size = New System.Drawing.Size(0, 20)
        Me.olaID.TabIndex = 3
        Me.olaID.Visible = False
        '
        'ocmSearch
        '
        Me.ocmSearch.Location = New System.Drawing.Point(391, 5)
        Me.ocmSearch.Name = "ocmSearch"
        Me.ocmSearch.Size = New System.Drawing.Size(101, 29)
        Me.ocmSearch.TabIndex = 2
        Me.ocmSearch.Tag = "2;ค้นหา;Search"
        Me.ocmSearch.Text = "ค้นหา"
        Me.ocmSearch.UseVisualStyleBackColor = True
        '
        'otbCode
        '
        Me.otbCode.Location = New System.Drawing.Point(192, 6)
        Me.otbCode.Name = "otbCode"
        Me.otbCode.Size = New System.Drawing.Size(193, 26)
        Me.otbCode.TabIndex = 1
        '
        'olaPmtCode
        '
        Me.olaPmtCode.AutoSize = True
        Me.olaPmtCode.Location = New System.Drawing.Point(29, 9)
        Me.olaPmtCode.Name = "olaPmtCode"
        Me.olaPmtCode.Size = New System.Drawing.Size(111, 20)
        Me.olaPmtCode.TabIndex = 0
        Me.olaPmtCode.Tag = "2;        รหัสโปรโมชั่น :;Promotion Code :"
        Me.olaPmtCode.Text = "รหัสโปรโมชั่น :"
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.947308!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.05269!))
        Me.opnMain.Controls.Add(Me.ogbPmt, 1, 1)
        Me.opnMain.Controls.Add(Me.ogbShareMapping, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 4
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 244.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 222.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.0!))
        Me.opnMain.Size = New System.Drawing.Size(873, 477)
        Me.opnMain.TabIndex = 2
        '
        'ogbShareMapping
        '
        Me.ogbShareMapping.Controls.Add(Me.ogdMapping)
        Me.ogbShareMapping.Controls.Add(Me.Panel2)
        Me.ogbShareMapping.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbShareMapping.Location = New System.Drawing.Point(19, 255)
        Me.ogbShareMapping.Name = "ogbShareMapping"
        Me.ogbShareMapping.Size = New System.Drawing.Size(828, 215)
        Me.ogbShareMapping.TabIndex = 1
        Me.ogbShareMapping.TabStop = False
        Me.ogbShareMapping.Text = "GP Mapping"
        '
        'ogdMapping
        '
        Me.ogdMapping.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdMapping.Location = New System.Drawing.Point(6, 62)
        Me.ogdMapping.Name = "ogdMapping"
        Me.ogdMapping.RowTemplate.Height = 24
        Me.ogdMapping.Size = New System.Drawing.Size(793, 147)
        Me.ogdMapping.TabIndex = 2
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.ocmMBtnSearch)
        Me.Panel2.Controls.Add(Me.otbPmtMap)
        Me.Panel2.Controls.Add(Me.olaPmtMap)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(3, 22)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(822, 40)
        Me.Panel2.TabIndex = 1
        '
        'ocmMBtnSearch
        '
        Me.ocmMBtnSearch.Location = New System.Drawing.Point(391, 5)
        Me.ocmMBtnSearch.Name = "ocmMBtnSearch"
        Me.ocmMBtnSearch.Size = New System.Drawing.Size(101, 29)
        Me.ocmMBtnSearch.TabIndex = 2
        Me.ocmMBtnSearch.Tag = "2;ค้นหา;Search"
        Me.ocmMBtnSearch.Text = "ค้นหา"
        Me.ocmMBtnSearch.UseVisualStyleBackColor = True
        '
        'otbPmtMap
        '
        Me.otbPmtMap.Location = New System.Drawing.Point(192, 6)
        Me.otbPmtMap.Name = "otbPmtMap"
        Me.otbPmtMap.Size = New System.Drawing.Size(193, 26)
        Me.otbPmtMap.TabIndex = 1
        '
        'olaPmtMap
        '
        Me.olaPmtMap.AutoSize = True
        Me.olaPmtMap.Location = New System.Drawing.Point(29, 9)
        Me.olaPmtMap.Name = "olaPmtMap"
        Me.olaPmtMap.Size = New System.Drawing.Size(111, 20)
        Me.olaPmtMap.TabIndex = 0
        Me.olaPmtMap.Tag = "2;        รหัสโปรโมชั่น :;Promotion Code :"
        Me.olaPmtMap.Text = "รหัสโปรโมชั่น :"
        '
        'cUrsGP
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Name = "cUrsGP"
        Me.Size = New System.Drawing.Size(873, 477)
        Me.ogbPmt.ResumeLayout(False)
        CType(Me.ogdPmt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.opnMain.ResumeLayout(False)
        Me.ogbShareMapping.ResumeLayout(False)
        CType(Me.ogdMapping, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents omnTalert As ToolTip
    Friend WithEvents ogbPmt As GroupBox
    Friend WithEvents ogdPmt As DataGridView
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ocmSearch As Button
    Friend WithEvents otbCode As TextBox
    Friend WithEvents olaPmtCode As Label
    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents olaSharCode As Label
    Friend WithEvents olaID As Label
    Friend WithEvents ogbShareMapping As GroupBox
    Friend WithEvents ogdMapping As DataGridView
    Friend WithEvents Panel2 As Panel
    Friend WithEvents ocmMBtnSearch As Button
    Friend WithEvents otbPmtMap As TextBox
    Friend WithEvents olaPmtMap As Label
End Class
