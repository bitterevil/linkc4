﻿Public Class cTCNTPdtAjpDT
    Public Property FTIphDocNo() As String
    Public Property FNIpdSeqNo() As Long
    Public Property FTPdtCode() As String
    Public Property FTPdtName() As String
    Public Property FTIphDocType() As String
    Public Property FDIphDocDate() As String
    Public Property FTBchCode() As String
    Public Property FTIpdBarCode() As String
    Public Property FTIpdStkCode() As String
    Public Property FCIpdStkFac() As Double
    Public Property FTPgpChain() As String
    Public Property FTPunCode() As String
    Public Property FTIpdUnitName() As String
    Public Property FCIpdFactor() As Double
    Public Property FNIpdAdjLev() As Long
    Public Property FCIpdPriOld() As Double
    Public Property FCIpdPriNew() As Double
    Public Property FNIpdUnitType() As Long
    Public Property FTIpdStaPrc() As String
    Public Property FCIpdCost() As Double
    Public Property FTPdtArticle() As String
    Public Property FTDcsCode() As String
    Public Property FTPszCode() As String
    Public Property FTClrCode() As String
    Public Property FTPszName() As String
    Public Property FTClrName() As String
    Public Property FTPdtNoDis() As String
    Public Property FCIpdDisAvg() As Double
    Public Property FCIpdFootAvg() As Double
    Public Property FCIpdRePackAvg() As Double
    Public Property FTIpdBchTo() As String
    Public Property FTIpdStaPrcStkCrd() As String
    Public Property FDDateUpd() As String
    Public Property FTTimeUpd() As String
    Public Property FTWhoUpd() As String
    Public Property FDDateIns() As String
    Public Property FTTimeIns() As String
    Public Property FTWhoIns() As String
    Public Property FDIphAffect() As String

End Class


