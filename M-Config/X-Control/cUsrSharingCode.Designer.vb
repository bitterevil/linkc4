﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class cUsrSharingCode
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ogbShardis = New System.Windows.Forms.GroupBox()
        Me.opnShardisMid = New System.Windows.Forms.Panel()
        Me.ogdShare = New System.Windows.Forms.DataGridView()
        Me.opnShardisTop = New System.Windows.Forms.Panel()
        Me.btnInsert = New System.Windows.Forms.Button()
        Me.ocmEdit = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.opnMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ogbShardis.SuspendLayout()
        Me.opnShardisMid.SuspendLayout()
        CType(Me.ogdShare, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnShardisTop.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.469136!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.53086!))
        Me.opnMain.Controls.Add(Me.Panel1, 1, 0)
        Me.opnMain.Controls.Add(Me.Panel2, 1, 1)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 2
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 62.66667!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 37.33333!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.opnMain.Size = New System.Drawing.Size(648, 450)
        Me.opnMain.TabIndex = 4
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ogbShardis)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(19, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(626, 276)
        Me.Panel1.TabIndex = 0
        '
        'ogbShardis
        '
        Me.ogbShardis.Controls.Add(Me.opnShardisMid)
        Me.ogbShardis.Controls.Add(Me.opnShardisTop)
        Me.ogbShardis.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbShardis.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbShardis.Location = New System.Drawing.Point(0, 0)
        Me.ogbShardis.Name = "ogbShardis"
        Me.ogbShardis.Size = New System.Drawing.Size(626, 276)
        Me.ogbShardis.TabIndex = 3
        Me.ogbShardis.TabStop = False
        Me.ogbShardis.Text = "Sharing Discount Code"
        '
        'opnShardisMid
        '
        Me.opnShardisMid.Controls.Add(Me.ogdShare)
        Me.opnShardisMid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnShardisMid.Location = New System.Drawing.Point(3, 65)
        Me.opnShardisMid.Name = "opnShardisMid"
        Me.opnShardisMid.Size = New System.Drawing.Size(620, 208)
        Me.opnShardisMid.TabIndex = 1
        '
        'ogdShare
        '
        Me.ogdShare.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdShare.Location = New System.Drawing.Point(14, 0)
        Me.ogdShare.Name = "ogdShare"
        Me.ogdShare.RowTemplate.Height = 24
        Me.ogdShare.Size = New System.Drawing.Size(592, 197)
        Me.ogdShare.TabIndex = 0
        '
        'opnShardisTop
        '
        Me.opnShardisTop.Controls.Add(Me.btnInsert)
        Me.opnShardisTop.Controls.Add(Me.ocmEdit)
        Me.opnShardisTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.opnShardisTop.Location = New System.Drawing.Point(3, 22)
        Me.opnShardisTop.Name = "opnShardisTop"
        Me.opnShardisTop.Size = New System.Drawing.Size(620, 43)
        Me.opnShardisTop.TabIndex = 0
        '
        'btnInsert
        '
        Me.btnInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.btnInsert.Location = New System.Drawing.Point(501, 6)
        Me.btnInsert.Name = "btnInsert"
        Me.btnInsert.Size = New System.Drawing.Size(105, 31)
        Me.btnInsert.TabIndex = 4
        Me.btnInsert.Text = "เพิ่ม"
        Me.btnInsert.UseVisualStyleBackColor = True
        '
        'ocmEdit
        '
        Me.ocmEdit.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!)
        Me.ocmEdit.Location = New System.Drawing.Point(642, 8)
        Me.ocmEdit.Name = "ocmEdit"
        Me.ocmEdit.Size = New System.Drawing.Size(97, 29)
        Me.ocmEdit.TabIndex = 3
        Me.ocmEdit.Text = "แก้ไข"
        Me.ocmEdit.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(19, 285)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(626, 162)
        Me.Panel2.TabIndex = 1
        '
        'cUsrSharingCode
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Name = "cUsrSharingCode"
        Me.Size = New System.Drawing.Size(648, 450)
        Me.opnMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ogbShardis.ResumeLayout(False)
        Me.opnShardisMid.ResumeLayout(False)
        CType(Me.ogdShare, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnShardisTop.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ogbShardis As GroupBox
    Friend WithEvents opnShardisMid As Panel
    Friend WithEvents ogdShare As DataGridView
    Friend WithEvents opnShardisTop As Panel
    Friend WithEvents btnInsert As Button
    Friend WithEvents ocmEdit As Button
    Friend WithEvents Panel2 As Panel
End Class
