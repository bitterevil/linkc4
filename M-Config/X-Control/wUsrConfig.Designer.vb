﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wUsrConfig
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wUsrConfig))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.olaPath = New System.Windows.Forms.Label()
        Me.otvDir = New System.Windows.Forms.TreeView()
        Me.imageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.ocmOk = New System.Windows.Forms.Button()
        Me.ocmCancel = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(178, -124)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(24, 10)
        Me.Button1.TabIndex = 3
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'olaPath
        '
        Me.olaPath.AutoSize = True
        Me.olaPath.Location = New System.Drawing.Point(228, -132)
        Me.olaPath.Name = "olaPath"
        Me.olaPath.Size = New System.Drawing.Size(29, 13)
        Me.olaPath.TabIndex = 5
        Me.olaPath.Text = "Path"
        '
        'otvDir
        '
        Me.otvDir.BackColor = System.Drawing.SystemColors.Info
        Me.otvDir.Dock = System.Windows.Forms.DockStyle.Top
        Me.otvDir.ImageIndex = 0
        Me.otvDir.ImageList = Me.imageList1
        Me.otvDir.Indent = 25
        Me.otvDir.Location = New System.Drawing.Point(0, 0)
        Me.otvDir.Name = "otvDir"
        Me.otvDir.SelectedImageIndex = 0
        Me.otvDir.Size = New System.Drawing.Size(316, 255)
        Me.otvDir.TabIndex = 7
        '
        'imageList1
        '
        Me.imageList1.ImageStream = CType(resources.GetObject("imageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.imageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.imageList1.Images.SetKeyName(0, "003-folder_1.gif")
        '
        'ocmOk
        '
        Me.ocmOk.Location = New System.Drawing.Point(148, 261)
        Me.ocmOk.Name = "ocmOk"
        Me.ocmOk.Size = New System.Drawing.Size(75, 23)
        Me.ocmOk.TabIndex = 6
        Me.ocmOk.Text = "OK"
        Me.ocmOk.UseVisualStyleBackColor = True
        '
        'ocmCancel
        '
        Me.ocmCancel.Location = New System.Drawing.Point(229, 261)
        Me.ocmCancel.Name = "ocmCancel"
        Me.ocmCancel.Size = New System.Drawing.Size(75, 23)
        Me.ocmCancel.TabIndex = 8
        Me.ocmCancel.Text = "Cancel"
        Me.ocmCancel.UseVisualStyleBackColor = True
        '
        'wUsrConfig
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(316, 313)
        Me.Controls.Add(Me.ocmCancel)
        Me.Controls.Add(Me.otvDir)
        Me.Controls.Add(Me.ocmOk)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.olaPath)
        Me.Name = "wUsrConfig"
        Me.Text = "Browse For Folder"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents olaPath As System.Windows.Forms.Label
    Private WithEvents otvDir As System.Windows.Forms.TreeView
    Private WithEvents imageList1 As System.Windows.Forms.ImageList
    Friend WithEvents ocmOk As System.Windows.Forms.Button
    Friend WithEvents ocmCancel As System.Windows.Forms.Button
End Class
