﻿Public Class wSettingSharing
    Property nW_IndexMenu As Integer = 0
    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        Me.oW_UsrCtr = New cControlGP()
    End Sub

    Private oW_UsrCtr As cControlGP
    Private bW_Close As Boolean = False



    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub olvMenu_SelectedIndexChanged_1(sender As Object, e As EventArgs) Handles olvMenu.SelectedIndexChanged
        For Each oItem As ListViewItem In Me.olvMenu.Items
            oItem.ForeColor = Color.Black
            oItem.BackColor = Color.White
            If oItem.Selected = True Then
                Dim nObj = CType(oItem.Index + 1, cControlGP.eType)
                Me.Cursor = Cursors.WaitCursor
                Me.oW_UsrCtr.W_GETxCurCtl(Me.opnForm, nObj)
                Me.Cursor = Cursors.Default
                Me.olaDesForm.Text = Me.oW_UsrCtr.W_GETtCurDes
                oItem.ForeColor = Color.White
                oItem.BackColor = Color.RoyalBlue
                Exit Sub
            End If
        Next
    End Sub

    Private Sub wSettingSharing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        If Me.olvMenu.Items.Count > 0 Then Me.olvMenu.Items(nW_IndexMenu).Selected = True
    End Sub
End Class