﻿Imports System.Net.Http
Imports Newtonsoft.Json

Public Class cmlExpSale

    <JsonProperty("customer_id")>
    Property FTCstID As String

    <JsonProperty("Date")>
    Property FTDate As String

    <JsonProperty("location_id")>
    Property FTlocationID As String

    <JsonProperty("currency_id")>
    Property FTCurrency As String

    <JsonProperty("exchange_rate")>
    Property FTExchange As String

    <JsonProperty("branch_no")>
    Property FTBranchNo As String

    <JsonProperty("pos_no")>
    Property FTPosNo As String

    <JsonProperty("document_number")>
    Property FTDocNo As String

    <JsonProperty("document_type")>
    Property FTDocType As String

    <JsonProperty("sub_total")>
    Property FTSubTotal As String

    <JsonProperty("tax_total")>
    Property FTTaxTotal As String

    <JsonProperty("revenue")>
    Property FTrevenue As String

    <JsonProperty("total_rounding")>
    Property FTTotalrounding As String

    <JsonProperty("total")>
    Property FTTotal As String

    <JsonProperty("cashsum")>
    Property FTCashSum As String

    <JsonProperty("creditsumcard1")>
    Property FTCrd1 As String

    <JsonProperty("creditsumcard2")>
    Property FTCrd2 As String

    <JsonProperty("creditsumcard3")>
    Property FTCrd3 As String

    <JsonProperty("othersum")>
    Property FTOthSum As String

    <JsonProperty("vouchersum")>
    Property FTVocherSum As String

    <JsonProperty("couponsum")>
    Property FTCouponSum As String

    <JsonProperty("member")>
    Property FTMember As String

    Property productdetails() As List(Of cmlExpSalDT)


End Class
