﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wHisLog
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wHisLog))
        Me.otcExport = New System.Windows.Forms.TabControl()
        Me.otmTitle = New System.Windows.Forms.TabPage()
        Me.ogdLog = New System.Windows.Forms.DataGridView()
        Me.opnTable = New System.Windows.Forms.TableLayoutPanel()
        Me.opnBottom = New System.Windows.Forms.Panel()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ocmClear = New System.Windows.Forms.Button()
        Me.odtDate = New System.Windows.Forms.DateTimePicker()
        Me.olaDate = New System.Windows.Forms.Label()
        Me.otcExport.SuspendLayout()
        Me.otmTitle.SuspendLayout()
        CType(Me.ogdLog, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.opnTable.SuspendLayout()
        Me.opnBottom.SuspendLayout()
        Me.SuspendLayout()
        '
        'otcExport
        '
        Me.otcExport.Controls.Add(Me.otmTitle)
        Me.otcExport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.otcExport.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.25!)
        Me.otcExport.Location = New System.Drawing.Point(37, 4)
        Me.otcExport.Margin = New System.Windows.Forms.Padding(4)
        Me.otcExport.Name = "otcExport"
        Me.otcExport.SelectedIndex = 0
        Me.otcExport.Size = New System.Drawing.Size(980, 443)
        Me.otcExport.TabIndex = 0
        Me.otcExport.Tag = "2;ส่งออก;Export"
        '
        'otmTitle
        '
        Me.otmTitle.Controls.Add(Me.ogdLog)
        Me.otmTitle.Location = New System.Drawing.Point(4, 27)
        Me.otmTitle.Margin = New System.Windows.Forms.Padding(4)
        Me.otmTitle.Name = "otmTitle"
        Me.otmTitle.Padding = New System.Windows.Forms.Padding(4)
        Me.otmTitle.Size = New System.Drawing.Size(972, 412)
        Me.otmTitle.TabIndex = 0
        Me.otmTitle.Tag = "2;ส่งออก;Export"
        Me.otmTitle.Text = "Export"
        Me.otmTitle.UseVisualStyleBackColor = True
        '
        'ogdLog
        '
        Me.ogdLog.AllowUserToAddRows = False
        Me.ogdLog.AllowUserToDeleteRows = False
        Me.ogdLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdLog.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogdLog.Location = New System.Drawing.Point(4, 4)
        Me.ogdLog.Margin = New System.Windows.Forms.Padding(4)
        Me.ogdLog.Name = "ogdLog"
        Me.ogdLog.Size = New System.Drawing.Size(964, 404)
        Me.ogdLog.TabIndex = 0
        '
        'opnTable
        '
        Me.opnTable.ColumnCount = 3
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.255208!))
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 96.74479!))
        Me.opnTable.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23.0!))
        Me.opnTable.Controls.Add(Me.otcExport, 1, 0)
        Me.opnTable.Controls.Add(Me.opnBottom, 1, 1)
        Me.opnTable.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnTable.Location = New System.Drawing.Point(0, 0)
        Me.opnTable.Margin = New System.Windows.Forms.Padding(4)
        Me.opnTable.Name = "opnTable"
        Me.opnTable.RowCount = 2
        Me.opnTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.43374!))
        Me.opnTable.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.56627!))
        Me.opnTable.Size = New System.Drawing.Size(1045, 511)
        Me.opnTable.TabIndex = 1
        '
        'opnBottom
        '
        Me.opnBottom.Controls.Add(Me.ocmClose)
        Me.opnBottom.Controls.Add(Me.ocmClear)
        Me.opnBottom.Controls.Add(Me.odtDate)
        Me.opnBottom.Controls.Add(Me.olaDate)
        Me.opnBottom.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnBottom.Location = New System.Drawing.Point(37, 455)
        Me.opnBottom.Margin = New System.Windows.Forms.Padding(4)
        Me.opnBottom.Name = "opnBottom"
        Me.opnBottom.Size = New System.Drawing.Size(980, 52)
        Me.opnBottom.TabIndex = 1
        '
        'ocmClose
        '
        Me.ocmClose.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClose.Location = New System.Drawing.Point(843, 9)
        Me.ocmClose.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(129, 33)
        Me.ocmClose.TabIndex = 5
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ocmClear
        '
        Me.ocmClear.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(222, Byte))
        Me.ocmClear.Location = New System.Drawing.Point(695, 9)
        Me.ocmClear.Margin = New System.Windows.Forms.Padding(4)
        Me.ocmClear.Name = "ocmClear"
        Me.ocmClear.Size = New System.Drawing.Size(140, 33)
        Me.ocmClear.TabIndex = 4
        Me.ocmClear.Tag = "2;ล้างค่า;Clear Log"
        Me.ocmClear.Text = "Clear Log"
        Me.ocmClear.UseVisualStyleBackColor = True
        '
        'odtDate
        '
        Me.odtDate.CalendarFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.odtDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.odtDate.Location = New System.Drawing.Point(521, 10)
        Me.odtDate.Margin = New System.Windows.Forms.Padding(4)
        Me.odtDate.Name = "odtDate"
        Me.odtDate.Size = New System.Drawing.Size(136, 22)
        Me.odtDate.TabIndex = 3
        '
        'olaDate
        '
        Me.olaDate.AutoSize = True
        Me.olaDate.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaDate.Location = New System.Drawing.Point(415, 15)
        Me.olaDate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.olaDate.Name = "olaDate"
        Me.olaDate.Size = New System.Drawing.Size(92, 20)
        Me.olaDate.TabIndex = 0
        Me.olaDate.Tag = "2;         วันที่ :;SelectDate :"
        Me.olaDate.Text = "SelectDate"
        '
        'wHisLog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1045, 511)
        Me.Controls.Add(Me.opnTable)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wHisLog"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ประวัติการใช้งาน;History Log"
        Me.Text = "History Log"
        Me.otcExport.ResumeLayout(False)
        Me.otmTitle.ResumeLayout(False)
        CType(Me.ogdLog, System.ComponentModel.ISupportInitialize).EndInit()
        Me.opnTable.ResumeLayout(False)
        Me.opnBottom.ResumeLayout(False)
        Me.opnBottom.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents otcExport As TabControl
    Friend WithEvents otmTitle As TabPage
    Friend WithEvents ogdLog As DataGridView
    Friend WithEvents opnTable As TableLayoutPanel
    Friend WithEvents opnBottom As Panel
    Friend WithEvents ocmClose As Button
    Friend WithEvents ocmClear As Button
    Friend WithEvents odtDate As DateTimePicker
    Friend WithEvents olaDate As Label
End Class
