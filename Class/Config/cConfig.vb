﻿Imports System.IO

Namespace AdaConfig

    Public Class cConfig

        Public Shared Property bStaAuto As Boolean = False
        Public Shared Property tAppPath As String = Application.StartupPath
        Private Shared oXmlCfg As XElement
        Public Shared oXmlCtgAPI As XElement
        Public Shared oXmlCtgShareDis As XElement

        Public Shared ReadOnly Property AdaConfigXmlAPI As String
            Get
                'เช็คว่ามี ไฟล์ config ไหม ถ้าไม่มีให้เข้าฟังชั่น
                If File.Exists(tAppPath & "\Config\AdaConfigAPI.xml") = False Then
                    Dim oFrmSetting As New wSetting()
                    oApplication = New oStrucApplication

                    'เปิด From setting เพื่อ กรอกค่า config 
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    End If

                End If

                Return tAppPath & "\Config\AdaConfigAPI.xml"
            End Get
        End Property

        Public Shared ReadOnly Property AdaConfigXmlDiscount As String
            Get
                If File.Exists(tAppPath & "\Config\AdaConfigShareDiscount.xml") = False Then
                    Dim oFrmSetting As New wSetting()
                    oApplication = New oStrucApplication

                    'เปิด From setting เพื่อ กรอกค่า config 
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    End If

                End If

                Return tAppPath & "\Config\AdaConfigShareDiscount.xml"
            End Get
        End Property

        Public Shared ReadOnly Property AdaConfigXml As String
            Get
                'เช็คว่ามี ไฟล์ config ไหม ถ้าไม่มีให้เข้าฟังชั่น
                If File.Exists(tAppPath & "\Config\AdaConfig.xml") = False Then
                    Dim oFrmSetting As New wSetting()
                    oApplication = New oStrucApplication

                    'เปิด From setting เพื่อ กรอกค่า config 
                    If oFrmSetting.ShowDialog() = Windows.Forms.DialogResult.OK Then

                    End If
                End If

                Return tAppPath & "\Config\AdaConfig.xml"
            End Get
        End Property

        Public Shared Sub C_GETxConfigXmlAPI()
            Try
                Dim tType As String = ""
                oXmlCtgAPI = XElement.Load(AdaConfigXmlAPI)
                Dim oQryConfigAPI = From c In oXmlCtgAPI.Elements("APIDetail") Select c
                For Each oItem In oQryConfigAPI
                    With oConfigAPI
                        ' .tUrlSale = oItem.Attribute("URL").Value
                        .tMethod = oItem.Attribute("Method").Value
                        .tContentType = oItem.Attribute("ContentType").Value
                        .tKey = oItem.Attribute("ApplicationKey").Value
                        .tCusKey = oItem.Attribute("CONSUMER_KEY").Value
                        .tCusSecret = oItem.Attribute("CONSUMER_SECRET").Value
                        .tTokenSecret = oItem.Attribute("TOKEN_SECRET").Value
                    End With
                Next

                Dim oQryUrl = From c In oXmlCtgAPI.Elements("APIUrlSale") Select c
                For Each oUrlSale In oQryUrl
                    oConfigAPI.tUrlSaleTran = oUrlSale.Attribute("URL").Value
                Next

                Dim oQryUrlReturn = From c In oXmlCtgAPI.Elements("APIUrlMember") Select c
                For Each oUrlRe In oQryUrlReturn
                    oConfigAPI.tUrlMember = oUrlRe.Attribute("URL").Value
                Next

                Dim oQryUrlAll = From c In oXmlCtgAPI.Elements("APIUrlMemberGrp") Select c
                For Each oUrlReAll In oQryUrlAll
                    oConfigAPI.tUrlGrpMember = oUrlReAll.Attribute("URL").Value
                Next

                Dim QryExp = From c In oXmlCtgAPI.Elements("ExportCstFirst") Select c
                For Each oType In QryExp
                    oConfigAPI.tExportCst = oType.Attribute("Type").Value
                Next

                Dim QryGrp = From c In oXmlCtgAPI.Elements("ExportGrpFirst") Select c
                For Each oType In QryGrp
                    oConfigAPI.tExportGrp = oType.Attribute("Type").Value
                Next

                Dim QrySale = From c In oXmlCtgAPI.Elements("ExportSaleFirst") Select c
                For Each oType In QrySale
                    oConfigAPI.tExportSale = oType.Attribute("Type").Value
                Next

                oQryConfigAPI = Nothing

            Catch ex As Exception

            End Try

        End Sub

        'Load Config AdaConfig.xml
        Public Shared Sub C_GETxConfigXml()
            Try
                'Load ไฟล์ config ใส่ตัวแปร
                oXmlCfg = XElement.Load(AdaConfigXml)
                Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                For Each oItem In oQryApp
                    With oApplication
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPass = oItem.Attribute("Pass").Value.SP_DecryptData
                        Try
                            .nLanguage = CInt(oItem.Attribute("Language").Value)
                        Catch ex As Exception
                            .nLanguage = 2 'Default Eng=2
                        End Try
                    End With
                Next
                oQryApp = Nothing

                Dim oQryConnSource = From c In oXmlCfg.Elements("Connection") Select c
                For Each oItem In oQryConnSource
                    With oConnSource
                        .tServer = oItem.Attribute("Server").Value
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPassword = oItem.Attribute("Password").Value.SP_DecryptData
                        .tCatalog = oItem.Attribute("Catalog").Value
                        .nTimeOut = oItem.Attribute("TimeOut").Value
                    End With
                Next

                oQryConnSource = From c In oXmlCfg.Elements("ConnectionMem") Select c
                For Each oItem In oQryConnSource
                    With oMemConnSource
                        .tServer = oItem.Attribute("Server").Value
                        .tUser = oItem.Attribute("User").Value.SP_DecryptData
                        .tPassword = oItem.Attribute("Password").Value.SP_DecryptData
                        .tCatalog = oItem.Attribute("Catalog").Value
                        .nTimeOut = oItem.Attribute("TimeOut").Value
                    End With
                Next

                oQryConnSource = Nothing
                Dim oQrySeparator = From c In oXmlCfg.Elements("Separator") Select c
                For Each oItem In oQrySeparator
                    With oSeparator
                        Try
                            .nIndex = CInt(oItem.Attribute("Index").Value)
                        Catch ex As Exception
                            .nIndex = 1
                        End Try
                        .tDefine = oItem.Attribute("Define").Value
                    End With
                Next
                oQrySeparator = Nothing

                'Dim oQryImport = From c In oXmlCfg.Elements("Import") Select c
                'For Each oItem In oQryImport
                '    With PathXml
                '        .tImport = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                '    End With
                'Next
                'oQryImport = Nothing

                'Dim oQryExport = From c In oXmlCfg.Elements("Export") Select c
                'For Each oItem In oQryExport
                '    With PathXml
                '        .tExport = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                '    End With
                'Next
                'oQryImport = Nothing


                ''Path sFTP *CH 22-06-2017
                'Dim oQryFTP = From c In oXmlCfg.Elements("FTP") Select c
                'For Each oItem In oQryFTP
                '    With PathXml
                '        .tFTP = tAppPath & "\Config\" & oItem.Attribute("Path").Value
                '    End With
                'Next
                'oQryImport = Nothing

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Shared Sub C_GETxSharingDiscount()
            Try
                Dim tType As String = ""
                oXmlCtgShareDis = XElement.Load(AdaConfigXmlDiscount)
                Dim oQryConfigDis = From c In oXmlCtgAPI.Elements("CompenSate") Select c
                For Each oItem In oQryConfigDis
                    With oDiscountShare
                        .tCompen = oItem.Attribute("ID").Value
                        .tCompenCode = oItem.Attribute("Detail").Value
                    End With
                Next

                Dim oQryUrl = From c In oXmlCtgAPI.Elements("ShareDis") Select c
                For Each oUrlSale In oQryUrl
                    With oDiscountShare
                        .tShareDis = oUrlSale.Attribute("ID").Value
                        .tShareDisCode = oUrlSale.Attribute("Detail").Value
                    End With

                Next

                Dim oQryUrlReturn = From c In oXmlCtgAPI.Elements("MarginOnnet") Select c
                For Each oUrlRe In oQryUrlReturn
                    With oDiscountShare
                        .tMarginNet = oUrlRe.Attribute("ID").Value
                        .tMarginNetCode = oUrlRe.Attribute("Detail").Value
                    End With
                Next

                Dim oQryUrlAll = From c In oXmlCtgAPI.Elements("MarginOnSale") Select c
                For Each oUrlReAll In oQryUrlAll
                    With oDiscountShare
                        .tMarginNetSale = oUrlReAll.Attribute("ID").Value
                        .tMarginNetSaleCode = oUrlReAll.Attribute("Detail").Value
                    End With
                Next

                Dim QryExp = From c In oXmlCtgAPI.Elements("ShareDiscount30") Select c
                For Each oType In QryExp
                    With oDiscountShare
                        .tShareDis30 = oType.Attribute("ID").Value
                        .tShareDis30 = oType.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40 = From c In oXmlCtgAPI.Elements("ShareDiscount40") Select c
                For Each oDis40 In QryDis40
                    With oDiscountShare
                        .tShareDis40 = oDis40.Attribute("ID").Value
                        .tShareDis40Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40_1 = From c In oXmlCtgAPI.Elements("ShareDiscount40_1") Select c
                For Each oDis40 In QryDis40_1
                    With oDiscountShare
                        .tShareDis40_1 = oDis40.Attribute("ID").Value
                        .tShareDis40_1Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40_2 = From c In oXmlCtgAPI.Elements("ShareDiscount40_2") Select c
                For Each oDis40 In QryDis40_2
                    With oDiscountShare
                        .tShareDis40_2 = oDis40.Attribute("ID").Value
                        .tShareDis40_2Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40_3 = From c In oXmlCtgAPI.Elements("ShareDiscount40_3") Select c
                For Each oDis40 In QryDis40_3
                    With oDiscountShare
                        .tShareDis40_3 = oDis40.Attribute("ID").Value
                        .tShareDis40_3Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40_4 = From c In oXmlCtgAPI.Elements("ShareDiscount40_4") Select c
                For Each oDis40 In QryDis40_4
                    With oDiscountShare
                        .tShareDis40_4 = oDis40.Attribute("ID").Value
                        .tShareDis40_4Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                Dim QryDis40_5 = From c In oXmlCtgAPI.Elements("ShareDiscount40_5") Select c
                For Each oDis40 In QryDis40_5
                    With oDiscountShare
                        .tShareDis40_5 = oDis40.Attribute("ID").Value
                        .tShareDis40_5Code = oDis40.Attribute("Detail").Value
                    End With
                Next

                oQryConfigDis = Nothing

            Catch ex As Exception

            End Try
        End Sub
        Public Shared Function C_SETbUpdateXmlAPI() As Boolean
            C_SETbUpdateXmlAPI = False
            '==== Load AdaConfigAPI ===='

            Try
                oXmlCtgAPI = XElement.Load(AdaConfigXmlAPI)
                Dim oQryConfig = From c In oXmlCtgAPI.Elements("APIDetail") Select c

                For Each oItem In oQryConfig
                    With oConfigAPI
                        oItem.Attribute("Method").Value = .tMethod
                        oItem.Attribute("ContentType").Value = .tContenttype
                        oItem.Attribute("ApplicationKey").Value = .tKey
                        oItem.Attribute("CONSUMER_KEY").Value = .tCusKey
                        oItem.Attribute("CONSUMER_SECRET").Value = .tCusSecret
                        oItem.Attribute("TOKEN_SECRET").Value = .tTokenSecret
                    End With
                Next

                Dim QryUrlSale = From c In oXmlCtgAPI.Elements("APIUrlSale") Select c

                For Each oSale In QryUrlSale
                    oSale.Attribute("URL").Value = oConfigAPI.tUrlSaleTran
                Next

                Dim QryReturn = From c In oXmlCtgAPI.Elements("APIUrlMember") Select c
                For Each oReturn In QryReturn
                    oReturn.Attribute("URL").Value = oConfigAPI.tUrlMember
                Next

                Dim QryReturnAll = From c In oXmlCtgAPI.Elements("APIUrlMemberGrp") Select c
                For Each oReturnAll In QryReturnAll
                    oReturnAll.Attribute("URL").Value = oConfigAPI.tUrlGrpMember
                Next

                Dim QryExp = From c In oXmlCtgAPI.Elements("ExportCstFirst") Select c
                For Each oType In QryExp
                    oType.Attribute("Type").Value = oConfigAPI.tExportCst
                Next

                Dim QryExpGrp = From c In oXmlCtgAPI.Elements("ExportGrpFirst") Select c
                For Each oType In QryExpGrp
                    oType.Attribute("Type").Value = oConfigAPI.tExportGrp
                Next

                Dim QryExpSale = From c In oXmlCtgAPI.Elements("ExportSaleFirst") Select c
                For Each oType In QryExpSale
                    oType.Attribute("Type").Value = oConfigAPI.tExportSale
                Next

                oQryConfig = Nothing
                oXmlCtgAPI.Save(AdaConfigXmlAPI)
                C_SETbUpdateXmlAPI = True

            Catch ex As Exception

            End Try



        End Function


        'Update Config AdaConfig.xml
        Public Shared Function C_SETbUpdateXml() As Boolean
            C_SETbUpdateXml = False
            Try
                '=== Load AdaConfig ===='
                Try
                    oXmlCfg = XElement.Load(AdaConfigXml)
                    Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                    For Each oItem In oQryApp
                        With oApplication
                            oItem.Attribute("User").Value = .tUser.SP_EncryptData
                            oItem.Attribute("Pass").Value = .tPass.SP_EncryptData
                            oItem.Attribute("Language").Value = .nLanguage.ToString
                        End With
                    Next
                    oQryApp = Nothing
                Catch ex As Exception

                End Try

                Dim oQryConnSource = From c In oXmlCfg.Elements("Connection") Select c
                For Each oItem In oQryConnSource
                    With oConnSource
                        oItem.Attribute("Server").Value = .tServer
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Password").Value = .tPassword.SP_EncryptData
                        oItem.Attribute("Catalog").Value = .tCatalog
                        oItem.Attribute("TimeOut").Value = .nTimeOut
                    End With
                Next
                'Member Database *CH 18-10-2014
                oQryConnSource = From c In oXmlCfg.Elements("ConnectionMem") Select c
                For Each oItem In oQryConnSource
                    With oMemConnSource
                        oItem.Attribute("Server").Value = .tServer
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Password").Value = .tPassword.SP_EncryptData
                        oItem.Attribute("Catalog").Value = .tCatalog
                        oItem.Attribute("TimeOut").Value = .nTimeOut
                    End With
                Next
                oQryConnSource = Nothing

                'Dim oQrySeparator = From c In oXmlCfg.Elements("Separator") Select c
                'For Each oItem In oQrySeparator
                '    With oSeparator
                '        oItem.Attribute("Index").Value = .nIndex
                '        oItem.Attribute("Define").Value = .tDefine
                '    End With
                'Next
                ' oQrySeparator = Nothing

                oXmlCfg.Save(AdaConfigXml)
                C_SETbUpdateXml = True

                '=== End  Load  AdaConfig ===='
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function

        Public Shared Function C_SETbUpdateLang() As Boolean
            C_SETbUpdateLang = False
            Try
                oXmlCfg = XElement.Load(AdaConfigXml)
                Dim oQryApp = From c In oXmlCfg.Elements("Application") Select c
                For Each oItem In oQryApp
                    With oApplication
                        oItem.Attribute("User").Value = .tUser.SP_EncryptData
                        oItem.Attribute("Pass").Value = .tPass.SP_EncryptData
                        oItem.Attribute("Language").Value = .nLanguage.ToString
                    End With
                Next
                oQryApp = Nothing

                oXmlCfg.Save(AdaConfigXml)
                C_SETbUpdateLang = True
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function

        'Test SQL Connection Source
        Public Shared Function C_CHKbSQLSourceConnect() As Boolean
            C_CHKbSQLSourceConnect = False
            Try
                Using oConn As New SqlClient.SqlConnection(tSQLConnSourceString)
                    oConn.Open()
                    oConn.Close()
                    C_CHKbSQLSourceConnect = True
                End Using
            Catch ex As Exception
            End Try
        End Function

        'Default Config
        Public Shared oApplication As New oStrucApplication
        Structure oStrucApplication
            Dim tUser As String
            Dim tPass As String
            Dim nLanguage As Integer
        End Structure

        Public Shared oConnSource As New oStrucConnection

        Structure oStrucConnection
            Dim tServer As String
            Dim tUser As String
            Dim tPassword As String
            Dim tCatalog As String
            Dim nTimeOut As Integer
        End Structure

        Public Shared oConfigAPI As New oStrucConfigAPI
        Structure oStrucConfigAPI
            Dim tKey As String
            Dim tMethod As String
            Dim tContenttype As String
            Dim tUrlMember As String
            Dim tUrlGrpMember As String
            Dim tUrlSaleTran As String
            Dim tExportCst As String
            Dim tExportGrp As String
            Dim tExportSale As String
            Dim tCusKey As String
            Dim tCusSecret As String
            Dim tTokenSecret As String

        End Structure

        Public Shared oDiscountShare As New oStrucDiscount
        Structure oStrucDiscount
            Dim tCompen As String
            Dim tCompenCode As String
            Dim tShareDis As String
            Dim tShareDisCode As String
            Dim tMarginNet As String
            Dim tMarginNetCode As String
            Dim tMarginNetSale As String
            Dim tMarginNetSaleCode As String
            Dim tShareDis30 As String
            Dim tShareDis30Code As String
            Dim tShareDis40 As String
            Dim tShareDis40Code As String
            Dim tShareDis40_1 As String
            Dim tShareDis40_1Code As String
            Dim tShareDis40_2 As String
            Dim tShareDis40_2Code As String
            Dim tShareDis40_3 As String
            Dim tShareDis40_3Code As String
            Dim tShareDis40_4 As String
            Dim tShareDis40_4Code As String
            Dim tShareDis40_5 As String
            Dim tShareDis40_5Code As String
        End Structure

        '*CH 18-10-2014
        Public Shared oMemConnSource As New oStrucMemConnection
        Structure oStrucMemConnection
            Dim tServer As String
            Dim tUser As String
            Dim tPassword As String
            Dim tCatalog As String
            Dim nTimeOut As Integer
        End Structure

        Public Shared oSeparator As New oStrucSeparator
        Structure oStrucSeparator
            Dim nIndex As Integer
            Dim tDefine As String
        End Structure

        Public Shared PathXml As New oPath
        Structure oPath
            Dim tImport As String
            Dim tExport As String
            Dim tFTP As String
        End Structure

        'Upload promotion '*CH 22-12-2014
        Public Shared UploadPmt As New oUploadPmt
        Structure oUploadPmt
            Dim tPathPmt As String
            Dim tTypePmt As String
        End Structure

        Public Shared ReadOnly Property tSeparator As String
            Get
                Dim tChr As String = ""
                Select Case oSeparator.nIndex
                    Case 0
                        tChr = ","
                    Case 1
                        tChr = "|"
                    Case 2
                        tChr = vbTab
                    Case 3
                        tChr = ";"
                    Case 4
                        tChr = oSeparator.tDefine
                End Select
                Return tChr
            End Get
        End Property

        Public Shared ReadOnly Property oConfigXml As cConfigXml
            Get
                oConfigXml = New cConfigXml
                '==Import==
                Dim bCreateNew As Boolean = False

                Dim oXEImport As XElement = _
                <Config>
                    <Import Name="Inbox" Path="" Log="" Backup=""></Import>
                    <Import Name="Config" Split="5000" CountIndex="100000"></Import>
                </Config>

                If File.Exists(PathXml.tImport) = False Then
                    'ไม่พบ File Config สร้างใหม่
                    Dim aXml = (From c In oXEImport.Elements("Import") Select c).FirstOrDefault
                    aXml.Attribute("Path").Value = Application.StartupPath & "\Inbox"
                    aXml.Attribute("Log").SetValue(Application.StartupPath & "\Log")
                    aXml.Attribute("Backup").SetValue(Application.StartupPath & "\Backup")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEImport)
                    oNewXml.Save(PathXml.tImport)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oImportXml As XElement = XElement.Load(PathXml.tImport)
                Dim aImportDefaultXml = oXEImport.Elements("Import").Attributes.Where(Function(c) c.Name = "Name")
                Dim aImportFileXml = oImportXml.Elements("Import").Attributes.Where(Function(c) c.Name = "Name")
                Dim bUpdate As Boolean = False

                For Each oItemDef In aImportDefaultXml
                    Dim oItemChk = aImportFileXml.Where(Function(c) c.Value = oItemDef.Value)
                    If oItemChk.Count = 0 Then
                        Dim oXEInsert = oXEImport.Elements("Import").Attributes.Where(Function(c) c.Name = "Name" And c.Value = oItemDef.Value)
                        Dim oAttr As XElement
                        Select Case oItemDef.Value
                            Case "Inbox"
                                oAttr = <Import Name="Inbox" Path="" Log="" Backup=""></Import>
                                oImportXml.Add(oAttr)
                            Case "Config"
                                oAttr = <Import Name="Config" Split="5000" CountIndex="100000"></Import>
                                oImportXml.Add(oAttr)
                        End Select
                        bUpdate = True
                    End If
                Next

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oImportXml)
                    oNewXml.Save(PathXml.tImport)
                    oImportXml = XElement.Load(PathXml.tImport)
                End If

                Dim aEmImport = From c In oImportXml.Elements("Import") Select c
                'Inbox
                Dim oInbox = aEmImport.Where(Function(c) c.Attribute("Name") = "Inbox").FirstOrDefault
                Dim oConfig = aEmImport.Where(Function(c) c.Attribute("Name") = "Config").FirstOrDefault
                Dim oSkyOutbox = aEmImport.Where(Function(c) c.Attribute("Name") = "AdaSkyOutbox").FirstOrDefault

                With oConfigXml
                    .tInbox = oInbox.Attribute("Path").Value
                    .tLog = oInbox.Attribute("Log").Value
                    .tBackup = oInbox.Attribute("Backup").Value
                    .nSplit = oConfig.Attribute("Split").Value
                    .nCountIndex = oConfig.Attribute("CountIndex").Value
                    If .nSplit < 1000 Then
                        .nSplit = 1000
                    End If

                    .tTemp = Application.StartupPath & "\Temp"

                    'Check Folder
                    If Directory.Exists(.tInbox) = False Then Directory.CreateDirectory(.tInbox)
                    If Directory.Exists(.tLog) = False Then Directory.CreateDirectory(.tLog)
                    If Directory.Exists(.tBackup) = False Then Directory.CreateDirectory(.tBackup)
                    If Directory.Exists(.tTemp) = False Then Directory.CreateDirectory(.tTemp)
                End With

                '==Export==


                'ไม่พบ File Config สร้างใหม่
                Dim oXEExport As XElement = _
                <Config>
                    <Export Name="Outbox" Path=""></Export>
                </Config>
                If IO.File.Exists(PathXml.tExport) = False Then
                    Dim aXml = (From c In oXEExport.Elements("Export") Select c).FirstOrDefault
                    aXml.Attribute("Path").SetValue(Application.StartupPath & "\Outbox")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEExport)
                    oNewXml.Save(PathXml.tExport)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oExportXml As XElement = XElement.Load(PathXml.tExport)

                Dim aExportDefaultXml = oXEExport.Elements("Export").Attributes.Where(Function(c) c.Name = "Name")
                Dim aExportFileXml = oExportXml.Elements("Export").Attributes.Where(Function(c) c.Name = "Name")
                bUpdate = False

                For Each oItemDef In aExportDefaultXml
                    Dim oItemChk = aExportFileXml.Where(Function(c) c.Value = oItemDef.Value)
                    If oItemChk.Count = 0 Then
                        Dim oXEInsert = oXEImport.Elements("Export").Attributes.Where(Function(c) c.Name = "Name" And c.Value = oItemDef.Value)
                        Dim oAttr As XElement
                        Select Case oItemDef.Value
                            Case "Inbox"
                                oAttr = <Export Name="Outbox" Path=""></Export>
                                oExportXml.Add(oAttr)
                        End Select
                    End If
                Next

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oExportXml)
                    oNewXml.Save(PathXml.tExport)
                    oExportXml = XElement.Load(PathXml.tExport)
                End If

                Dim aEmExport = From c In oExportXml.Elements("Export") Select c
                'Outbox
                Dim oOutbox = aEmExport.Where(Function(c) c.Attribute("Name") = "Outbox").FirstOrDefault
                With oConfigXml
                    .tOutbox = oOutbox.Attribute("Path").Value
                    'Check Folder
                    If Directory.Exists(.tOutbox) = False Then Directory.CreateDirectory(.tOutbox)
                End With

                '==FTP=='
                Dim oXEFtp As XElement =
                <Config>
                    <FTP Name="Bound" InFolder="" OutFolder="" Reconcile=""></FTP>
                    <FTP Name="sFTP" HostName="" UserName="" Password="" PortNumber=""></FTP>
                    <FTP Name="Bound" CashTransfer="" Expense=""></FTP>
                </Config>

                If File.Exists(PathXml.tFTP) = False Then
                    'ไม่พบ File Config สร้างใหม่
                    Dim aXml = (From c In oXEFtp.Elements("FTP") Select c).FirstOrDefault
                    aXml.Attribute("InFolder").Value = "InBound"
                    aXml.Attribute("OutFolder").SetValue("OutBound")
                    aXml.Attribute("Reconcile").SetValue("Reconcile")
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEFtp)
                    oNewXml.Save(PathXml.tFTP)
                    aXml = Nothing
                End If
                '-----------------------------------------------------------------------------------------------
                Dim oFTPXml As XElement = XElement.Load(PathXml.tFTP)
                Dim aFTPFileXml = oFTPXml.Elements("FTP").Attributes.Where(Function(c) c.Name = "Name")

                Dim bAddBound As Boolean = True
                Dim bAddFTP As Boolean = True
                Dim bAddZGLINT As Boolean = True
                For Each oItemDef In aFTPFileXml
                    Select Case oItemDef.Value
                        Case "Bound" : bAddBound = False
                        Case "sFTP" : bAddFTP = False
                        Case "ZGLINT" : bAddZGLINT = False
                    End Select
                Next
                If bAddBound Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="Bound" InFolder="" OutFolder="" Reconcile=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If
                If bAddFTP Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="sFTP" HostName="" UserName="" Password="" PortNumber=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If
                If bAddZGLINT Then
                    Dim oAttr As XElement
                    oAttr = <FTP Name="ZGLINT" CashTransfer="" Expense=""></FTP>
                    oFTPXml.Add(oAttr)
                    bUpdate = True
                End If

                If bUpdate = True Then
                    Dim oNewXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oFTPXml)
                    oNewXml.Save(PathXml.tFTP)
                    oFTPXml = XElement.Load(PathXml.tFTP)
                End If

                Dim aEmFTP = From c In oFTPXml.Elements("FTP") Select c
                Dim oBound = aEmFTP.Where(Function(c) c.Attribute("Name") = "Bound").FirstOrDefault
                Dim osFTP = aEmFTP.Where(Function(c) c.Attribute("Name") = "sFTP").FirstOrDefault
                Dim oZGLINT = aEmFTP.Where(Function(c) c.Attribute("Name") = "ZGLINT").FirstOrDefault

                With oConfigXml
                    .tInFolder = oBound.Attribute("InFolder").Value
                    .tOutFolder = oBound.Attribute("OutFolder").Value
                    .tReconcile = oBound.Attribute("Reconcile").Value
                    .tHostName = osFTP.Attribute("HostName").Value
                    .tUserName = osFTP.Attribute("UserName").Value
                    .tPassword = osFTP.Attribute("Password").Value
                    .tPortNumber = osFTP.Attribute("PortNumber").Value
                    .tCashTransfer = oZGLINT.Attribute("CashTransfer").Value
                    .tExpense = oZGLINT.Attribute("Expense").Value
                End With

            End Get
        End Property

        Public Shared Sub C_CALxUpdateImport(ptInbox As String, ptLog As String, ptBackup As String)
            Try
                Dim oImportXml As XElement = XElement.Load(PathXml.tImport)
                Dim aEmImport = From c In oImportXml.Elements("Import") Select c
                'Inbox
                Dim oInbox = aEmImport.Where(Function(c) c.Attribute("Name") = "Inbox").FirstOrDefault
                oInbox.Attribute("Path").SetValue(ptInbox)
                oInbox.Attribute("Log").SetValue(ptLog)
                oInbox.Attribute("Backup").SetValue(ptBackup)

                oImportXml.Save(PathXml.tImport)
                oImportXml = Nothing
                aEmImport = Nothing
                oInbox = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Shared Sub C_CALxUpdateExport(ptOutbox As String)
            Try
                Dim oExportXml As XElement = XElement.Load(PathXml.tExport)
                Dim aEmExport = From c In oExportXml.Elements("Export") Select c
                'Inbox
                Dim oOutbox = aEmExport.Where(Function(c) c.Attribute("Name") = "Outbox").FirstOrDefault
                oOutbox.Attribute("Path").SetValue(ptOutbox)
                oExportXml.Save(PathXml.tExport)
                oExportXml = Nothing
                aEmExport = Nothing
                oOutbox = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub



        ''' <summary>
        ''' Update sFTP Config
        ''' </summary>
        ''' <param name="ptHostName"></param>
        ''' <param name="ptUserName"></param>
        ''' <param name="ptPassword"></param>
        ''' <param name="ptPortNumber"></param>
        ''' <param name="ptInFolder"></param>
        ''' <param name="ptOutFolder"></param>
        Public Shared Sub C_CALxUpdateFTP(ptHostName As String, ptUserName As String, ptPassword As String, ptPortNumber As String,
                                          ptInFolder As String, ptOutFolder As String, ByVal ptReconcileFolder As String,
                                          ByVal ptCashTnf As String, ByVal ptExpense As String)
            Try
                Dim oFtpXml As XElement = XElement.Load(PathXml.tFTP)
                Dim aEmFtp = From oFtp In oFtpXml.Elements("FTP") Select oFtp
                'Bound
                Dim oBound = aEmFtp.Where(Function(c) c.Attribute("Name") = "Bound").FirstOrDefault
                oBound.Attribute("InFolder").SetValue(ptInFolder)
                oBound.Attribute("OutFolder").SetValue(ptOutFolder)
                oBound.Attribute("Reconcile").SetValue(ptReconcileFolder)
                oFtpXml.Save(PathXml.tFTP)

                'sFTP
                Dim osFTP = aEmFtp.Where(Function(c) c.Attribute("Name") = "sFTP").FirstOrDefault
                osFTP.Attribute("HostName").SetValue(ptHostName)
                osFTP.Attribute("UserName").SetValue(ptUserName)
                osFTP.Attribute("Password").SetValue(ptPassword)
                osFTP.Attribute("PortNumber").SetValue(ptPortNumber)
                oFtpXml.Save(PathXml.tFTP)

                'ZGLINT
                Dim oZGLINT = aEmFtp.Where(Function(c) c.Attribute("Name") = "ZGLINT").FirstOrDefault
                oZGLINT.Attribute("CashTransfer").SetValue(ptCashTnf)
                oZGLINT.Attribute("Expense").SetValue(ptExpense)
                oFtpXml.Save(PathXml.tFTP)

                oFtpXml = Nothing
                aEmFtp = Nothing
                oBound = Nothing
                osFTP = Nothing
                oZGLINT = Nothing
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        Public Shared ReadOnly Property tSQLConnSourceString As String
            Get
                Return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Connect Timeout={4};APP=" & My.Application.Info.ProductName.ToString, oConnSource.tServer, oConnSource.tCatalog, oConnSource.tUser, oConnSource.tPassword, 5) 'Connection.nTimeOut
            End Get
        End Property

        'Member Connection *CH 18-10-2014
        Public Shared ReadOnly Property tSQLMemConnSourceString As String
            Get
                Return String.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3};Connect Timeout={4};APP=" & My.Application.Info.ProductName.ToString, oMemConnSource.tServer, oMemConnSource.tCatalog, oMemConnSource.tUser, oMemConnSource.tPassword, 5) 'Connection.nTimeOut
            End Get
        End Property

        ''' <summary>
        ''' Config AdaLinkTCC 
        ''' </summary>
        ''' <value></value>
        ''' <returns>Path Config</returns>
        ''' <remarks>Add Path upload.xml *CH 22-12-2014</remarks>
        Public Shared ReadOnly Property AdaConfigUploadXml As String
            Get
                If File.Exists(tAppPath & "\Config\Upload.xml") = False Then
                    If Directory.Exists(tAppPath & "\Config") = False Then
                        Directory.CreateDirectory(tAppPath & "\Config")
                    End If
                    Dim oXEAdaConfig As XElement = _
                        <config>
                            <Upload Path="" Type="xls,xlsx"></Upload>
                        </config>
                    Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
                    oAdaConfigXml.Save(tAppPath & "\Config\Upload.xml")
                    oAdaConfigXml = Nothing
                    oXEAdaConfig = Nothing
                End If

                Return tAppPath & "\Config\Upload.xml"
            End Get
        End Property

        'Load Config AdaConfigUpload.xml
        Public Shared Sub C_GETxConfigUploadXml()
            Try
                oXmlCfg = XElement.Load(AdaConfigUploadXml)

                'Upload promotion '*CH 22-12-2014
                Dim oQryUpload = From c In oXmlCfg.Elements("Upload") Select c
                For Each oItem In oQryUpload
                    With UploadPmt
                        .tPathPmt = oItem.Attribute("Path").Value
                        .tTypePmt = oItem.Attribute("Type").Value
                    End With
                Next

                oQryUpload = Nothing

            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try
        End Sub

        'Update Config AdaConfigUpload.xml
        Public Shared Function C_SETbUpdateConfigUploadXml(ByVal ptPath As String) As Boolean
            C_SETbUpdateConfigUploadXml = False
            Try
                oXmlCfg = XElement.Load(AdaConfigUploadXml)

                Dim oQryUpload = From c In oXmlCfg.Elements("Upload") Select c
                For Each oItem In oQryUpload
                    oItem.Attribute("Path").Value = ptPath
                    oItem.Attribute("Type").Value = oItem.Attribute("Type").Value
                Next
                oQryUpload = Nothing

                oXmlCfg.Save(AdaConfigUploadXml)
                C_SETbUpdateConfigUploadXml = True
            Catch ex As Exception
                Throw New Exception(ex.Message)
            End Try

        End Function
    End Class

End Namespace


