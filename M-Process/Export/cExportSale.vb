﻿Imports System.Text
Imports Newtonsoft.Json
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Collections.Generic
Imports System.Net
Imports System.Threading.Tasks
Imports System.Web.Script.Serialization
Imports System.IO
Imports System
Imports System.Security.Cryptography.X509Certificates
Imports System.Net.Security
Imports System.Security.Cryptography
Imports System.Runtime.InteropServices

Public Class cExportSale

    Public Shared tC_DocNoProcess As String
    Public Shared oC_DbtblLog As DataTable  'Table สำหรับเก็บข้อมูล Log
    Public unreservedChars As String = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~"
    Public OAuthParameterPrefix As String = "oauth_"
    Public includeVersion As Boolean = True

    Protected Const OAuthVersion As String = "1.0"
    Protected Const OAuthConsumerKeyKey As String = "oauth_consumer_key"
    Protected Const OAuthCallbackKey As String = "oauth_callback"
    Protected Const OAuthVersionKey As String = "oauth_version"
    Protected Const OAuthSignatureMethodKey As String = "oauth_signature_method"
    Protected Const OAuthSignatureKey As String = "oauth_signature"
    Protected Const OAuthTimestampKey As String = "oauth_timestamp"
    Protected Const OAuthNonceKey As String = "oauth_nonce"
    Protected Const OAuthTokenKey As String = "oauth_token"
    Protected Const OAuthTokenSecretKey As String = "oauth_token_secret"
    Protected Const HMACSHA1SignatureType As String = "HMAC-SHA1"
    Protected Const PlainTextSignatureType As String = "PLAINTEXT"
    Protected Const RSASHA1SignatureType As String = "RSA-SHA1"


    'Public Class Extensions
    '    Function AsJson(ByVal o As Object) As StringContent
    '        Return New StringContent(JsonConvert.SerializeObject(o), Encoding.UTF8, "application/json")
    '    End Function
    'End Class


    ''' <summary>
    '''    Select query sale การขาย
    ''' </summary>
    ''' <param name="ptDateDoc"> วันที่ส่งออก </param>
    ''' <param name="ptDoc">เลขที่ เอกสาร </param>
    ''' <returns></returns>
    Public Function C_EXPbExportSaleCustomer(ByVal ptExpType As String, ByVal ptDocNo As String _
                                             , ByVal ptCstCode As String,
                                             ByVal ptBchCode As String, ByVal ptPosCode As String,
                                             ByVal ptDate As String,
                                             ByVal ptSaleAmt As String) As Boolean
        Dim bStatus As Boolean = False

        Try
            Dim oExpSale As New cmlExpSale
            Dim oDbtblSum As New DataTable
            Dim oDbtblHD As New DataTable
            Dim oDbTblRC As New DataTable
            Dim oDbtblDT As New DataTable
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal


            'เตรียม ข้อมูลใน Table Sale HD 
            oSQL.Clear()
            oSQL.AppendLine(" SELECT CASE HD.FTCstCode WHEN '0' THEN 'AR' + FTBchCode + FTPosCode + '-' + '000001'  ")
            oSQL.AppendLine(" ELSE FTCstCode END AS FTCstCodeFmt, ")
            oSQL.AppendLine(" 'THB' AS FTCurrency,'1' AS FTExchangeRate, ")
            oSQL.AppendLine(" 'IN' AS FTDocType,")
            oSQL.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'N' ELSE 'Y' END AS FTMember, ")
            oSQL.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode,")
            oSQL.AppendLine(" SUM(HD.FCShdVatable) AS FCShdVatable ,SUM(HD.FCShdVat) AS FCShdVat ,SUM(HD.FCShdAftDisChg) AS FCShdAftDisChg, ")
            oSQL.AppendLine(" SUM(HD.FCShdRnd) AS FCShdRnd,SUM(HD.FCShdGrand) AS FCShdGrand ")
            oSQL.AppendLine(" FROM			TPSTSalHD HD")
            oSQL.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSQL.AppendLine(" AND		    HD.FTShdStaPrcDoc = '1' ")
            oSQL.AppendLine(" AND		    HD.FTShdDocType = '1' ")
            oSQL.AppendLine(" AND		    HD.FTCstCode= '" + ptCstCode + "' ")
            oSQL.AppendLine(" AND		    HD.FTBchCode = '" + ptBchCode + "'")
            oSQL.AppendLine(" AND		    HD.FTPosCode = '" + ptPosCode + "' ")
            oSQL.AppendLine(" GROUP BY		FTCstCode,FTBchCode,FTPosCode ")

            oDbtblHD = oDatabase.C_GEToTblSQL(oSQL.ToString())

            'เตรียมข้อมูลของ Detail
            oSQL.Clear()
            oSQL.AppendLine(" SELECT ")
            oSQL.AppendLine("  CONVERT(decimal(18,2), FCsdtB4DisChg - ((FCsdtB4DisChg * TVAT) / (100 + TVAT))) AS 'SaleAmtExVat' , ")
            oSQL.AppendLine("  CONVERT(decimal(18,2),(Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discountamount', ")
            oSQL.AppendLine(" * FROM ( ")
            oSQL.AppendLine(" SELECT DT.FTSdtStkCode,SUM(DT.FCSdtQtyAll) AS FCSdtQtyAll ,SUM(DT.FCSdtVatable) AS FCSdtVatable, ")
            oSQL.AppendLine(" HD.FCShdVATRate,SUM(DT.FCSdtVat) AS FCSdtVat ,SUM(DT.FCSdtVatable + DT.FCSdtVat) AS FCVatAmt,")
            oSQL.AppendLine(" DT.FCSdtSalePrice,DT.FTPmhCode,SUM(DT.FCsdtB4DisChg) AS FCsdtB4DisChg, ")
            oSQL.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT, ")
            oSQL.AppendLine(" SUM(ISNULL(FCSdtDis + FCSdtChg + FCSdtNet+ FCSdtVat ,0)) AS Discount ")
            oSQL.AppendLine(" FROM			    TPSTSalHD HD ")
            oSQL.AppendLine(" LEFT JOIN		    TPSTSalDT DT ON HD.FTShdDocNo = DT.FTShdDocNo ")
            oSQL.AppendLine(" WHERE			    CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSQL.AppendLine(" AND				HD.FTShdStaPrcDoc = '1' ")
            oSQL.AppendLine(" AND				HD.FTShdDocType = '1' ")
            oSQL.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "'  ")
            oSQL.AppendLine(" AND				HD.FTBchCode = '" + ptBchCode + "' ")
            oSQL.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "' ")
            oSQL.AppendLine(" GROUP BY		HD.FTCstCode,HD.FTBchCode,HD.FTPosCode, ")
            oSQL.AppendLine(" HD.FCShdVATRate, ")
            oSQL.AppendLine(" DT.FTSdtStkCode,DT.FCSdtSalePrice,DT.FTPmhCode ")
            oSQL.AppendLine(" ) AS DT ")
            oDbtblDT = oDatabase.C_GEToTblSQL(oSQL.ToString())

            If oDbtblHD.Rows.Count > 0 Then
                C_POStSendApi(oDbtblHD, oDbtblDT, ptExpType, ptDate, ptDocNo, ptSaleAmt)
            Else

            End If

            bStatus = True
            Return bStatus
        Catch ex As Exception
            wExportData.nW_Fail += 1
            Return bStatus
        End Try

    End Function

    Public Function C_EXPbExportReturnCustomer(ByVal ptExpType As String, ByVal ptDocNo As String _
                                             , ByVal ptCstCode As String,
                                             ByVal ptBchCode As String, ByVal ptPosCode As String,
                                             ByVal ptDate As String,
                                             ByVal ptSaleAmt As String) As Boolean
        Dim bStatus As Boolean = False

        Try
            Dim oExpSale As New cmlExpSale
            Dim oDbtblSum As New DataTable
            Dim oDbtblHD As New DataTable
            Dim oDbTblRC As New DataTable
            Dim oDbtblDT As New DataTable
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal


            'เตรียม ข้อมูลใน Table Sale HD 
            oSQL.Clear()
            oSQL.AppendLine(" SELECT CASE HD.FTCstCode WHEN '0' THEN 'AR' + FTBchCode + FTPosCode + '-' + '000001'  ")
            oSQL.AppendLine(" ELSE FTCstCode END AS FTCstCodeFmt, ")
            oSQL.AppendLine(" 'THB' AS FTCurrency,'1' AS FTExchangeRate, ")
            oSQL.AppendLine(" 'RE' AS FTDocType,")
            oSQL.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'N' ELSE 'Y' END AS FTMember, ")
            oSQL.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode,")
            oSQL.AppendLine(" SUM(HD.FCShdVatable) AS FCShdVatable ,SUM(HD.FCShdVat) AS FCShdVat ,SUM(HD.FCShdAftDisChg) AS FCShdAftDisChg, ")
            oSQL.AppendLine(" SUM(HD.FCShdRnd) AS FCShdRnd,SUM(HD.FCShdGrand) AS FCShdGrand ")
            oSQL.AppendLine(" FROM			TPSTSalHD HD")
            oSQL.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSQL.AppendLine(" AND		    HD.FTShdStaPrcDoc = '1' ")
            oSQL.AppendLine(" AND		    HD.FTShdDocType = '9' ")
            oSQL.AppendLine(" AND		    HD.FTCstCode= '" + ptCstCode + "' ")
            oSQL.AppendLine(" AND		    HD.FTBchCode = '" + ptBchCode + "'")
            oSQL.AppendLine(" AND		    HD.FTPosCode = '" + ptPosCode + "' ")
            oSQL.AppendLine(" GROUP BY		FTCstCode,FTBchCode,FTPosCode ")

            oDbtblHD = oDatabase.C_GEToTblSQL(oSQL.ToString())

            'เตรียมข้อมูลของ Detail
            oSQL.Clear()
            oSQL.AppendLine(" SELECT   ")
            oSQL.AppendLine(" CONVERT(decimal(18,2), FCsdtB4DisChg - ((FCsdtB4DisChg * TVAT) / (100 + TVAT))) AS 'SaleAmtExVat' ,")
            oSQL.AppendLine(" CONVERT(decimal(18,2),(Discount - (Discount * TVAT) / (100 + TVAT))) AS 'Discountamount', ")
            oSQL.AppendLine(" * FROM ( ")
            oSQL.AppendLine(" SELECT DT.FTSdtStkCode,SUM(DT.FCSdtQtyAll) AS FCSdtQtyAll ,SUM(DT.FCSdtVatable) AS FCSdtVatable, ")
            oSQL.AppendLine(" HD.FCShdVATRate,SUM(DT.FCSdtVat) AS FCSdtVat ,SUM(DT.FCSdtVatable + DT.FCSdtVat) AS FCVatAmt,")
            oSQL.AppendLine(" DT.FCSdtSalePrice,DT.FTPmhCode,SUM(DT.FCsdtB4DisChg) AS FCsdtB4DisChg, ")
            oSQL.AppendLine(" (SELECT TOP(1) FCCmpVatAmT  FROM TCNMComp) AS TVAT, ")
            oSQL.AppendLine(" SUM(ISNULL(FCSdtDis + FCSdtChg + FCSdtNet+ FCSdtVat ,0)) AS Discount ")
            oSQL.AppendLine(" FROM			    TPSTSalHD HD ")
            oSQL.AppendLine(" LEFT JOIN		    TPSTSalDT DT ON HD.FTShdDocNo = DT.FTShdDocNo ")
            oSQL.AppendLine(" WHERE			    CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSQL.AppendLine(" AND				HD.FTShdStaPrcDoc = '1' ")
            oSQL.AppendLine(" AND				HD.FTShdDocType = '9' ")
            oSQL.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "'  ")
            oSQL.AppendLine(" AND				HD.FTBchCode = '" + ptBchCode + "' ")
            oSQL.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "' ")
            oSQL.AppendLine(" GROUP BY		HD.FTCstCode,HD.FTBchCode,HD.FTPosCode, ")
            oSQL.AppendLine(" HD.FCShdVATRate, ")
            oSQL.AppendLine(" DT.FTSdtStkCode,DT.FCSdtSalePrice,DT.FTPmhCode ")
            oSQL.AppendLine(" ) AS DT ")
            oDbtblDT = oDatabase.C_GEToTblSQL(oSQL.ToString())

            If oDbtblHD.Rows.Count > 0 Then
                C_POStSendApi(oDbtblHD, oDbtblDT, ptExpType, ptDate, ptDocNo, ptSaleAmt)
            Else

            End If

            bStatus = True
            Return bStatus
        Catch ex As Exception
            wExportData.nW_Fail += 1
            Return bStatus
        End Try

    End Function

    Public Function C_EXPbExportCustomer(ByVal ptCstCode As String) As Boolean

        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTbl As New DataTable

            oSQL.Clear()
            oSQL.AppendLine(" SELECT FTCstCode, FTCstName,FTCstTel,")
            oSQL.AppendLine(" FTCstEmail,FTCstFax,FTCstResTel, CASE ISNULL(FTAreCode,'') WHEN '' THEN 'TH' ELSE FTAreCode END AS FTAreCode,  ")
            oSQL.AppendLine(" FTCstAddr,FTCstPostCode,	FTCstCode,FTCstCardID,FTCstCardID ")
            oSQL.AppendLine(" ,FDCstCrdExpire,FTBchCode,FTCstSex,FDCstDob,FCCstRetPoint,FTCpgCode ")
            oSQL.AppendLine(" FROM TCNMCst ")
            oSQL.AppendLine(" WHERE FTCstCode = '" + ptCstCode + "' ")
            oDbTbl = oDatabase.C_GEToTblSQL(oSQL.ToString())
            If oDbTbl.Rows.Count > 0 Then
                C_POStSendApi(oDbTbl, Nothing, "Customer")
            End If

        Catch ex As Exception
            wExportData.nW_Fail += 1
        End Try

    End Function

    Public Function C_EXPbExportGrpCustomer(ByVal ptGrpCode As String) As Boolean

        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTbl As New DataTable

            oSQL.Clear()
            oSQL.AppendLine(" SELECT FTCgpCode,FTCgpName FROM TCNMCstGrp")
            oSQL.AppendLine(" WHERE FTCgpCode = '" + ptGrpCode + "' ")
            oDbTbl = oDatabase.C_GEToTblSQL(oSQL.ToString())
            If oDbTbl.Rows.Count > 0 Then
                C_POStSendApi(oDbTbl, Nothing, "GrpCustomer")
            End If

        Catch ex As Exception
            wExportData.nW_Fail += 1
        End Try

    End Function

    ''' <summary>
    ''' Call Api
    ''' </summary>
    ''' <param name="poDbtbl"> Datatable ของส่วน Header นำมาดึงข้อมูลเพื่อ แปลงเป็น Json และส่งไปยัง API </param>
    ''' <param name="poDbtblDT"> Datatable ของส่วน Detail นำมาดึงข้อมูลเพื่อ แปลงเป็น Json และส่งไปยัง API  </param>
    ''' <returns></returns>
    Public Function C_POStSendApi(ByVal poDbtbl As DataTable, ByVal poDbtblDT As DataTable, ByVal _
                                  ptExptype As String, ByVal ptDate As String, ByVal ptDocNo As String,
                                  ByVal ptSaleAmt As String) As String

        Try
            Dim oJson
            Dim oRespons As New cmlRes   'สำหรับ รับค่าที่ Respons กลับมาจาก API
            Dim oExpSale As New cmlExpSale   'Model ของ ขาย ส่วน Header
            Dim oExpSaleDT As cmlExpSalDT 'Model ของขาย ส่วน Detail 
            Dim oListSaleDT As New List(Of cmlExpSalDT)  ' Model List ของ ขาย
            Dim aoListSale As New List(Of cmlExpSale)
            Dim oResponse As New HttpResponseMessage
            Dim tStatusDesc As String = ""
            Dim tUrl As String = ""
            Dim tCodeStatus As String = ""
            Dim tValueLog As String = ""
            Dim tPoint As String = ""
            Dim tCstCode As String = ""
            Dim tBchCode As String = ""
            Dim tPosCode As String = ""
            Dim oRes As StreamReader
            Dim tLineRes As String

            Dim tTokenID As String = AdaConfig.cConfig.oConfigAPI.tKey  'Token Key for send API
            Dim tTokenSecret As String = AdaConfig.cConfig.oConfigAPI.tTokenSecret 'Token Secret
            Dim tCusKey As String = AdaConfig.cConfig.oConfigAPI.tCusKey 'Cussumer KEY
            Dim tCusSecret As String = AdaConfig.cConfig.oConfigAPI.tCusSecret 'Cussemer Secret

            Select Case ptExptype
                Case "Sale"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlSaleTran
                    'วนข้อมูลใน Detail
                    tCstCode = poDbtbl.Rows(0)("FTCstCode").ToString
                    tBchCode = poDbtbl.Rows(0)("FTBchCode").ToString
                    tPosCode = poDbtbl.Rows(0)("FTPosCode").ToString

                    For nRowDT As Integer = 0 To poDbtblDT.Rows.Count - 1
                        oExpSaleDT = New cmlExpSalDT
                        With oExpSaleDT
                            .FTItemID = poDbtblDT(nRowDT)("FTSdtStkCode").ToString()
                            .FTItemQty = poDbtblDT(nRowDT)("FCSdtQtyAll").ToString()
                            .FTItemRate = poDbtblDT(nRowDT)("SaleAmtExVat").ToString()
                            .FTAmoute = poDbtblDT(nRowDT)("FCSdtVatable").ToString()
                            .FTTaxRate = W_DATtGetDataVat("1", tCstCode, tBchCode, tPosCode, ptDate)
                            .FTTaxAmt = poDbtblDT(nRowDT)("FCSdtVat").ToString()
                            .FTPrice = poDbtblDT(nRowDT)("FCSdtSalePrice").ToString()
                            .FTPmtCode = IIf(poDbtblDT(nRowDT)("FTPmhCode") = "", 0, poDbtblDT(nRowDT)("FTPmhCode"))
                            .FTTotalBeforeDiscount = poDbtblDT(nRowDT)("FCsdtB4DisChg").ToString()
                            .FTTotalDiscount = poDbtblDT(nRowDT)("Discountamount").ToString()
                            .FTGp = W_DATtGetDataGP(poDbtblDT(nRowDT)("FTPmhCode"), "GP")
                            .FTGpPmt = W_DATtGetDataGP(poDbtblDT(nRowDT)("FTPmhCode"), "GPPmt")
                            oListSaleDT.Add(oExpSaleDT)
                        End With
                    Next

                    'ข้อมูลส่วน Header 

                    With oExpSale

                        .FTCstID = poDbtbl.Rows(0)("FTCstCodeFmt").ToString
                        .FTDate = ptDate
                        .FTlocationID = tBchCode
                        .FTCurrency = poDbtbl.Rows(0)("FTCurrency").ToString
                        .FTExchange = poDbtbl.Rows(0)("FTExchangeRate").ToString
                        .FTBranchNo = tBchCode
                        .FTPosNo = tPosCode
                        .FTDocNo = ptDocNo
                        .FTDocType = poDbtbl.Rows(0)("FTDocType").ToString
                        .FTSubTotal = poDbtbl.Rows(0)("FCShdVatable").ToString
                        .FTTaxTotal = poDbtbl.Rows(0)("FCShdVat").ToString
                        .FTrevenue = poDbtbl.Rows(0)("FCShdAftDisChg").ToString
                        .FTTotalrounding = poDbtbl.Rows(0)("FCShdRnd").ToString
                        .FTTotal = poDbtbl.Rows(0)("FCShdGrand").ToString

                        'เงินสด
                        .FTCashSum = W_DATtGetSumPayment("001", ptDate, tCstCode,
                                            tBchCode, tPosCode, "1")

                        'หัวรูด SCB
                        .FTCrd1 = W_DATtGetSumPaymentCredit("001", ptDate, tCstCode,
                                            tBchCode, tPosCode, "1")

                        'หัวรูด BBL
                        .FTCrd2 = W_DATtGetSumPaymentCredit("002", ptDate, tCstCode,
                                             tBchCode, tPosCode, "1")

                        'หัวรูด KBank
                        .FTCrd3 = W_DATtGetSumPaymentCredit("003", ptDate, tCstCode,
                                             tBchCode, tPosCode, "1")

                        'ชำระบัตรเครดิตช่องทางอื่นๆ
                        .FTOthSum = W_DATtGetSumPayment("002", ptDate, tCstCode,
                                            tBchCode, tPosCode, "1")

                        'วอยเชอร์
                        .FTVocherSum = W_DATtGetSumPayment("008", ptDate, tCstCode,
                                             tBchCode, tPosCode, "1")

                        'คูปอง 
                        .FTCouponSum = W_DATtGetSumPayment("004", ptDate, tCstCode,
                                             tBchCode, tPosCode, "1")
                        .FTMember = poDbtbl.Rows(0)("FTMember").ToString
                        .productdetails = oListSaleDT
                    End With
                    aoListSale.Add(oExpSale)
                    oJson = JsonConvert.SerializeObject(aoListSale)

                    'Write log Json
                    wExportData.nC_CreateFileSale += 1
                    C_SETxWriteTextFile(ptDocNo, ptExptype, oJson)


                Case "Return"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlSaleTran
                    'วนข้อมูลใน Detail
                    tCstCode = poDbtbl.Rows(0)("FTCstCode").ToString
                    tBchCode = poDbtbl.Rows(0)("FTBchCode").ToString
                    tPosCode = poDbtbl.Rows(0)("FTPosCode").ToString

                    For nRowDT As Integer = 0 To poDbtblDT.Rows.Count - 1
                        oExpSaleDT = New cmlExpSalDT
                        With oExpSaleDT
                            .FTItemID = poDbtblDT(nRowDT)("FTSdtStkCode").ToString()
                            .FTItemQty = poDbtblDT(nRowDT)("FCSdtQtyAll").ToString()
                            .FTItemRate = ""
                            .FTAmoute = poDbtblDT(nRowDT)("FCSdtVatable").ToString()
                            .FTTaxRate = W_DATtGetDataVat("9", tCstCode, tBchCode, tPosCode, ptDate)
                            .FTTaxAmt = poDbtblDT(nRowDT)("FCSdtVat").ToString()
                            .FTPrice = poDbtblDT(nRowDT)("FCSdtSalePrice").ToString()
                            .FTPmtCode = IIf(W_SETtDbnull(poDbtblDT(nRowDT)("FTPmhCode")) = "", 0, poDbtblDT(nRowDT)("FTPmhCode"))
                            .FTTotalBeforeDiscount = poDbtblDT(nRowDT)("FCsdtB4DisChg").ToString()
                            .FTTotalDiscount = poDbtblDT(nRowDT)("FCsdtB4DisChg").ToString()  'ยังไม่ได้แก้ราคา
                            .FTGp = W_DATtGetDataGP(W_SETtDbnull(poDbtblDT(nRowDT)("FTPmhCode")), "GP")
                            .FTGpPmt = W_DATtGetDataGP(W_SETtDbnull(poDbtblDT(nRowDT)("FTPmhCode")), "GPPmt")
                            oListSaleDT.Add(oExpSaleDT)


                        End With
                    Next

                    'ข้อมูลส่วน Header 

                    With oExpSale
                        .FTCstID = poDbtbl.Rows(0)("FTCstCodeFmt").ToString
                        .FTDate = ptDate
                        .FTlocationID = tBchCode
                        .FTCurrency = poDbtbl.Rows(0)("FTCurrency").ToString
                        .FTExchange = poDbtbl.Rows(0)("FTExchangeRate").ToString
                        .FTBranchNo = tBchCode
                        .FTPosNo = tPosCode
                        .FTDocNo = ptDocNo
                        .FTDocType = poDbtbl.Rows(0)("FTDocType").ToString
                        .FTSubTotal = poDbtbl.Rows(0)("FCShdVatable").ToString
                        .FTTaxTotal = poDbtbl.Rows(0)("FCShdVat").ToString
                        .FTrevenue = poDbtbl.Rows(0)("FCShdAftDisChg").ToString
                        .FTTotalrounding = poDbtbl.Rows(0)("FCShdRnd").ToString
                        .FTTotal = poDbtbl.Rows(0)("FCShdGrand").ToString

                        'เงินสด
                        .FTCashSum = W_DATtGetSumPayment("001", ptDate, tCstCode,
                                            tBchCode, tPosCode, "9")

                        'หัวรูด SCB
                        .FTCrd1 = W_DATtGetSumPaymentCredit("001", ptDate, tCstCode,
                                            tBchCode, tPosCode, "9")

                        'หัวรูด BBL
                        .FTCrd2 = W_DATtGetSumPaymentCredit("002", ptDate, tCstCode,
                                             tBchCode, tPosCode, "9")

                        'หัวรูด KBank
                        .FTCrd3 = W_DATtGetSumPaymentCredit("003", ptDate, tCstCode,
                                             tBchCode, tPosCode, "9")

                        'ชำระบัตรเครดิตช่องทางอื่นๆ
                        .FTOthSum = W_DATtGetSumPayment("002", ptDate, tCstCode,
                                            tBchCode, tPosCode, "9")

                        'วอยเชอร์
                        .FTVocherSum = W_DATtGetSumPayment("008", ptDate, tCstCode,
                                             tBchCode, tPosCode, "9")

                        'คูปอง 
                        .FTCouponSum = W_DATtGetSumPayment("004", ptDate, tCstCode,
                                             tBchCode, tPosCode, "9")
                        .FTMember = poDbtbl.Rows(0)("FTMember").ToString
                        .productdetails = oListSaleDT
                    End With
                    aoListSale.Add(oExpSale)
                    oJson = JsonConvert.SerializeObject(aoListSale)


                    'Write log Json
                    wExportData.nC_CreateFileSale += 1
                    C_SETxWriteTextFile(ptDocNo, ptExptype, oJson)
            End Select


            'Process Call API  แบบ oAuth
            Dim oAuthe As New StringBuilder()
            Dim tTimeStamp As String = (CInt((DateTime.UtcNow - New DateTime(1970, 1, 1)).TotalSeconds)).ToString()

            Dim Nonce As String = "kt9Xt56Ltio"
            Dim oConsumer_Key = Uri.EscapeDataString(tCusKey)
            Dim oCussumer_Secert = Uri.EscapeDataString(tCusSecret)
            Dim oToken_secret = Uri.EscapeDataString(tTokenSecret)
            Dim oToken_ID = Uri.EscapeDataString(tTokenID)

            'Dim oConsumer_Key = Uri.EscapeDataString("74a243228a4dd66f834ab989e3d1c5731d9f541812623402c9d46bbb3d6503a4")
            'Dim oCussumer_Secert = Uri.EscapeDataString("60bcf110e11cc3b0f97669f2a4a81c1405bb935370cf8540d9ed419abbd93664")
            'Dim oToken_secret = Uri.EscapeDataString("92c12ddf792125fb0ae3f56cd3371e91468cbe3036be00496566fac308237860")
            'Dim oToken_ID = Uri.EscapeDataString("76d39fb0d68e7b543bdb95597b89a8a6d86adc7e03344a0598d7b619a75b5bd2")


            '  Dim signature_base_string As String = GetSignatureBaseString(tTimeStamp, Nonce)

            Dim oStringContent = New StringContent(oJson, UnicodeEncoding.UTF8, "application/json")
            Dim oClient = New HttpClient()
            Dim oWebReq As HttpWebRequest
            Dim oWebRes As HttpWebResponse

            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf C_GENbTrustAllCertificateCallback)
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            oWebReq = WebRequest.Create(tUrl)

            'Convert String  to url
            Dim oUri As Uri
            oUri = New Uri(tUrl)
            Dim normalizedUrl As String = ""
            Dim normalizedRequestParameters As String = ""
            Dim tAuth As String = GenerateSignature(oUri, oConsumer_Key, oCussumer_Secert, oToken_ID,
                                                    oToken_secret, "Post", tTimeStamp, Nonce,
                                                    "HMACSHA1",
                                                     normalizedUrl, normalizedRequestParameters)

            oAuthe.Append("OAuth realm=""4975451_SB1"",")
            oAuthe.Append("oauth_consumer_key=""" + tCusKey + """,")
            oAuthe.Append("oauth_token=""" + tTokenID + """,")
            oAuthe.Append("oauth_signature_method=""HMAC-SHA1"",")
            oAuthe.Append("oauth_timestamp=""" + tTimeStamp + """,")
            oAuthe.Append("oauth_nonce=""kt9Xt56Ltio"",")
            oAuthe.Append("oauth_version=""1.0"",")
            oAuthe.Append("oauth_signature=""" + UrlEncode(tAuth) + """")

            oWebReq.Method = "POST"
            oWebReq.ContentType = "application/json"
            oWebReq.Headers.Add("Authorization", oAuthe.ToString())


            Using oStramWrite As New StreamWriter(oWebReq.GetRequestStream())
                With oStramWrite
                    .WriteLine(oJson)
                    .Flush()
                    .Close()
                End With
            End Using
            oWebRes = oWebReq.GetResponse()
            If oWebRes.StatusCode = "200" Then
                oRes = New StreamReader(oWebRes.GetResponseStream())
                tLineRes = oRes.ReadToEnd()
                wExportData.nW_Success += 1
            Else
                oRes = New StreamReader(oWebRes.GetResponseStream())
                tLineRes = oRes.ReadToEnd()
                wExportData.nW_Fail += 1
            End If


            'WriteLog Process
            cLog.C_CALxWriteLogExportSale(oWebRes.StatusCode, tLineRes, ptDocNo, tCstCode, tBchCode, tPosCode, ptSaleAmt)

            Return tStatusDesc
        Catch ex As Exception
            wExportData.nW_Fail += 1
        End Try

    End Function

    Public Shared Function GetSha1Hash(ByVal key As String, ByVal message As String) As String
        Dim encoding = New System.Text.ASCIIEncoding()
        Dim keyBytes As Byte() = encoding.GetBytes(key)
        Dim messageBytes As Byte() = encoding.GetBytes(message)
        Dim Sha1Result As String = String.Empty

        Using SHA1 As HMACSHA1 = New HMACSHA1(keyBytes)
            Dim Hashed = SHA1.ComputeHash(messageBytes)
            Sha1Result = Convert.ToBase64String(Hashed)
        End Using

        Return Sha1Result
    End Function


    Public Function GetSignatureBaseString(ByVal TimeStamp As String, ByVal Nonce As String) As String
        Dim turl As String = "https://rest.netsuite.com/app/site/hosting/restlet.nl?script=239&deploy=1"

        Dim Signature_Base_String As String = "Post"
        Signature_Base_String = Signature_Base_String.ToUpper()


        Signature_Base_String = Signature_Base_String + "&"


        Dim PercentEncodedURL As String = Uri.EscapeDataString(turl)
        Signature_Base_String = Signature_Base_String + PercentEncodedURL


        Signature_Base_String = Signature_Base_String + "&"

        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("oauth_consumer_key=" + "74a243228a4dd66f834ab989e3d1c5731d9f541812623402c9d46bbb3d6503a4")
        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_token=" + "60bcf110e11cc3b0f97669f2a4a81c1405bb935370cf8540d9ed419abbd93664")
        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_signature_method=" + "HMAC-SHA1")
        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_timestamp=" + TimeStamp)
        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_nonce=" + Nonce)
        Signature_Base_String = Signature_Base_String + Uri.EscapeDataString("&oauth_version=" + "1.0")

        ' Signature_Base_String = 

        Return Signature_Base_String

    End Function

    Public Function W_CONxUrlEncode(ByVal value As String) As String
        Dim result As StringBuilder = New StringBuilder()

        For Each symbol As Char In value

            If unreservedChars.IndexOf(symbol) <> -1 Then
                result.Append(symbol)
            Else
                result.Append("%"c + String.Format("{0:X2}", Convert.ToInt32(symbol)))
            End If
        Next

        Return result.ToString()
    End Function

    Public Function C_GENbTrustAllCertificateCallback(ByVal sender As Object, ByVal cert As X509Certificate,
       ByVal poChain As X509Chain, ByVal poErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Function C_POStSendApi(ByVal poDbtbl As DataTable, ByVal poDbtblDT As DataTable, ByVal ptExptype As String) As String

        Try
            Dim oJson
            Dim oRespons As New cmlRes   'สำหรับ รับค่าที่ Respons กลับมาจาก API

            Dim oExpCustomer As New cmlExpCustomer    'ส่วนของ Header ลูกค้า 
            Dim oExpCustomerInfo As New cmlExpCustomerInfo  'สำหรับใส่ค่า DT ลูกค้า
            Dim oListExpCustomer As New List(Of cmlExpCustomerInfo)   ' สำหรับรับค่า List ลูกค้า
            Dim oExpGrpCst As New cmlExpGrpCustomer()
            Dim aoListGrpCst As New List(Of cmlExpGrpCustomer)
            Dim aoListCsr As New List(Of cmlExpCustomer)
            Dim aoListSale As New List(Of cmlExpSale)
            Dim oResponse As New HttpResponseMessage
            Dim tStatusDesc As String = ""
            Dim tUrl As String = ""
            Dim tCodeStatus As String = ""
            Dim tValueLog As String = ""
            Dim tPoint As String = ""
            Dim tCstCode As String = ""
            Dim tBchCode As String = ""
            Dim tPosCode As String = ""
            Dim oRes As StreamReader
            Dim tLineRes As String


            Dim tTokenID As String = AdaConfig.cConfig.oConfigAPI.tKey  'Token Key for send API
            Dim tTokenSecret As String = AdaConfig.cConfig.oConfigAPI.tTokenSecret 'Token Secret
            Dim tCusKey As String = AdaConfig.cConfig.oConfigAPI.tCusKey 'Cussumer KEY
            Dim tCusSecret As String = AdaConfig.cConfig.oConfigAPI.tCusSecret 'Cussemer Secret

            Select Case ptExptype
                Case "Customer"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlMember
                    oExpCustomerInfo = New cmlExpCustomerInfo()
                    With oExpCustomerInfo
                        .tCountryID = W_SETtDbnull(poDbtbl.Rows(0)("FTAreCode"))
                        .tAddr1 = W_SETtDbnull(poDbtbl.Rows(0)("FTCstAddr"))
                        .tZipcode = W_SETtDbnull(poDbtbl.Rows(0)("FTCstPostCode"))
                        oListExpCustomer.Add(oExpCustomerInfo)
                    End With

                    oExpCustomer = New cmlExpCustomer()

                    'Spite Name
                    Dim tData As String = poDbtbl.Rows(0)("FTCstName")
                    Dim tLastname As String = " "
                    Dim tFname As String = ""
                    Dim oArr As Array = tData.Split(" ")
                    If oArr.Length = 1 Then
                        tFname = W_SETtDbnull(poDbtbl.Rows(0)("FTCstName"))

                    ElseIf (oArr).Length = 2 Then
                        tFname = oArr(0)
                        tLastname = oArr(1)

                    ElseIf (oArr).Length > 2 Then
                        Dim nLeng As Integer = (oArr).Length - 1
                        tLastname = oArr(nLeng)
                        tFname = oArr((oArr).Length - 2)
                    Else
                        tFname = W_SETtDbnull(poDbtbl.Rows(0)("FTCstName"))
                    End If



                    With oExpCustomer
                        .tFirstName = tFname
                        .tLastname = tLastname
                        .tPhone = W_SETtDbnull(poDbtbl.Rows(0)("FTCstTel"))
                        .tEmailID = W_SETtDbnull(poDbtbl.Rows(0)("FTCstEmail"))
                        .tFax = W_SETtDbnull(poDbtbl.Rows(0)("FTCstFax"))
                        .tMoblile = W_SETtDbnull(poDbtbl.Rows(0)("FTCstResTel"))
                        .tPriCurent = "THB"
                        .tCusID = W_SETtDbnull(poDbtbl.Rows(0)("FTCstCode"))
                        .tIDCard = W_SETtDbnull(poDbtbl.Rows(0)("FTCstCardID"))
                        .tValidDate = W_SETtDbnull(Format(poDbtbl.Rows(0)("FDCstCrdExpire"), "dd-MM-yyyy"))
                        .tBchCode = W_SETtDbnull(poDbtbl.Rows(0)("FTBchCode"))
                        .tGender = W_SETtDbnull(poDbtbl.Rows(0)("FTCstSex"))
                        .tBirthDay = W_SETtDbnull(Format(poDbtbl.Rows(0)("FDCstDob"), "dd-mm-yyyy"))
                        .tMemberPoint = W_SETtDbnull(poDbtbl.Rows(0)("FCCstRetPoint"))
                        .tMemberClass = W_SETtDbnull(poDbtbl.Rows(0)("FTCpgCode"))
                        .oMemberDetail = oListExpCustomer
                    End With
                    aoListCsr.Add(oExpCustomer)

                    tPoint = W_SETtDbnull(poDbtbl.Rows(0)("FCCstRetPoint"))
                    tValueLog = W_SETtDbnull(poDbtbl.Rows(0)("FTCstCode"))
                    tCodeStatus = "ST0000"
                    'Write log Json
                    wExportData.nC_CreateFileCst += 1

                    oJson = JsonConvert.SerializeObject(aoListCsr)
                    C_SETxWriteTextFile(poDbtbl.Rows(0)("FTCstCode"), ptExptype, oJson)
                Case "GrpCustomer"
                    tUrl = AdaConfig.cConfig.oConfigAPI.tUrlGrpMember
                    With oExpGrpCst
                        .tMemberClass = W_SETtDbnull(poDbtbl.Rows(0)("FTCgpCode"))
                        .tMemberDetail = W_SETtDbnull(poDbtbl.Rows(0)("FTCgpName"))

                    End With
                    aoListGrpCst.Add(oExpGrpCst)
                    wExportData.nC_CreateFileGrp += 1

                    'Write log Json
                    tValueLog = W_SETtDbnull(poDbtbl.Rows(0)("FTCgpCode"))
                    tCodeStatus = "ST0000"
                    tPoint = ""
                    oJson = JsonConvert.SerializeObject(aoListGrpCst)
                    C_SETxWriteTextFile(poDbtbl.Rows(0)("FTCgpCode"), ptExptype, oJson)

            End Select


            'Process Call API  แบบ oAuth
            Dim oAuthe As New StringBuilder()
            Dim tTimeStamp As String = (CInt((DateTime.UtcNow - New DateTime(1970, 1, 1)).TotalSeconds)).ToString()

            Dim Nonce As String = "kt9Xt56Ltio"
            Dim oConsumer_Key = Uri.EscapeDataString(tCusKey)
            Dim oCussumer_Secert = Uri.EscapeDataString(tCusSecret)
            Dim oToken_secret = Uri.EscapeDataString(tTokenSecret)
            Dim oToken_ID = Uri.EscapeDataString(tTokenID)

            Dim oStringContent = New StringContent(oJson, UnicodeEncoding.UTF8, "application/json")
            Dim oClient = New HttpClient()
            Dim oWebReq As HttpWebRequest
            Dim oWebRes As HttpWebResponse

            ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf C_GENbTrustAllCertificateCallback)
            ServicePointManager.Expect100Continue = True
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12
            oWebReq = WebRequest.Create(tUrl)

            'Convert String  to url
            Dim oUri As Uri
            oUri = New Uri(tUrl)
            Dim normalizedUrl As String = ""
            Dim normalizedRequestParameters As String = ""
            Dim tAuth As String = GenerateSignature(oUri, oConsumer_Key, oCussumer_Secert, oToken_ID,
                                                    oToken_secret, "Post", tTimeStamp, Nonce,
                                                    "HMACSHA1",
                                                     normalizedUrl, normalizedRequestParameters)

            oAuthe.Append("OAuth realm=""4975451_SB1"",")
            oAuthe.Append("oauth_consumer_key=""" + tCusKey + """,")
            oAuthe.Append("oauth_token=""" + tTokenID + """,")
            oAuthe.Append("oauth_signature_method=""HMAC-SHA1"",")
            oAuthe.Append("oauth_timestamp=""" + tTimeStamp + """,")
            oAuthe.Append("oauth_nonce=""kt9Xt56Ltio"",")
            oAuthe.Append("oauth_version=""1.0"",")
            oAuthe.Append("oauth_signature=""" + UrlEncode(tAuth) + """")

            oWebReq.Method = "POST"
            oWebReq.ContentType = "application/json"
            oWebReq.Headers.Add("Authorization", oAuthe.ToString())

            Using oStramWrite As New StreamWriter(oWebReq.GetRequestStream())
                With oStramWrite
                    .WriteLine(oJson)
                    .Flush()
                    .Close()
                End With
            End Using
            oWebRes = oWebReq.GetResponse()
            If oWebRes.StatusCode = "200" Then
                oRes = New StreamReader(oWebRes.GetResponseStream())
                tLineRes = oRes.ReadToEnd()
                wExportData.nW_Success += 1
            Else
                oRes = New StreamReader(oWebRes.GetResponseStream())
                tLineRes = oRes.ReadToEnd()
                wExportData.nW_Fail += 1
            End If

            cLog.C_CALxWriteLogExport(oWebRes.StatusCode, tLineRes, tValueLog, ptExptype, tPoint)


            Return tStatusDesc

        Catch ex As Exception
            cLog.C_CALxWriteLogExport("ServiceNotfound", "Error", "", ptExptype, "")
            wExportData.nW_Fail += 1
        End Try

    End Function

    ''' <summary>
    ''' คำนวณ ราคา
    ''' </summary>
    ''' <param name="tDoc"> เลขที่เอกสาร </param>
    ''' <param name="tPdtCode">รหัสสินค้า</param>
    ''' <returns></returns>
    Public Function C_CALtSumNet(ByVal tDoc As String, ByVal tPdtCode As String) As String
        Dim tpdtSum As String = ""
        Try

            Dim oSql As New StringBuilder
            Dim oDbtblSum As New DataTable
            Dim oDatabase As New cDatabaseLocal

            oSql.Clear()
            oSql.AppendLine(" Select SUM(FCSdtNet - (FCSdtDisAvg + FCSdtFootAvg + FCSdtRePackAvg)) As FTResult ")
            oSql.AppendLine(" FROM TPSTSalDT ")
            oSql.AppendLine(" WHERE (FTShdDocNo = '" & tDoc & "') AND (FTPdtCode = '" & tPdtCode & "') ")
            oDbtblSum = oDatabase.C_GEToTblSQL(oSql.ToString())

            tpdtSum = oDbtblSum.Rows(0)("FTResult")

        Catch ex As Exception
            Return ""
        End Try
        Return tpdtSum
    End Function

    ''' <summary>
    ''' Write textfile
    ''' </summary>
    ''' <param name="poDataHD"> ข้อมูลตาราง Header</param>
    ''' <param name="poDataDT"> ข้อมูลตาราง Detail</param>

    Public Sub C_SETxWriteTextFile(ByVal poID As String, ByVal ptStatus As String, ByVal ptJson As String)
        Dim tPathFile As String = ""
        Dim oWrite As New StringBuilder
        Dim oExpSale As New cmlExpSale

        Try
            Dim tPathBackup As String = AdaConfig.cConfig.tAppPath & "\Backup"  'เพิ่ม Folder AdaLog *CH 02-12-2014 

            If Not IO.Directory.Exists(tPathBackup) Then
                IO.Directory.CreateDirectory(tPathBackup)
            End If

            Select Case ptStatus
                Case "Sale"
                    tPathBackup &= "\" & "ExportSale_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poID & ".txt"
                    oWrite.AppendLine(ptJson)
                Case "Return"
                    tPathBackup &= "\" & "ExportReturn_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poID & ".txt"
                    oWrite.AppendLine(ptJson)
                Case "Customer"
                    tPathBackup &= "\" & "ExportCustomer_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poID & ".txt"
                    oWrite.AppendLine(ptJson)
                Case "GrpCustomer"
                    tPathBackup &= "\" & "ExportGroupCustomer_" & Format(Date.Now, "yyyyMMddhhmm") & "-" & poID & ".txt"
                    oWrite.AppendLine(ptJson)
            End Select

            If File.Exists(tPathBackup) = False Then
                Using oSw As StreamWriter = File.CreateText(tPathBackup)
                    oSw.Close()
                End Using
            End If

            Using oStreamWriter As StreamWriter = File.AppendText(tPathBackup)
                With oStreamWriter
                    .WriteLine(oWrite)
                    .Flush()
                    .Close()
                End With
            End Using


        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' Convert to string 
    ''' </summary>
    ''' <param name="oVal">Value </param>
    ''' <returns></returns>
    Public Function W_SETtDbnull(ByVal oVal As Object) As String

        Try
            If oVal Is DBNull.Value Then
                Return ""
            Else
                Return oVal
            End If
        Catch ex As Exception

        End Try
    End Function

    Public Function W_SETtColumnLog() As DataTable
        Try

            Dim oDbtbl As New DataTable()
            oDbtbl.Columns.Add("FTStatus", GetType(String))
            oDbtbl.Columns.Add("FTShdDocNo", GetType(String))
            oDbtbl.Columns.Add("FTExptype", GetType(String))
            oDbtbl.Columns.Add("FTShdPosCN", GetType(String))

            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub W_SETxSaveStatusExpFirst(ByVal ptUpdate As String)
        Try
            Dim tStaExp As String = ""
            If ptUpdate = "ExportGrpFirst" Then
                tStaExp = AdaConfig.cConfig.oConfigAPI.tExportGrp
            Else
                tStaExp = AdaConfig.cConfig.oConfigAPI.tExportCst
            End If

            Dim tStatus As String = ""
            If tStaExp = 0 Then

                AdaConfig.cConfig.oXmlCtgAPI = XElement.Load(AdaConfig.cConfig.AdaConfigXmlAPI)
                Dim oQryConfig = From c In AdaConfig.cConfig.oXmlCtgAPI.Elements("APIDetail") Select c

                Dim QryExp = From c In AdaConfig.cConfig.oXmlCtgAPI.Elements("" + ptUpdate + "") Select c
                For Each oType In QryExp
                    oType.Attribute("Type").Value = 1
                Next

                oQryConfig = Nothing
                AdaConfig.cConfig.oXmlCtgAPI.Save(AdaConfig.cConfig.AdaConfigXmlAPI)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Public Function W_DATtGetDataGP(ByVal ptPmhCode As String, ByVal ptType As String) As String
        Dim oDbTbl As New DataTable()
        Dim oDatabase As New cDatabaseLocal()
        Dim oSql As New StringBuilder()
        Dim tReturn As String = ""
        Try
            oSql.Clear()
            oSql.AppendLine(" SELECT FTLnkShrCode,FCLnkGP FROM TLNKMappingGP ")
            oSql.AppendLine(" WHERE FTPmhCode = '" + ptPmhCode + "' ")
            oDbTbl = oDatabase.C_GEToTblSQL(oSql.ToString())
            If oDbTbl.Rows.Count > 0 Then
                If ptType = "GP" Then
                    tReturn = oDbTbl.Rows(0)("FCLnkGP").ToString
                ElseIf ptType = "GPPmt" Then
                    tReturn = oDbTbl.Rows(0)("FTLnkShrCode").ToString
                End If
            End If

            Return tReturn
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function W_DATtGetSumPayment(ByVal ptType As String, ByVal ptDate As String,
                                        ByVal ptCstCode As String, ByVal ptBchCode As String,
                                        ByVal ptPosCode As String,
                                        ByVal ptDoctype As String) As String
        Dim oDbTblRC As New DataTable()
        Dim oDatabase As New cDatabaseLocal()
        Dim oSql As New StringBuilder()
        Dim tReturn As String = ""
        Try
            oSql.Clear()
            oSql.AppendLine(" SELECT RC.FTRcvCode, SUM(RC.FCSrcNet) AS  FTSumNet ")
            oSql.AppendLine(" FROM			TPSTSalHD HD")
            oSql.AppendLine(" LEFT JOIN		TPSTSalRC RC ON HD.FTShdDocNo = RC.FTShdDocNo ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSql.AppendLine(" AND				HD.FTShdStaPrcDoc = '1' ")
            oSql.AppendLine(" AND				HD.FTShdDocType = '" + ptDoctype + "' ")
            oSql.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "'  ")
            oSql.AppendLine(" And				HD.FTBchCode = '" + ptBchCode + "' ")
            oSql.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "' ")
            oSql.AppendLine(" AND				RC.FTRcvCode = '" + ptType + "' ")
            oSql.AppendLine(" GROUP BY		RC.FTRcvCode")

            oDbTblRC = oDatabase.C_GEToTblSQL(oSql.ToString())

            If oDbTblRC.Rows.Count > 0 Then
                tReturn = oDbTblRC.Rows(0)("FTSumNet").ToString()
            End If

            Return tReturn
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function W_DATtGetSumPaymentCredit(ByVal ptType As String, ByVal ptDate As String,
                                      ByVal ptCstCode As String, ByVal ptBchCode As String,
                                      ByVal ptPosCode As String,
                                       ByVal ptDocType As String) As String
        Dim oDbTblRC As New DataTable()
        Dim oDatabase As New cDatabaseLocal()
        Dim oSql As New StringBuilder()
        Dim tReturn As String = ""
        Try
            'oSql.Clear()
            'oSql.AppendLine(" SELECT RC.FTBnkCode , ")
            'oSql.AppendLine(" (SELECT EDC.FTBnkMapping FROM  TLNKMappingEDC EDC  ")
            'oSql.AppendLine(" WHERE (EDC.FTEdcCode = RC.FTBnkCode)) AS EDC, ")
            'oSql.AppendLine(" SUM(RC.FCSrcNet) AS  FTSumNet ")
            'oSql.AppendLine(" FROM			TPSTSalHD HD ")
            'oSql.AppendLine(" LEFT JOIN		TPSTSalRC RC ON HD.FTShdDocNo = RC.FTShdDocNo ")
            'oSql.AppendLine(" LEFT JOIN     TLNKMappingEDC EDC ON RC.FTBnkCode = EDC.FTEdcCode ")
            'oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            'oSql.AppendLine(" And				HD.FTShdStaPrcDoc = '1' ")
            'oSql.AppendLine(" AND				HD.FTShdDocType = '" + ptDocType + "' ")
            'oSql.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "' ")
            'oSql.AppendLine(" AND				HD.FTBchCode = '" + ptBchCode + "' ")
            'oSql.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "'  ")
            'oSql.AppendLine(" AND				RC.FTRcvCode = '002' ")
            'oSql.AppendLine(" AND				EDC.FTBnkMapping = '" + ptType + "' ")
            'oSql.AppendLine(" GROUP BY		RC.FTBnkCode ")

            oSql.Clear()
            oSql.AppendLine(" SELECT RC.FTSrcRetDocRef , ")
            oSql.AppendLine(" SUM(RC.FCSrcNet) AS  FTSumNet ")
            oSql.AppendLine(" FROM			TPSTSalHD HD ")
            oSql.AppendLine(" LEFT JOIN		TPSTSalRC RC ON HD.FTShdDocNo = RC.FTShdDocNo  ")
            oSql.AppendLine(" LEFT JOIN TLNKMappingEDC EDC ON RC.FTSrcRetDocRef = EDC.FTEdcCode ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSql.AppendLine(" And				HD.FTShdStaPrcDoc = '1' ")
            oSql.AppendLine(" AND				HD.FTShdDocType = '" + ptDocType + "' ")
            oSql.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "' ")
            oSql.AppendLine(" AND				HD.FTBchCode = '" + ptBchCode + "' ")
            oSql.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "'  ")
            oSql.AppendLine(" AND				RC.FTRcvCode = '002' ")
            oSql.AppendLine(" AND				EDC.FTEdcCode = '" + ptType + "' ")
            oSql.AppendLine(" GROUP BY		RC.FTSrcRetDocRef ")


            oDbTblRC = oDatabase.C_GEToTblSQL(oSql.ToString())

            If oDbTblRC.Rows.Count > 0 Then
                tReturn = oDbTblRC.Rows(0)("FTSumNet").ToString()
            End If

            Return tReturn
        Catch ex As Exception
            Return ""
        End Try
    End Function

    Public Function UrlEncode(ByVal value As String) As String
        Dim result As StringBuilder = New StringBuilder()

        For Each symbol As Char In value

            If unreservedChars.IndexOf(symbol) <> -1 Then
                result.Append(symbol)
            Else
                result.Append("%"c + String.Format("{0:X2}", Convert.ToInt32(symbol)))
            End If
        Next

        Return result.ToString()
    End Function


    Public Function GenerateSignature(ByVal url As Uri, ByVal consumerKey As String, ByVal consumerSecret As String,
                                      ByVal token As String, ByVal tokenSecret As String, ByVal httpMethod As String,
                                      ByVal timeStamp As String, ByVal nonce As String,
                                      ByVal signatureType As String, <Out> ByRef normalizedUrl As String,
                                      <Out> ByRef normalizedRequestParameters As String) As String
        normalizedUrl = Nothing
        normalizedRequestParameters = Nothing

        Dim signatureBase As String = GenerateSignatureBase(url, consumerKey, token, tokenSecret, httpMethod, timeStamp, nonce, HMACSHA1SignatureType, normalizedUrl, normalizedRequestParameters)
        Try
            Dim hmacsha1 As HMACSHA1 = New HMACSHA1()
            hmacsha1.Key = Encoding.ASCII.GetBytes(String.Format("{0}&{1}", UrlEncode(consumerSecret), If(String.IsNullOrEmpty(tokenSecret), "", UrlEncode(tokenSecret))))
            Return GenerateSignatureUsingHash(signatureBase, hmacsha1)
        Catch ex As Exception
            Throw New ArgumentException("Unknown signature type", "signatureType")
        End Try

    End Function

    Public Function GenerateSignatureUsingHash(ByVal signatureBase As String, ByVal hash As HashAlgorithm) As String
        Return ComputeHash(hash, signatureBase)
    End Function

    Private Function ComputeHash(ByVal hashAlgorithm As HashAlgorithm, ByVal data As String) As String
        If hashAlgorithm Is Nothing Then
            Throw New ArgumentNullException("hashAlgorithm")
        End If

        If String.IsNullOrEmpty(data) Then
            Throw New ArgumentNullException("data")
        End If

        Dim dataBuffer As Byte() = System.Text.Encoding.ASCII.GetBytes(data)
        Dim hashBytes As Byte() = hashAlgorithm.ComputeHash(dataBuffer)
        Return Convert.ToBase64String(hashBytes)
    End Function

    Public Function GenerateSignatureBase(ByVal url As Uri, ByVal consumerKey As String, ByVal token As String,
                                          ByVal tokenSecret As String, ByVal httpMethod As String,
                                          ByVal timeStamp As String, ByVal nonce As String,
                                          ByVal signatureType As String,
                                          <Out()> ByRef normalizedUrl As String,
                                          <Out()> ByRef normalizedRequestParameters As String) As String
        If token Is Nothing Then
            token = String.Empty
        End If

        If tokenSecret Is Nothing Then
            tokenSecret = String.Empty
        End If

        If String.IsNullOrEmpty(consumerKey) Then
            Throw New ArgumentNullException("consumerKey")
        End If

        If String.IsNullOrEmpty(httpMethod) Then
            Throw New ArgumentNullException("httpMethod")
        End If

        If String.IsNullOrEmpty(signatureType) Then
            Throw New ArgumentNullException("signatureType")
        End If

        normalizedUrl = Nothing
        normalizedRequestParameters = Nothing
        Dim parameters As List(Of QueryParameter) = GetQueryParameters(url.Query)

        If includeVersion Then
            parameters.Add(New QueryParameter(OAuthVersionKey, OAuthVersion))
        End If

        parameters.Add(New QueryParameter(OAuthNonceKey, nonce))
        parameters.Add(New QueryParameter(OAuthTimestampKey, timeStamp))
        parameters.Add(New QueryParameter(OAuthSignatureMethodKey, signatureType))
        parameters.Add(New QueryParameter(OAuthConsumerKeyKey, consumerKey))

        If Not String.IsNullOrEmpty(token) Then
            parameters.Add(New QueryParameter(OAuthTokenKey, token))
        End If

        parameters.Sort(New QueryParameterComparer())
        normalizedUrl = String.Format("{0}://{1}", url.Scheme, url.Host)

        If Not ((url.Scheme = "http" AndAlso url.Port = 80) OrElse (url.Scheme = "https" AndAlso url.Port = 443)) Then
            normalizedUrl += ":" & url.Port
        End If

        normalizedUrl += url.AbsolutePath
        normalizedRequestParameters = NormalizeRequestParameters(parameters)
        Dim signatureBase As StringBuilder = New StringBuilder()
        signatureBase.AppendFormat("{0}&", httpMethod.ToUpper())
        signatureBase.AppendFormat("{0}&", UrlEncode(normalizedUrl))
        signatureBase.AppendFormat("{0}", UrlEncode(normalizedRequestParameters))
        Return signatureBase.ToString()
    End Function

    Private Function GetQueryParameters(ByVal parameters As String) As List(Of QueryParameter)
        If parameters.StartsWith("?") Then
            parameters = parameters.Remove(0, 1)
        End If

        Dim result As List(Of QueryParameter) = New List(Of QueryParameter)()

        If Not String.IsNullOrEmpty(parameters) Then
            Dim p As String() = parameters.Split("&"c)

            For Each s As String In p

                If Not String.IsNullOrEmpty(s) AndAlso Not s.StartsWith(OAuthParameterPrefix) Then

                    If s.IndexOf("="c) > -1 Then
                        Dim temp As String() = s.Split("="c)
                        result.Add(New QueryParameter(temp(0), temp(1)))
                    Else
                        result.Add(New QueryParameter(s, String.Empty))
                    End If
                End If
            Next
        End If

        Return result
    End Function

    Protected Function NormalizeRequestParameters(ByVal parameters As IList(Of QueryParameter)) As String
        Dim sb As StringBuilder = New StringBuilder()
        Dim p As QueryParameter = Nothing

        For i As Integer = 0 To parameters.Count - 1
            p = parameters(i)
            sb.AppendFormat("{0}={1}", p.Name, p.Value)

            If i < parameters.Count - 1 Then
                sb.Append("&")
            End If
        Next

        Return sb.ToString()
    End Function

    Public Function W_DATtGetDataVat(ByVal ptDoctype As String, ByVal ptCstCode As String, ByVal ptBchCode As String, ByVal ptPosCode As String, ByVal ptDocDate As String) As String
        Dim oDbTbl As New DataTable()
        Dim oDatabase As New cDatabaseLocal()
        Dim oSql As New StringBuilder()
        Dim tReturn As String = ""
        Try
            oSql.Clear()
            oSql.AppendLine(" SELECT FCShdVATRate")
            oSql.AppendLine(" FROM			TPSTSalHD HD ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDocDate + "' ")
            oSql.AppendLine(" AND		    HD.FTShdStaPrcDoc = '1'  ")
            oSql.AppendLine(" AND		    HD.FTShdDocType = '" + ptDoctype + "'  ")
            oSql.AppendLine(" AND		    HD.FTCstCode= '" + ptCstCode + "' ")
            oSql.AppendLine(" AND		    HD.FTBchCode = '" + ptBchCode + "' ")
            oSql.AppendLine(" AND		    HD.FTPosCode = '" + ptPosCode + "'   ")
            oSql.AppendLine(" GROUP BY		FTCstCode,FTBchCode,FTPosCode,FCShdVATRate")
            oDbTbl = oDatabase.C_GEToTblSQL(oSql.ToString())
            If oDbTbl.Rows.Count > 0 Then
                tReturn = oDbTbl.Rows(0)("FCShdVATRate").ToString()
            End If

            Return tReturn
        Catch ex As Exception
            Return ""
        End Try
    End Function

End Class
