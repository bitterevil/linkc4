﻿Imports Newtonsoft.Json

Public Class cmlExpGrpCustomer

    <JsonProperty("Member_Class")>
    Property tMemberClass As String

    <JsonProperty("Member_ClassDetails")>
    Property tMemberDetail As String
End Class
