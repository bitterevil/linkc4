﻿Public Class wInsertSharing
    Public Shared oXmlCtgShareDis As XElement

    Private Sub ocmInsert_Click(sender As Object, e As EventArgs) Handles ocmInsert.Click
        cLog.C_CALxWriteLog("wInsertSharing > Save")
        If W_SETbUpdateXmlSharing(otbCode.Text) = True Then
            MsgBox("บันทึกข้อมูลสำเร็จ", MsgBoxStyle.Information)
            Me.DialogResult = Windows.Forms.DialogResult.OK
        End If
    End Sub

    Public Function W_UPTbUpdateXML()
        Try

        Catch ex As Exception

        End Try
    End Function

    Public Function W_INSbInsertXML()
        Try

        Catch ex As Exception

        End Try
    End Function

    Public Function W_SETbUpdateXmlSharing(ByVal tCaseUpt As String) As Boolean

        W_SETbUpdateXmlSharing = False
        Dim tElement As String = ""
        Try
            oXmlCtgShareDis = XElement.Load(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")

            Select Case tCaseUpt
                Case "1"
                    tElement = "CompenSate"
                Case "2"
                    tElement = "ShareDis"
                Case "3"
                    tElement = "MarginOnnet"
                Case "4"
                    tElement = "MarginOnSale"
                Case "5"
                    tElement = "ShareDiscount30"
                Case "6"
                    tElement = "ShareDiscount40"
                Case "7"
                    tElement = "ShareDiscount40_1"
                Case "8"
                    tElement = "ShareDiscount40_2"
                Case "9"
                    tElement = "ShareDiscount40_3"
                Case "10"
                    tElement = "ShareDiscount40_4"
                Case "11"
                    tElement = "ShareDiscount40_5"
            End Select

            Dim oElelment = From c In oXmlCtgShareDis.Elements("" + tElement + "") Select c
            For Each oItem In oElelment
                oItem.Attribute("ID").Value = otbCode.Text
                oItem.Attribute("Detail").Value = otbDetail.Text
            Next


            oXmlCtgShareDis.Save(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")
            W_SETbUpdateXmlSharing = True

        Catch ex As Exception

        End Try

    End Function

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub wInsertSharing_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub
End Class