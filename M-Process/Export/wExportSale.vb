﻿Imports System.IO
Imports System.Text

Public Class wExportSale

    Dim oW_oDbtblSale As DataTable
    Dim oW_oDbtblReturn As DataTable
    Public Shared oW_oDbtblSaleExp As DataTable
    Public Shared oW_oDbtblReturnExp As DataTable
    Public Shared oW_tDate As DateTime
    Dim tW_DocNo As String


    ''' <summary>
    ''' สร้าง Column สำหรับ Table ขาย
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnSale() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbtbl.Columns.Add("FTDocNo", GetType(String))
            oDbtbl.Columns.Add("FTCstCodeFmt", GetType(String))
            oDbtbl.Columns.Add("FTCstCode", GetType(String))
            oDbtbl.Columns.Add("FTBchCode", GetType(String))
            oDbtbl.Columns.Add("FTPostCode", GetType(String))
            oDbtbl.Columns.Add("FTSumNet", GetType(String))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    ''' <summary>
    ''' สร้าง Column สำหรับ Table คืน
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnReturn() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbtbl.Columns.Add("FTDocNo", GetType(String))
            oDbtbl.Columns.Add("FTCstCodeFmt", GetType(String))
            oDbtbl.Columns.Add("FTCstCode", GetType(String))
            oDbtbl.Columns.Add("FTBchCode", GetType(String))
            oDbtbl.Columns.Add("FTPostCode", GetType(String))
            oDbtbl.Columns.Add("FTSumNet", GetType(String))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try

    End Function

    Public Function W_GETxDataSale(ByVal ptDate As String) As Boolean

        Try
            Dim tDate As String = Convert.ToDateTime(ptDate).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
            Dim oDbTblSaleHD As New DataTable
            Dim oDbTblHD As New DataTable
            Dim oDbTblRc As New DataTable
            Dim oDatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder

            'ดึงค่าที่ Group by  วันที่ , รหัสลูกค้า , BranchCode , PosCode
            oSql.AppendLine(" SELECT        CONVERT(VARCHAR, HD.FDShdDocDate, 112) As FDShdDocDate ,")
            oSql.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode")
            oSql.AppendLine(" FROM			TPSTSalHD HD  ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + tDate + "' ")
            oSql.AppendLine(" AND			HD.FTShdStaPrcDoc = '1' ")
            oSql.AppendLine(" AND			HD.FTShdDocType = '1' ")
            oSql.AppendLine(" GROUP BY		HD.FDShdDocDate,HD.FTCstCode,HD.FTBchCode,HD.FTPosCode ")
            oDbTblHD = oDatabase.C_GEToTblSQL(oSql.ToString())
            oW_oDbtblSale = W_SEToColumnSale()
            If oDbTblHD.Rows.Count > 0 Then
                For nRow As Integer = 0 To oDbTblHD.Rows.Count - 1
                    'ดึง ค่าอื่น ๆ จาก Sale HD โดย มีการ group promotion เพื่อหาสินค้าที่มี Promotion
                    oSql.Clear()
                    oSql.AppendLine(" SELECT 'THB' AS FTCurrency, ")
                    oSql.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'AR' + HD.FTBchCode + HD.FTPosCode + '-' + '000001' ")
                    oSql.AppendLine(" ELSE FTCstCode END AS FTCstCodeFmt,")
                    oSql.AppendLine(" '1' AS FTExchangeRate, ")
                    oSql.AppendLine(" 'IN' AS FTDocType, ")
                    oSql.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'N' ELSE 'Y' END AS FTMember,")
                    oSql.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode, ")
                    oSql.AppendLine(" SUM(HD.FCShdVatable) AS FCShdVatable , ")
                    oSql.AppendLine(" SUM(HD.FCShdVat) AS FCShdVat ,")
                    oSql.AppendLine(" SUM(HD.FCShdAftDisChg) AS FCShdAftDisChg,")
                    oSql.AppendLine(" SUM(HD.FCShdRnd) AS FCShdRnd,SUM(HD.FCShdGrand) AS FCShdGrand ")
                    oSql.AppendLine(" FROM			TPSTSalHD HD ")
                    oSql.AppendLine(" WHERE		    CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + oDbTblHD(nRow)("FDShdDocDate").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTShdStaPrcDoc = '1' ")
                    oSql.AppendLine(" AND			HD.FTShdDocType = '1' ")
                    oSql.AppendLine(" AND			HD.FTCstCode= '" + oDbTblHD(nRow)("FTCstCode").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTBchCode = '" + oDbTblHD(nRow)("FTBchCode").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTPosCode = '" + oDbTblHD(nRow)("FTPosCode").ToString + "' ")
                    oSql.AppendLine(" GROUP BY		HD.FTCstCode,HD.FTBchCode,HD.FTPosCode ")
                    oDbTblSaleHD = oDatabase.C_GEToTblSQL(oSql.ToString)

                    If oDbTblSaleHD.Rows.Count > 0 Then
                        For nRowSale As Integer = 0 To oDbTblSaleHD.Rows.Count - 1
                            Dim oDataRow As DataRow = oW_oDbtblSale.NewRow()
                            oDataRow("FTSelect") = True
                            oDataRow("FTDocNo") = "IN" + oDbTblHD(nRow)("FDShdDocDate").ToString + "-" +
                                oDbTblHD(nRow)("FTBchCode").ToString + oDbTblHD(nRow)("FTPosCode").ToString + "-" +
                                W_GENtGenterateRunning(oDbTblHD(nRow)("FTPosCode").ToString, oDbTblHD(nRow)("FTBchCode").ToString)

                            oDataRow("FTCstCodeFmt") = oDbTblSaleHD.Rows(nRowSale)("FTCstCodeFmt").ToString
                            oDataRow("FTCstCode") = oDbTblSaleHD.Rows(nRowSale)("FTCstCode").ToString
                            oDataRow("FTBchCode") = oDbTblHD(nRow)("FTBchCode").ToString
                            oDataRow("FTPostCode") = oDbTblHD(nRow)("FTPosCode").ToString
                            oDataRow("FTSumNet") = W_DATtGETSaleRC(oDbTblHD(nRow)("FTCstCode").ToString,
                                                                   oDbTblHD(nRow)("FDShdDocDate").ToString,
                                                                   oDbTblHD(nRow)("FTBchCode").ToString,
                                                                   oDbTblHD(nRow)("FTPosCode").ToString, "1")
                            oW_oDbtblSale.Rows.Add(oDataRow)
                        Next
                    End If
                Next


                With ogdSale
                    .DataSource = oW_oDbtblSale
                    .RowHeadersVisible = False
                    .AllowUserToAddRows = False
                    .Columns("FTDocNo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FTCstCode").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTSelect").Width = 50
                        .Columns("FTSelect").HeaderText = ""
                        .Columns("FTDocNo").HeaderText = "เลขที่เอกสาร"
                        .Columns("FTCstCodeFmt").HeaderText = "ลูกค้า"
                        .Columns("FTBchCode").HeaderText = "สาขา"
                        .Columns("FTPostCode").HeaderText = "เครื่องจุดขาย"
                        .Columns("FTSumNet").HeaderText = "จำนวนเงิน"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTSelect").HeaderText = ""
                        .Columns("FTDocNo").HeaderText = "Doccument No"
                        .Columns("FTCstCodeFmt").HeaderText = "Customer"
                        .Columns("FTBchCode").HeaderText = "Branch"
                        .Columns("FTPostCode").HeaderText = "PosCode"
                        .Columns("FTSumNet").HeaderText = "Amount"
                    End If

                    .Columns("FTDocNo").ReadOnly = True
                    .Columns("FTCstCodeFmt").ReadOnly = True
                    .Columns("FTCstCode").Visible = False
                    .Columns("FTBchCode").ReadOnly = True
                    .Columns("FTPostCode").ReadOnly = True
                    .Columns("FTSumNet").ReadOnly = True
                End With

                W_SETxCheckboxSaleAllow()
                Return True
            Else
                Return False
            End If


        Catch ex As Exception

        End Try
    End Function

    Public Function W_GETxDataReturn(ByVal ptDate As String) As Boolean
        Try
            Dim tDate As String = Convert.ToDateTime(ptDate).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
            Dim oDbTblSaleHD As New DataTable
            Dim oDbTblHD As New DataTable
            Dim oDbTblRc As New DataTable
            Dim oDatabase As New cDatabaseLocal
            Dim oSql As New StringBuilder

            'ดึงค่าที่ Group by  วันที่ , รหัสลูกค้า , BranchCode , PosCode
            oSql.AppendLine(" SELECT        CONVERT(VARCHAR, HD.FDShdDocDate, 112) As FDShdDocDate ,")
            oSql.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode")
            oSql.AppendLine(" FROM			TPSTSalHD HD  ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + tDate + "' ")
            oSql.AppendLine(" AND			HD.FTShdStaPrcDoc = '1' ")
            oSql.AppendLine(" AND			HD.FTShdDocType = '9' ")
            oSql.AppendLine(" GROUP BY		HD.FDShdDocDate,HD.FTCstCode,HD.FTBchCode,HD.FTPosCode ")
            oDbTblHD = oDatabase.C_GEToTblSQL(oSql.ToString())
            oW_oDbtblReturn = W_SEToColumnReturn()
            If oDbTblHD.Rows.Count > 0 Then
                For nRow As Integer = 0 To oDbTblHD.Rows.Count - 1
                    'ดึง ค่าอื่น ๆ จาก Sale HD โดย มีการ group promotion เพื่อหาสินค้าที่มี Promotion
                    oSql.Clear()
                    oSql.AppendLine(" SELECT 'THB' AS FTCurrency, ")
                    oSql.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'AR' + HD.FTBchCode + HD.FTPosCode + '-' + '000001' ")
                    oSql.AppendLine(" ELSE FTCstCode END AS FTCstCodeFmt,")
                    oSql.AppendLine(" '1' AS FTExchangeRate, ")
                    oSql.AppendLine(" 'RE' AS FTDocType, ")
                    oSql.AppendLine(" CASE HD.FTCstCode WHEN '0' THEN 'N' ELSE 'Y' END AS FTMember,")
                    oSql.AppendLine(" HD.FTCstCode,HD.FTBchCode,HD.FTPosCode, ")
                    oSql.AppendLine(" SUM(HD.FCShdVatable) AS FCShdVatable , ")
                    oSql.AppendLine(" SUM(HD.FCShdVat) AS FCShdVat ,")
                    oSql.AppendLine(" SUM(HD.FCShdAftDisChg) AS FCShdAftDisChg,")
                    oSql.AppendLine(" SUM(HD.FCShdRnd) AS FCShdRnd,SUM(HD.FCShdGrand) AS FCShdGrand ")
                    oSql.AppendLine(" FROM			TPSTSalHD HD ")
                    oSql.AppendLine(" WHERE		    CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + oDbTblHD(nRow)("FDShdDocDate").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTShdStaPrcDoc = '1' ")
                    oSql.AppendLine(" AND			HD.FTShdDocType = '9' ")
                    oSql.AppendLine(" AND			HD.FTCstCode= '" + oDbTblHD(nRow)("FTCstCode").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTBchCode = '" + oDbTblHD(nRow)("FTBchCode").ToString + "' ")
                    oSql.AppendLine(" AND			HD.FTPosCode = '" + oDbTblHD(nRow)("FTPosCode").ToString + "' ")
                    oSql.AppendLine(" GROUP BY		HD.FTCstCode,HD.FTBchCode,HD.FTPosCode ")
                    oDbTblSaleHD = oDatabase.C_GEToTblSQL(oSql.ToString)

                    If oDbTblSaleHD.Rows.Count > 0 Then
                        For nRowSale As Integer = 0 To oDbTblSaleHD.Rows.Count - 1
                            Dim oDataRow As DataRow = oW_oDbtblReturn.NewRow()
                            oDataRow("FTSelect") = True
                            oDataRow("FTDocNo") = "RE" + oDbTblHD(nRow)("FDShdDocDate").ToString + "-" + oDbTblHD(nRow)("FTBchCode").ToString _
                                + oDbTblHD(nRow)("FTPosCode").ToString + "-" +
                                W_GENtGenterateRunning(oDbTblHD(nRow)("FTPosCode").ToString, oDbTblHD(nRow)("FTBchCode").ToString)

                            oDataRow("FTCstCodeFmt") = oDbTblSaleHD.Rows(nRowSale)("FTCstCodeFmt").ToString
                            oDataRow("FTCstCode") = oDbTblSaleHD.Rows(nRowSale)("FTCstCode").ToString
                            oDataRow("FTBchCode") = oDbTblHD(nRow)("FTBchCode").ToString
                            oDataRow("FTPostCode") = oDbTblHD(nRow)("FTPosCode").ToString
                            oDataRow("FTSumNet") = W_DATtGETSaleRC(oDbTblHD(nRow)("FTCstCode").ToString,
                                                                   oDbTblHD(nRow)("FDShdDocDate").ToString,
                                                                   oDbTblHD(nRow)("FTBchCode").ToString,
                                                                   oDbTblHD(nRow)("FTPosCode").ToString, "9")
                            oW_oDbtblReturn.Rows.Add(oDataRow)
                        Next
                    End If
                Next

                With ogdReturn
                    .DataSource = oW_oDbtblReturn
                    .RowHeadersVisible = False
                    .AllowUserToAddRows = False
                    .Columns("FTDocNo").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FTCstCodeFmt").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTSelect").Width = 50
                        .Columns("FTSelect").HeaderText = ""
                        .Columns("FTDocNo").HeaderText = "เลขที่เอกสาร"
                        .Columns("FTCstCodeFmt").HeaderText = "ลูกค้า"
                        .Columns("FTBchCode").HeaderText = "สาขา"
                        .Columns("FTPostCode").HeaderText = "เครื่องจุดขาย"
                        .Columns("FTSumNet").HeaderText = "จำนวนเงิน"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTSelect").HeaderText = ""
                        .Columns("FTDocNo").HeaderText = "Doccument No"
                        .Columns("FTCstCodeFmt").HeaderText = "Customer"
                        .Columns("FTBchCode").HeaderText = "Branch"
                        .Columns("FTPostCode").HeaderText = "PosCode"
                        .Columns("FTSumNet").HeaderText = "Amount"
                    End If
                    .Columns("FTCstCode").Visible = False
                    .Columns("FTDocNo").ReadOnly = True
                    .Columns("FTCstCodeFmt").ReadOnly = True
                    .Columns("FTBchCode").ReadOnly = True
                    .Columns("FTPostCode").ReadOnly = True
                    .Columns("FTSumNet").ReadOnly = True

                End With
                W_SETxCheckboxReturnAllow()
            Else
                Return False
            End If
        Catch ex As Exception

        End Try

    End Function
    Public Function W_DATtGETSaleRC(ByVal ptCstCode As String, ByVal ptDate As String _
                                    , ByVal ptBchCode As String, ByVal ptPosCode As String,
                                    ByVal PtStatus As String) As String
        Dim oDatabase As New cDatabaseLocal
        Dim oSql As New StringBuilder
        Dim oDbTblRc As New DataTable

        Try
            oSql.Clear()
            oSql.AppendLine(" SELECT SUM(RC.FCSrcNet)  FTSumNet ")
            oSql.AppendLine(" FROM			TPSTSalHD HD ")
            oSql.AppendLine(" LEFT JOIN		TPSTSalRC RC ON HD.FTShdDocNo = RC.FTShdDocNo ")
            oSql.AppendLine(" WHERE			CONVERT(VARCHAR, HD.FDShdDocDate, 112) = '" + ptDate + "' ")
            oSql.AppendLine(" AND				HD.FTShdStaPrcDoc = '1' ")
            oSql.AppendLine(" AND				HD.FTShdDocType = '" + PtStatus + "' ")
            oSql.AppendLine(" AND				HD.FTCstCode= '" + ptCstCode + "' ")
            oSql.AppendLine(" AND				HD.FTBchCode = '" + ptBchCode + "' ")
            oSql.AppendLine(" AND				HD.FTPosCode = '" + ptPosCode + "' ")
            oSql.AppendLine(" GROUP BY		HD.FTCstCode,	HD.FTBchCode ,	HD.FTPosCode ")
            oDbTblRc = oDatabase.C_GEToTblSQL(oSql.ToString())
            If oDbTblRc.Rows.Count > 0 Then
                Return oDbTblRc.Rows(0)("FTSumNet").ToString()
            End If

        Catch ex As Exception

        End Try

    End Function

    Public Function W_GENtGenterateRunning(ByVal ptPos As String, ByVal ptBch As String) As String
        Try
            Dim oDatabase As New cDatabaseLocal
            Dim oDbTblRunning As New DataTable
            Dim oDbTblRGet As New DataTable
            Dim oCmd As New StringBuilder
            Dim tRunning As String = ""
            Dim tLastrunning As String = ""
            Dim tCalRunning As String = ""
            Dim tResult As String = ""
            Dim nCount As Integer = 0


            oCmd.Clear()
            oCmd.AppendLine(" SELECT  FNNum, FTRunningNumber,FTPos,FTBranch")
            oCmd.AppendLine(" FROM TCNTPdtRunning")
            oCmd.AppendLine(" WHERE FTPos = '" + ptPos + "' ")
            oCmd.AppendLine(" AND FTBranch = '" + ptBch + "' ")
            oDbTblRGet = oDatabase.C_CALoExecuteReader(oCmd.ToString)

            nCount = oDbTblRGet.Rows.Count
            If nCount = 0 Then
                tCalRunning = "0001"
                tResult = tCalRunning
                C_INSbRunningNumber(tCalRunning, "Insert", ptPos, ptBch)
            Else

                tLastrunning = oDbTblRGet.Rows(nCount - 1)("FTRunningNumber")
                '   tRunning = "0000" + (tLastrunning + 1)
                ' tReSult = Right("0000" + tRunning, 4)
                tRunning = tLastrunning + 1
                tCalRunning = "000" + tRunning
                tResult = tCalRunning.Substring(tCalRunning.ToString().Length - 4)
                C_INSbRunningNumber(tResult, "Update", ptPos, ptBch)
            End If

            Return tResult

        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Update RunningNumber
    ''' </summary>
    ''' <param name="ptRuningNum">Running Number New for update </param>
    ''' <returns></returns>
    Public Function C_INSbRunningNumber(ByVal ptRuningNum As String, ByVal ptStatus As String, ByVal ptPos As String, ByVal ptBch As String) As Boolean
        Try
            Dim oIns As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            If ptStatus = "Update" Then
                oIns.Clear()
                oIns.AppendLine(" UPDATE TCNTPdtRunning ")
                oIns.AppendLine(" SET FTRunningNumber = '" + ptRuningNum + "' ")
                oIns.AppendLine(" WHERE FTPos = '" + ptPos + "' ")
                oIns.AppendLine(" AND FTBranch = '" + ptBch + "'")
            Else
                oIns.Clear()
                oIns.AppendLine(" Insert Into ")
                oIns.AppendLine(" TCNTPdtRunning (FTRunningNumber,FTPos,FTBranch)")
                oIns.AppendLine(" VALUES ('" & ptRuningNum & "','" + ptPos + "','" + ptBch + "')")
            End If

            If oDatabase.C_CALnExecuteNonQuery(oIns.ToString) Then
                Return True
            End If

        Catch ex As Exception
            Return False
        End Try

    End Function

    Public Sub W_EXPxExportDate(ByVal ptDate As DateTime)
        Try

            Dim tDate As String = ""

            Dim tStatusReturn As String = ""
            If ptDate <> Nothing Then
                tDate = Format(ptDate, "yyyyMMdd").ToString
            Else
                oW_tDate = Convert.ToDateTime(odpDate.Value).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
            End If

            Dim oExpSale As New cExportSale
            Dim tSelect As String = ""
            Dim tType As String = ""
            Dim tExportFirst As String = ""
            Dim tDocNo As String = ""
            Dim tCstCode As String = ""
            Dim tBchCode As String = ""
            Dim tPosCode As String = ""
            Dim tSaleAmt As String = ""


            'Export ประเภทขาย 
            If oW_oDbtblSale.Rows.Count Then
                For nRowCst As Integer = 0 To oW_oDbtblSale.Rows.Count - 1
                    If ogdSale.Rows(nRowCst).Cells(0).Value = True Then

                        tDocNo = ogdSale.Rows(nRowCst).Cells(1).Value
                        tCstCode = ogdSale.Rows(nRowCst).Cells(3).Value
                        tBchCode = ogdSale.Rows(nRowCst).Cells(4).Value
                        tPosCode = ogdSale.Rows(nRowCst).Cells(5).Value
                        tSaleAmt = ogdSale.Rows(nRowCst).Cells(6).Value

                        tW_DocNo = tDocNo
                        If oExpSale.C_EXPbExportSaleCustomer("Sale", tDocNo, tCstCode, tBchCode, tPosCode, IIf(ptDate <> Nothing, tDate, oW_tDate), tSaleAmt) Then
                        End If
                    End If
                Next
            End If




            'Export ประเภทคืน
            If oW_oDbtblReturn.Rows.Count Then
                For nRowReturn As Integer = 0 To oW_oDbtblReturn.Rows.Count - 1
                    If ogdReturn.Rows(nRowReturn).Cells(0).Value = True Then
                        System.Threading.Thread.Sleep(1000)

                        tDocNo = ogdReturn.Rows(nRowReturn).Cells(1).Value
                        tCstCode = ogdReturn.Rows(nRowReturn).Cells(3).Value
                        tBchCode = ogdReturn.Rows(nRowReturn).Cells(4).Value
                        tPosCode = ogdReturn.Rows(nRowReturn).Cells(5).Value
                        tSaleAmt = ogdReturn.Rows(nRowReturn).Cells(6).Value
                        tW_DocNo = tDocNo
                        If oExpSale.C_EXPbExportReturnCustomer("Return", tDocNo, tCstCode, tBchCode, tPosCode, IIf(ptDate <> Nothing, tDate, oW_tDate), tSaleAmt) Then

                        End If
                    End If
                Next
            End If


        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmSearch_Click(sender As Object, e As EventArgs) Handles ocmSearch.Click
        W_GETxDataSale(odpDate.Value)
        W_GETxDataReturn(odpDate.Value)
        oW_tDate = odpDate.Value
    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Private Sub ocmSelect_Click(sender As Object, e As EventArgs) Handles ocmSelect.Click
        Dim bChoose As Boolean = False
        Dim bReturn As Boolean = False

        For nrow As Integer = 0 To ogdSale.Rows.Count - 1
            If ogdSale.Rows(nrow).Cells(0).Value = True Then
                bChoose = True
            End If
        Next

        For nrowReturn As Integer = 0 To ogdReturn.Rows.Count - 1
            If ogdReturn.Rows(nrowReturn).Cells(0).Value = True Then
                bReturn = True
            End If
        Next

        If bChoose = True Or bReturn = True Then

            wExportData.bW_StatusExport = True
            oW_tDate = odpDate.Value
            Try
                oW_oDbtblSaleExp = W_SEToColumnSale()
                oW_oDbtblReturnExp = W_SEToColumnReturn()


                'Export ประเภทขาย 
                For nRowSale As Integer = 0 To oW_oDbtblSale.Rows.Count - 1
                    If ogdSale.Rows(nRowSale).Cells(0).Value = True Then
                        Dim oRow As DataRow = oW_oDbtblSaleExp.NewRow()
                        oRow("FTSelect") = True
                        oRow("FTDocNo") = oW_oDbtblSale.Rows(nRowSale).Item("FTDocNo")
                        oRow("FTCstCodeFmt") = oW_oDbtblSale.Rows(nRowSale).Item("FTCstCodeFmt")
                        oRow("FTCstCode") = oW_oDbtblSale.Rows(nRowSale).Item("FTCstCode")
                        oRow("FTBchCode") = oW_oDbtblSale.Rows(nRowSale).Item("FTBchCode")
                        oRow("FTPostCode") = oW_oDbtblSale.Rows(nRowSale).Item("FTPostCode")
                        oRow("FTSumNet") = oW_oDbtblSale.Rows(nRowSale).Item("FTSumNet")

                        oW_oDbtblSaleExp.Rows.Add(oRow)

                    End If
                Next


                'Export ประเภทคืน
                For nRowReturn As Integer = 0 To oW_oDbtblReturn.Rows.Count - 1
                    If ogdReturn.Rows(nRowReturn).Cells(0).Value = True Then
                        Dim oRow As DataRow = oW_oDbtblReturnExp.NewRow()
                        oRow("FTSelect") = True
                        oRow("FTDocNo") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTDocNo")
                        oRow("FTCstCodeFmt") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTCstCodeFmt")
                        oRow("FTCstCode") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTCstCode")
                        oRow("FTBchCode") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTBchCode")
                        oRow("FTPostCode") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTPostCode")
                        oRow("FTSumNet") = oW_oDbtblReturn.Rows(nRowReturn).Item("FTSumNet")

                        oW_oDbtblReturnExp.Rows.Add(oRow)

                    End If
                Next

                If cCNVB.nVB_CutLng = 1 Then
                    MsgBox("ระบบทำรายการเลือกข้อมูลสำเร็จ", MsgBoxStyle.Information)
                Else
                    MsgBox("Select information success", MsgBoxStyle.Information)
                End If

                Me.Close()
            Catch ex As Exception

            End Try
        Else
            If cCNVB.nVB_CutLng = 1 Then
                MsgBox("กรุณาเลือกรายการที่ต้องการส่งออก", MsgBoxStyle.Information)
            Else
                MsgBox("Please select information for export", MsgBoxStyle.Information)
            End If

        End If

    End Sub

    Private headerBox As CheckBox
    Private Sub W_SETxCheckboxSaleAllow()
        Dim checkboxHeader As CheckBox = New CheckBox()
        Dim rect As Rectangle = ogdSale.GetCellDisplayRectangle(4, -1, True)
        rect.X = 20
        rect.Y = 7
        With checkboxHeader
            .BackColor = Color.Transparent
        End With
        checkboxHeader.Name = "checkboxHeader"
        checkboxHeader.Size = New Size(14, 14)
        checkboxHeader.Location = rect.Location
        checkboxHeader.Checked = True
        AddHandler checkboxHeader.CheckedChanged, AddressOf checkboxHeader_CheckedChanged
        ogdSale.Controls.Add(checkboxHeader)
    End Sub

    Private Sub checkboxHeader_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        headerBox = DirectCast(ogdSale.Controls.Find("checkboxHeader", True)(0), CheckBox)
        For Each row As DataGridViewRow In ogdSale.Rows
            row.Cells(0).Value = headerBox.Checked
        Next
        ogdSale.RefreshEdit()
    End Sub

    Private oHeaderBoxReturn As CheckBox
    Private Sub W_SETxCheckboxReturnAllow()
        Dim oCheckboxHeaderReturn As CheckBox = New CheckBox()
        Dim rect As Rectangle = ogdReturn.GetCellDisplayRectangle(4, -1, True)
        rect.X = 20
        rect.Y = 7
        With oCheckboxHeaderReturn
            .BackColor = Color.Transparent
        End With
        oCheckboxHeaderReturn.Name = "checkboxHeaderReturn"
        oCheckboxHeaderReturn.Size = New Size(14, 14)
        oCheckboxHeaderReturn.Location = rect.Location
        oCheckboxHeaderReturn.Checked = True
        AddHandler oCheckboxHeaderReturn.CheckedChanged, AddressOf checkboxHeaderReturn_CheckedChanged
        ogdReturn.Controls.Add(oCheckboxHeaderReturn)
    End Sub

    Private Sub checkboxHeaderReturn_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        oHeaderBoxReturn = DirectCast(ogdReturn.Controls.Find("checkboxHeaderReturn", True)(0), CheckBox)
        For Each row As DataGridViewRow In ogdReturn.Rows
            row.Cells(0).Value = oHeaderBoxReturn.Checked
        Next
        ogdReturn.RefreshEdit()
    End Sub

    Private Sub wExportSale_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        W_GETxDataSale(odpDate.Value)
        W_GETxDataReturn(odpDate.Value)
        cCNSP.SP_FrmSetCapControl(Me)
    End Sub
End Class