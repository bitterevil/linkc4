﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wExportGrpCst
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wExportGrpCst))
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ocmSelect = New System.Windows.Forms.Button()
        Me.olaCstGrp = New System.Windows.Forms.Label()
        Me.ocmClose = New System.Windows.Forms.Button()
        Me.ogbGrpCst = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.ocmSearch = New System.Windows.Forms.Button()
        Me.otbCst = New System.Windows.Forms.TextBox()
        Me.olaCst = New System.Windows.Forms.Label()
        Me.ogdGrpCst = New System.Windows.Forms.DataGridView()
        Me.opnMain.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.ogbGrpCst.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.ogdGrpCst, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 3
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.4!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.6!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 17.0!))
        Me.opnMain.Controls.Add(Me.Panel1, 1, 3)
        Me.opnMain.Controls.Add(Me.ogbGrpCst, 1, 2)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 4
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 324.0!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me.opnMain.Size = New System.Drawing.Size(869, 426)
        Me.opnMain.TabIndex = 1
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ocmSelect)
        Me.Panel1.Controls.Add(Me.olaCstGrp)
        Me.Panel1.Controls.Add(Me.ocmClose)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(23, 344)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(825, 79)
        Me.Panel1.TabIndex = 1
        '
        'ocmSelect
        '
        Me.ocmSelect.Location = New System.Drawing.Point(605, 19)
        Me.ocmSelect.Name = "ocmSelect"
        Me.ocmSelect.Size = New System.Drawing.Size(104, 29)
        Me.ocmSelect.TabIndex = 5
        Me.ocmSelect.Tag = "2;เลือก;Select"
        Me.ocmSelect.Text = "Select"
        Me.ocmSelect.UseVisualStyleBackColor = True
        '
        'olaCstGrp
        '
        Me.olaCstGrp.AutoSize = True
        Me.olaCstGrp.Location = New System.Drawing.Point(85, 8)
        Me.olaCstGrp.Name = "olaCstGrp"
        Me.olaCstGrp.Size = New System.Drawing.Size(0, 17)
        Me.olaCstGrp.TabIndex = 4
        '
        'ocmClose
        '
        Me.ocmClose.Location = New System.Drawing.Point(715, 19)
        Me.ocmClose.Name = "ocmClose"
        Me.ocmClose.Size = New System.Drawing.Size(103, 29)
        Me.ocmClose.TabIndex = 2
        Me.ocmClose.Tag = "2;ปิด;Close"
        Me.ocmClose.Text = "Close"
        Me.ocmClose.UseVisualStyleBackColor = True
        '
        'ogbGrpCst
        '
        Me.ogbGrpCst.Controls.Add(Me.TableLayoutPanel2)
        Me.ogbGrpCst.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbGrpCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbGrpCst.Location = New System.Drawing.Point(23, 20)
        Me.ogbGrpCst.Name = "ogbGrpCst"
        Me.ogbGrpCst.Size = New System.Drawing.Size(825, 318)
        Me.ogbGrpCst.TabIndex = 2
        Me.ogbGrpCst.TabStop = False
        Me.ogbGrpCst.Tag = "2;เลือกข้อมูลกลุ่มลูกค้าที่ต้องการส่งออก;Select Information Group Customer for ex" &
    "port"
        Me.ogbGrpCst.Text = "เลือกข้อมูลกลุ่มลูกค้าที่ต้องการส่งออก"
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.669759!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 98.33024!))
        Me.TableLayoutPanel2.Controls.Add(Me.Panel4, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.ogdGrpCst, 1, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 22)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 301.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 65.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(819, 293)
        Me.TableLayoutPanel2.TabIndex = 3
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.ocmSearch)
        Me.Panel4.Controls.Add(Me.otbCst)
        Me.Panel4.Controls.Add(Me.olaCst)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(16, 3)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(800, 41)
        Me.Panel4.TabIndex = 0
        '
        'ocmSearch
        '
        Me.ocmSearch.Location = New System.Drawing.Point(283, 5)
        Me.ocmSearch.Name = "ocmSearch"
        Me.ocmSearch.Size = New System.Drawing.Size(106, 29)
        Me.ocmSearch.TabIndex = 5
        Me.ocmSearch.Tag = "2;ค้นหา;Search"
        Me.ocmSearch.Text = "ค้นหา"
        Me.ocmSearch.UseVisualStyleBackColor = True
        '
        'otbCst
        '
        Me.otbCst.Location = New System.Drawing.Point(125, 7)
        Me.otbCst.Name = "otbCst"
        Me.otbCst.Size = New System.Drawing.Size(152, 26)
        Me.otbCst.TabIndex = 4
        '
        'olaCst
        '
        Me.olaCst.AutoSize = True
        Me.olaCst.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaCst.Location = New System.Drawing.Point(-4, 10)
        Me.olaCst.Name = "olaCst"
        Me.olaCst.Size = New System.Drawing.Size(118, 20)
        Me.olaCst.TabIndex = 3
        Me.olaCst.Tag = "2;      รหัสกลุ่มลูกค้า :;GroupCode :"
        Me.olaCst.Text = "รหัสกลุ่มลูกค้า : "
        '
        'ogdGrpCst
        '
        Me.ogdGrpCst.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.ogdGrpCst.Location = New System.Drawing.Point(16, 50)
        Me.ogdGrpCst.Name = "ogdGrpCst"
        Me.ogdGrpCst.RowTemplate.Height = 24
        Me.ogdGrpCst.Size = New System.Drawing.Size(800, 232)
        Me.ogdGrpCst.TabIndex = 1
        '
        'wExportGrpCst
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(869, 426)
        Me.Controls.Add(Me.opnMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wExportGrpCst"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;ส่งออกข้อมูลกลุ่มลูกค้า;Export GroupCustomer"
        Me.Text = "ส่งออกข้อมูลกลุ่มลูกค้า"
        Me.opnMain.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ogbGrpCst.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.ogdGrpCst, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ocmClose As Button
    Friend WithEvents ogbGrpCst As GroupBox
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents ocmSearch As Button
    Friend WithEvents otbCst As TextBox
    Friend WithEvents olaCst As Label
    Friend WithEvents ogdGrpCst As DataGridView
    Friend WithEvents olaCstGrp As Label
    Friend WithEvents ocmSelect As Button
End Class
