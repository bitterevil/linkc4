﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class wEditSharing
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(wEditSharing))
        Me.ogbMain = New System.Windows.Forms.GroupBox()
        Me.olaKey = New System.Windows.Forms.Label()
        Me.ocmCancle = New System.Windows.Forms.Button()
        Me.ocmSave = New System.Windows.Forms.Button()
        Me.otbGp = New System.Windows.Forms.TextBox()
        Me.ocbSharing = New System.Windows.Forms.ComboBox()
        Me.olaGP = New System.Windows.Forms.Label()
        Me.olaSharing = New System.Windows.Forms.Label()
        Me.ogbMain.SuspendLayout()
        Me.SuspendLayout()
        '
        'ogbMain
        '
        Me.ogbMain.Controls.Add(Me.olaKey)
        Me.ogbMain.Controls.Add(Me.ocmCancle)
        Me.ogbMain.Controls.Add(Me.ocmSave)
        Me.ogbMain.Controls.Add(Me.otbGp)
        Me.ogbMain.Controls.Add(Me.ocbSharing)
        Me.ogbMain.Controls.Add(Me.olaGP)
        Me.ogbMain.Controls.Add(Me.olaSharing)
        Me.ogbMain.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbMain.Location = New System.Drawing.Point(12, 12)
        Me.ogbMain.Name = "ogbMain"
        Me.ogbMain.Size = New System.Drawing.Size(476, 168)
        Me.ogbMain.TabIndex = 0
        Me.ogbMain.TabStop = False
        Me.ogbMain.Tag = "2;แก้ไขข้อมูล Sharing;Edit Sharing"
        Me.ogbMain.Text = "แก้ไขข้อมูล Sharing"
        '
        'olaKey
        '
        Me.olaKey.AutoSize = True
        Me.olaKey.Location = New System.Drawing.Point(307, 82)
        Me.olaKey.Name = "olaKey"
        Me.olaKey.Size = New System.Drawing.Size(59, 20)
        Me.olaKey.TabIndex = 6
        Me.olaKey.Text = "Label1"
        Me.olaKey.Visible = False
        '
        'ocmCancle
        '
        Me.ocmCancle.Location = New System.Drawing.Point(251, 128)
        Me.ocmCancle.Name = "ocmCancle"
        Me.ocmCancle.Size = New System.Drawing.Size(115, 30)
        Me.ocmCancle.TabIndex = 5
        Me.ocmCancle.Tag = "2;ยกเลิก;Cancle"
        Me.ocmCancle.Text = "ยกเลิก"
        Me.ocmCancle.UseVisualStyleBackColor = True
        '
        'ocmSave
        '
        Me.ocmSave.Location = New System.Drawing.Point(130, 128)
        Me.ocmSave.Name = "ocmSave"
        Me.ocmSave.Size = New System.Drawing.Size(115, 30)
        Me.ocmSave.TabIndex = 4
        Me.ocmSave.Tag = "2;บันทึก;Save"
        Me.ocmSave.Text = "บันทึก"
        Me.ocmSave.UseVisualStyleBackColor = True
        '
        'otbGp
        '
        Me.otbGp.Location = New System.Drawing.Point(172, 79)
        Me.otbGp.Name = "otbGp"
        Me.otbGp.Size = New System.Drawing.Size(104, 26)
        Me.otbGp.TabIndex = 3
        '
        'ocbSharing
        '
        Me.ocbSharing.FormattingEnabled = True
        Me.ocbSharing.Location = New System.Drawing.Point(172, 31)
        Me.ocbSharing.Name = "ocbSharing"
        Me.ocbSharing.Size = New System.Drawing.Size(282, 28)
        Me.ocbSharing.TabIndex = 2
        '
        'olaGP
        '
        Me.olaGP.AutoSize = True
        Me.olaGP.Location = New System.Drawing.Point(96, 82)
        Me.olaGP.Name = "olaGP"
        Me.olaGP.Size = New System.Drawing.Size(70, 20)
        Me.olaGP.TabIndex = 1
        Me.olaGP.Text = "GP(%) :"
        '
        'olaSharing
        '
        Me.olaSharing.AutoSize = True
        Me.olaSharing.Location = New System.Drawing.Point(47, 39)
        Me.olaSharing.Name = "olaSharing"
        Me.olaSharing.Size = New System.Drawing.Size(120, 20)
        Me.olaSharing.TabIndex = 0
        Me.olaSharing.Text = "Sharing Code :"
        '
        'wEditSharing
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(500, 192)
        Me.Controls.Add(Me.ogbMain)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "wEditSharing"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Tag = "2;แก้ไข;Edit"
        Me.Text = "Edit"
        Me.ogbMain.ResumeLayout(False)
        Me.ogbMain.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ogbMain As GroupBox
    Friend WithEvents ocmCancle As Button
    Friend WithEvents ocmSave As Button
    Friend WithEvents otbGp As TextBox
    Friend WithEvents ocbSharing As ComboBox
    Friend WithEvents olaGP As Label
    Friend WithEvents olaSharing As Label
    Friend WithEvents olaKey As Label
End Class
