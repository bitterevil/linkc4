﻿Public Class cZGLINT002
    Property BUKRS As String = "1000"
    Property BLART As String = "IF"
    Property BLDAT As String = "" 'FDMibDocDate
    Property BUDAT As String = "" 'FDMibDocDate
    Property WAERS As String = "THB"
    Property XBLNR As String = "" 'FDDateSalStart
    Property BRNCH As String = "" 'BchMapping + FTBchCode
    Property BUZEI As String = "001"
    Property BSCHL As String = "40"
    Property HKONT As String = "" 'FTAbkRefCode
    Property WRBTR As String = "" 'FCMibQty
    Property WERKS As String = "" 'Plant Mapping
    Property SGTXT As String = "" 'FTBnkCode + CONVERT(VARCHAR(18), FCMibQty) + FDMibDocDate + FDDateSalStart
    '*CH 12-10-2017
    Property FTMibDocNo As String = ""
    Property FTExpFileName As String = ""
    Property FTExpRunning As String = ""
End Class
