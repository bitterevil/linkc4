﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class cUsrConfiguration
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.opnMain = New System.Windows.Forms.TableLayoutPanel()
        Me.ogbMember = New System.Windows.Forms.GroupBox()
        Me.otbSaleTran = New System.Windows.Forms.TextBox()
        Me.otbMemGrp = New System.Windows.Forms.TextBox()
        Me.otbMember = New System.Windows.Forms.TextBox()
        Me.olaSaleTran = New System.Windows.Forms.Label()
        Me.olaMemGrp = New System.Windows.Forms.Label()
        Me.olaMember = New System.Windows.Forms.Label()
        Me.ogbAPI = New System.Windows.Forms.GroupBox()
        Me.otbTokenSecret = New System.Windows.Forms.TextBox()
        Me.otbKeySecret = New System.Windows.Forms.Label()
        Me.otbCusSecret = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.otbCusKey = New System.Windows.Forms.TextBox()
        Me.otbContent = New System.Windows.Forms.TextBox()
        Me.otbMethod = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.olaContent = New System.Windows.Forms.Label()
        Me.olaSale = New System.Windows.Forms.Label()
        Me.olaGrp = New System.Windows.Forms.Label()
        Me.olaMethod = New System.Windows.Forms.Label()
        Me.olaStatusExp = New System.Windows.Forms.Label()
        Me.otbKey = New System.Windows.Forms.TextBox()
        Me.olaKey = New System.Windows.Forms.Label()
        Me.opnMain.SuspendLayout()
        Me.ogbMember.SuspendLayout()
        Me.ogbAPI.SuspendLayout()
        Me.SuspendLayout()
        '
        'opnMain
        '
        Me.opnMain.ColumnCount = 2
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.469136!))
        Me.opnMain.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 97.53086!))
        Me.opnMain.Controls.Add(Me.ogbMember, 1, 1)
        Me.opnMain.Controls.Add(Me.ogbAPI, 1, 0)
        Me.opnMain.Dock = System.Windows.Forms.DockStyle.Fill
        Me.opnMain.Location = New System.Drawing.Point(0, 0)
        Me.opnMain.Name = "opnMain"
        Me.opnMain.RowCount = 2
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.42029!))
        Me.opnMain.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.57971!))
        Me.opnMain.Size = New System.Drawing.Size(648, 533)
        Me.opnMain.TabIndex = 6
        '
        'ogbMember
        '
        Me.ogbMember.Controls.Add(Me.otbSaleTran)
        Me.ogbMember.Controls.Add(Me.otbMemGrp)
        Me.ogbMember.Controls.Add(Me.otbMember)
        Me.ogbMember.Controls.Add(Me.olaSaleTran)
        Me.ogbMember.Controls.Add(Me.olaMemGrp)
        Me.ogbMember.Controls.Add(Me.olaMember)
        Me.ogbMember.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbMember.Location = New System.Drawing.Point(19, 319)
        Me.ogbMember.Name = "ogbMember"
        Me.ogbMember.Size = New System.Drawing.Size(626, 211)
        Me.ogbMember.TabIndex = 4
        Me.ogbMember.TabStop = False
        Me.ogbMember.Text = "URL Setting"
        '
        'otbSaleTran
        '
        Me.otbSaleTran.Location = New System.Drawing.Point(10, 162)
        Me.otbSaleTran.Name = "otbSaleTran"
        Me.otbSaleTran.Size = New System.Drawing.Size(587, 26)
        Me.otbSaleTran.TabIndex = 12
        '
        'otbMemGrp
        '
        Me.otbMemGrp.Location = New System.Drawing.Point(9, 103)
        Me.otbMemGrp.Name = "otbMemGrp"
        Me.otbMemGrp.Size = New System.Drawing.Size(588, 26)
        Me.otbMemGrp.TabIndex = 11
        '
        'otbMember
        '
        Me.otbMember.Location = New System.Drawing.Point(9, 42)
        Me.otbMember.Name = "otbMember"
        Me.otbMember.Size = New System.Drawing.Size(588, 26)
        Me.otbMember.TabIndex = 10
        '
        'olaSaleTran
        '
        Me.olaSaleTran.AutoSize = True
        Me.olaSaleTran.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaSaleTran.Location = New System.Drawing.Point(6, 139)
        Me.olaSaleTran.Name = "olaSaleTran"
        Me.olaSaleTran.Size = New System.Drawing.Size(145, 20)
        Me.olaSaleTran.TabIndex = 9
        Me.olaSaleTran.Text = "Sale Transaction :"
        '
        'olaMemGrp
        '
        Me.olaMemGrp.AutoSize = True
        Me.olaMemGrp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaMemGrp.Location = New System.Drawing.Point(5, 80)
        Me.olaMemGrp.Name = "olaMemGrp"
        Me.olaMemGrp.Size = New System.Drawing.Size(131, 20)
        Me.olaMemGrp.TabIndex = 8
        Me.olaMemGrp.Text = "Member Group :"
        '
        'olaMember
        '
        Me.olaMember.AutoSize = True
        Me.olaMember.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaMember.Location = New System.Drawing.Point(6, 22)
        Me.olaMember.Name = "olaMember"
        Me.olaMember.Size = New System.Drawing.Size(80, 20)
        Me.olaMember.TabIndex = 7
        Me.olaMember.Text = "Member :"
        '
        'ogbAPI
        '
        Me.ogbAPI.Controls.Add(Me.otbTokenSecret)
        Me.ogbAPI.Controls.Add(Me.otbKeySecret)
        Me.ogbAPI.Controls.Add(Me.otbCusSecret)
        Me.ogbAPI.Controls.Add(Me.Label2)
        Me.ogbAPI.Controls.Add(Me.otbCusKey)
        Me.ogbAPI.Controls.Add(Me.otbContent)
        Me.ogbAPI.Controls.Add(Me.otbMethod)
        Me.ogbAPI.Controls.Add(Me.Label1)
        Me.ogbAPI.Controls.Add(Me.olaContent)
        Me.ogbAPI.Controls.Add(Me.olaSale)
        Me.ogbAPI.Controls.Add(Me.olaGrp)
        Me.ogbAPI.Controls.Add(Me.olaMethod)
        Me.ogbAPI.Controls.Add(Me.olaStatusExp)
        Me.ogbAPI.Controls.Add(Me.otbKey)
        Me.ogbAPI.Controls.Add(Me.olaKey)
        Me.ogbAPI.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ogbAPI.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.ogbAPI.Location = New System.Drawing.Point(19, 3)
        Me.ogbAPI.Name = "ogbAPI"
        Me.ogbAPI.Size = New System.Drawing.Size(626, 310)
        Me.ogbAPI.TabIndex = 2
        Me.ogbAPI.TabStop = False
        Me.ogbAPI.Text = "APIs Setting"
        '
        'otbTokenSecret
        '
        Me.otbTokenSecret.Location = New System.Drawing.Point(9, 162)
        Me.otbTokenSecret.Name = "otbTokenSecret"
        Me.otbTokenSecret.Size = New System.Drawing.Size(588, 26)
        Me.otbTokenSecret.TabIndex = 27
        '
        'otbKeySecret
        '
        Me.otbKeySecret.AutoSize = True
        Me.otbKeySecret.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.otbKeySecret.Location = New System.Drawing.Point(5, 139)
        Me.otbKeySecret.Name = "otbKeySecret"
        Me.otbKeySecret.Size = New System.Drawing.Size(148, 20)
        Me.otbKeySecret.TabIndex = 26
        Me.otbKeySecret.Text = "TOKEN SECRET :"
        '
        'otbCusSecret
        '
        Me.otbCusSecret.Location = New System.Drawing.Point(10, 106)
        Me.otbCusSecret.Name = "otbCusSecret"
        Me.otbCusSecret.Size = New System.Drawing.Size(587, 26)
        Me.otbCusSecret.TabIndex = 25
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.Label2.Location = New System.Drawing.Point(6, 83)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(188, 20)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "CONSUMER SECRET :"
        '
        'otbCusKey
        '
        Me.otbCusKey.Location = New System.Drawing.Point(10, 51)
        Me.otbCusKey.Name = "otbCusKey"
        Me.otbCusKey.Size = New System.Drawing.Size(587, 26)
        Me.otbCusKey.TabIndex = 21
        '
        'otbContent
        '
        Me.otbContent.Location = New System.Drawing.Point(10, 273)
        Me.otbContent.Name = "otbContent"
        Me.otbContent.Size = New System.Drawing.Size(247, 26)
        Me.otbContent.TabIndex = 6
        '
        'otbMethod
        '
        Me.otbMethod.Location = New System.Drawing.Point(295, 273)
        Me.otbMethod.Name = "otbMethod"
        Me.otbMethod.Size = New System.Drawing.Size(302, 26)
        Me.otbMethod.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.Label1.Location = New System.Drawing.Point(6, 28)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(153, 20)
        Me.Label1.TabIndex = 20
        Me.Label1.Text = "CONSUMER KEY :"
        '
        'olaContent
        '
        Me.olaContent.AutoSize = True
        Me.olaContent.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaContent.Location = New System.Drawing.Point(291, 250)
        Me.olaContent.Name = "olaContent"
        Me.olaContent.Size = New System.Drawing.Size(74, 20)
        Me.olaContent.TabIndex = 3
        Me.olaContent.Text = "Method :"
        '
        'olaSale
        '
        Me.olaSale.AutoSize = True
        Me.olaSale.Location = New System.Drawing.Point(500, 13)
        Me.olaSale.Name = "olaSale"
        Me.olaSale.Size = New System.Drawing.Size(18, 20)
        Me.olaSale.TabIndex = 19
        Me.olaSale.Text = "0"
        Me.olaSale.Visible = False
        '
        'olaGrp
        '
        Me.olaGrp.AutoSize = True
        Me.olaGrp.Location = New System.Drawing.Point(465, 13)
        Me.olaGrp.Name = "olaGrp"
        Me.olaGrp.Size = New System.Drawing.Size(18, 20)
        Me.olaGrp.TabIndex = 18
        Me.olaGrp.Text = "0"
        Me.olaGrp.Visible = False
        '
        'olaMethod
        '
        Me.olaMethod.AutoSize = True
        Me.olaMethod.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaMethod.Location = New System.Drawing.Point(6, 250)
        Me.olaMethod.Name = "olaMethod"
        Me.olaMethod.Size = New System.Drawing.Size(113, 20)
        Me.olaMethod.TabIndex = 2
        Me.olaMethod.Text = "Content type :"
        '
        'olaStatusExp
        '
        Me.olaStatusExp.AutoSize = True
        Me.olaStatusExp.Location = New System.Drawing.Point(426, 13)
        Me.olaStatusExp.Name = "olaStatusExp"
        Me.olaStatusExp.Size = New System.Drawing.Size(18, 20)
        Me.olaStatusExp.TabIndex = 17
        Me.olaStatusExp.Text = "0"
        Me.olaStatusExp.Visible = False
        '
        'otbKey
        '
        Me.otbKey.Location = New System.Drawing.Point(10, 218)
        Me.otbKey.Name = "otbKey"
        Me.otbKey.Size = New System.Drawing.Size(587, 26)
        Me.otbKey.TabIndex = 4
        '
        'olaKey
        '
        Me.olaKey.AutoSize = True
        Me.olaKey.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!)
        Me.olaKey.Location = New System.Drawing.Point(6, 195)
        Me.olaKey.Name = "olaKey"
        Me.olaKey.Size = New System.Drawing.Size(86, 20)
        Me.olaKey.TabIndex = 1
        Me.olaKey.Text = "Token ID :"
        '
        'cUsrConfiguration
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.opnMain)
        Me.Name = "cUsrConfiguration"
        Me.Size = New System.Drawing.Size(648, 533)
        Me.opnMain.ResumeLayout(False)
        Me.ogbMember.ResumeLayout(False)
        Me.ogbMember.PerformLayout()
        Me.ogbAPI.ResumeLayout(False)
        Me.ogbAPI.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents opnMain As TableLayoutPanel
    Friend WithEvents ogbMember As GroupBox
    Friend WithEvents otbSaleTran As TextBox
    Friend WithEvents otbMemGrp As TextBox
    Friend WithEvents otbMember As TextBox
    Friend WithEvents olaSaleTran As Label
    Friend WithEvents olaMemGrp As Label
    Friend WithEvents olaMember As Label
    Friend WithEvents ogbAPI As GroupBox
    Friend WithEvents otbTokenSecret As TextBox
    Friend WithEvents otbKeySecret As Label
    Friend WithEvents otbCusSecret As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents otbCusKey As TextBox
    Friend WithEvents otbContent As TextBox
    Friend WithEvents otbMethod As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents olaContent As Label
    Friend WithEvents olaSale As Label
    Friend WithEvents olaGrp As Label
    Friend WithEvents olaMethod As Label
    Friend WithEvents olaStatusExp As Label
    Friend WithEvents otbKey As TextBox
    Friend WithEvents olaKey As Label
End Class
