﻿Public Class cCNEN

    'select message style
    Public Enum eEN_MSGStyle
        nEN_MSGErr = 1
        nEN_MSGWarn = 2
        nEN_MSGInfo = 3
        nEN_MSGYesNo = 4
        nEN_MSGOkCancel = 5
        nEN_MSGYesNoCancel = 6
    End Enum

    'cut caption
    Public Enum eEN_CutCaption
        nEN_Cut1Msg = 1
        nEN_Cut2Tag = 2
        nEN_Cut3Const = 3
    End Enum

    Public Enum eEN_SellWeight
        nEN_AddOrder = 1
        nEN_EditOrder = 2
    End Enum

    Public Enum eEN_DisplayName
        nEN_Short = 0
        nEN_Long = 1
    End Enum

    Public Enum eEN_Align
        nEN_Left = 1
        nEN_Right = 2
    End Enum

    Public Enum enStaPrn
        Preview = 1
        Print = 2
    End Enum

    Public Enum eLanguage
        Lang1 = 1
        Lang2 = 2
    End Enum

End Class