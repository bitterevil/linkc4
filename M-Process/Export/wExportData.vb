﻿Imports System.IO
Imports System.Text

Public Class wExportData

    Public Shared nC_CreateFile As Integer = 0
    Private tW_CapPackage As String = "รายการ|Catalog"
    Private tW_CapHDCD As String = "รายการ|Catalog"
    Private tW_CapButton As String = "..."
    Private tW_To As String = "ถึง|To"
    Private tW_Cst As String = "ข้อมูลลูกค้า|Information Customer."
    Private tW_GrpCst As String = "ข้อมูลกลุ่มลูกค้า|Information Customer Group."
    Private tW_Sale As String = "ข้อมูลการขาย|Information Sale."
    Private tW_HeaderButton As String = "เงื่อนไข|Conditions"
    Private tW_Button As String = "...|..."
    Public tW_TranDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_SaleDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_PayDate As String = Now.ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
    Public tW_BackupData As String = Now.ToString("yyyyMMddHHmmss", New System.Globalization.CultureInfo("en-US"))
    Private bW_ChkSale As Boolean = True
    Private bW_ChkCst As Boolean = True
    Private bW_ChkGrp As Boolean = True
    Private dW_StartTime As DateTime
    Private dW_EndTime As DateTime
    Private tW_FileName As String = ""
    Private tW_AlertMsgData As String = "ไม่มีการอัพเดทข้อมูลประจำวัน|Today no data update"
    Private Const tW_Success As String = "สำเร็จ;Success"
    Private Const tW_Unsuccess As String = "ไม่สำเร็จ;Unsuccessful"
    Private Const tW_StatusExport As String = "กำลังส่งออกไฟล์;Exporting file"


    Public Shared nC_CreateFileCst As Integer = 0
    Public Shared nC_CreateFileGrp As Integer = 0
    Public Shared nC_CreateFileSale As Integer = 0

    Public Shared nW_Success As Integer = 0
    Public Shared nW_Fail As Integer = 0
    Public Shared nW_ServiceNotfound As Integer = 0
    Public Shared bW_StatusExport As Boolean = False
    Public bW_DoworkRuntime As Boolean = False

    Private Sub wExportData_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_SETxGridExport()
        W_SETxListExport()

        'CreateTable
        Dim oUrsGP As New cUrsGP
        C_CALxFindTable()
        wSettingEDC.W_GENxTableMapping()
        oUrsGP.C_GENxTableMapping()



    End Sub


    Public Sub W_SETxGridExport()
        Dim tHeadPackage As String = ""
        Dim tHeadCD As String = ""
        Dim tHeadBt As String = ""
        Dim atHeadPackage() As String
        Dim atHeadCD() As String
        Dim atHeadButton() As String
        Dim nLeng As Integer = 0

        Try
            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
            atHeadPackage = tW_CapPackage.Split("|")
            atHeadCD = tW_CapHDCD.Split("|")
            atHeadButton = tW_HeaderButton.Split("|")
            tHeadPackage = atHeadPackage(nLeng)
            tHeadCD = atHeadCD(nLeng)
            tHeadBt = atHeadButton(nLeng)

            Dim oRowNo As New DataGridViewTextBoxColumn
            oRowNo.HeaderText = ""
            oRowNo.Width = 0
            oRowNo.ReadOnly = True
            oRowNo.Visible = False
            ogdExport.Columns.Add(oRowNo)


            Dim ockExp As New DataGridViewCheckBoxColumn()
            ockExp.HeaderText = ""
            ockExp.Width = 50
            ogdExport.Columns.Add(ockExp)

            Dim oPackage As New DataGridViewTextBoxColumn
            oPackage.HeaderText = tHeadCD
            oPackage.Width = 130
            oPackage.ReadOnly = True
            ogdExport.Columns.Add(oPackage)

            Dim ocmDate As New DataGridViewButtonColumn
            ocmDate.HeaderText = tHeadBt
            ocmDate.Width = 70
            ocmDate.CellTemplate.Style.Padding = New Padding(17, 0, 17, 0)

            ogdExport.Columns.Add(ocmDate)


            ogdExport.EnableHeadersVisualStyles = False
            ogdExport.Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            ogdExport.Columns(3).HeaderCell.Style.Alignment = DataGridViewContentAlignment.BottomCenter
            ogdExport.ColumnHeadersHeight = 25
            ogdExport.RowHeadersVisible = False
            ogdExport.AllowUserToAddRows = False

        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_SETxListExport()


        Dim nLeng As Integer = 0
        Dim atTo() As String
        Dim tTo As String

        Dim atGrp() As String
        Dim tCstGrp As String
        Dim oRowGrp As String()
        Dim tNoGrp As String = "001"

        Dim atCst() As String
        Dim tCst As String
        Dim oRowCst As String()
        Dim tNoCst As String = "002"

        Dim atSale() As String
        Dim tSale As String
        Dim oRowSale As String()
        Dim tNoSale As String = "003"
        Dim atButton() As String
        Dim tButton As String



        Try

            If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
            ogdExport.Rows.Clear()
            atTo = tW_To.Split("|")
            'atCapDetailCD = .Split("|")
            atCst = tW_Cst.Split("|")
            atGrp = tW_GrpCst.Split("|")
            atSale = tW_Sale.Split("|")
            atButton = tW_Button.Split("|")

            tTo = atTo(nLeng)
            'tCapDetailCD = atCapDetailCD(nLeng)
            tCst = atCst(nLeng)
            tCstGrp = atGrp(nLeng)
            tSale = atSale(nLeng)
            tButton = atButton(nLeng)

            oRowGrp = New String() {tNoGrp, bW_ChkSale, tCstGrp, tButton}
            ogdExport.Rows.Add(oRowGrp)

            oRowCst = New String() {tNoCst, bW_ChkCst, tCst, tButton}
            ogdExport.Rows.Add(oRowCst)

            oRowSale = New String() {tNoSale, bW_ChkGrp, tSale, tButton}
            ogdExport.Rows.Add(oRowSale)

        Catch ex As Exception
        End Try




    End Sub

    Private Sub ogdExport_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdExport.CellContentClick
        If e.ColumnIndex = 3 Then
            Select Case e.RowIndex
                Case 0
                    wExportGrpCst.ShowDialog()
                    wExportGrpCst.Dispose()
                Case 1
                    wExportCst.ShowDialog()
                    wExportCst.Dispose()
                Case 2
                    wExportSale.ShowDialog()
                    wExportSale.Dispose()
            End Select
        End If
    End Sub

    ''' <summary>
    ''' สร้าง Table Running Number.
    ''' </summary>
    Public Sub C_CALxFindTable()
        Try
            Dim oSQL As New StringBuilder
            Dim oDatabase As New cDatabaseLocal

            oSQL.Clear()
            oSQL.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSQL.AppendLine("WHERE object_id = OBJECT_ID(N'TCNTPdtRunning') AND type in (N'U'))")
            oSQL.AppendLine("BEGIN")
            oSQL.AppendLine("CREATE TABLE [dbo].[TCNTPdtRunning]")
            oSQL.AppendLine("([FNNum] [int] IDENTITY(1,1) NOT NULL,")
            oSQL.AppendLine("[FTRunningNumber] [varchar](50) NULL,")
            oSQL.AppendLine(" [FTPos] [varchar](50) NOT NULL, ")
            oSQL.AppendLine(" [FTBranch] [varchar](50) NOT NULL, ")
            oSQL.AppendLine(" CONSTRAINT [PK_TCNTPdtRunning] PRIMARY KEY CLUSTERED ")
            oSQL.AppendLine(" (")
            oSQL.AppendLine(" [FTPos] ASC,[FTBranch] ASC")
            oSQL.AppendLine(" )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
            oSQL.AppendLine(") ON [PRIMARY]")
            oSQL.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSQL.ToString)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmExport_Click(sender As Object, e As EventArgs) Handles ocmExport.Click

        Try
            If ogdExport.Rows(0).Cells(1).Value = False And ogdExport.Rows(1).Cells(1).Value = False And ogdExport.Rows(2).Cells(1).Value = False Then

                If cCNVB.nVB_CutLng = 1 Then
                    MsgBox("กรุณาเลือกรายการที่ต้องการส่งออก", MsgBoxStyle.Information)
                Else
                    MsgBox("Please select information ", MsgBoxStyle.Information)
                End If

                Exit Sub
            Else

                If Not obgWorker.IsBusy Then
                    obgWorker.RunWorkerAsync()
                End If

            End If

        Catch ex As Exception

        End Try

    End Sub

    Public Sub W_EXPxExportData()
        Dim nLeng As Integer = 0
        If AdaConfig.cConfig.oApplication.nLanguage = 2 Then nLeng = 1
        Dim oExpSale As New cExportSale
        Dim tSelect As String = ""
        Dim tType As String = ""
        Dim tExportFirst As String = ""
        Dim tCstCode As String = ""
        Dim tW_DocNo As String = ""
        Dim atCaption() As String
        Dim tStatusReturn As String = ""
        Dim tDate As String = Convert.ToDateTime(wExportSale.oW_tDate).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))
        Dim tDocNo As String = ""
        Dim tBchCode As String = ""
        Dim tPosCode As String = ""
        Dim tSaleAmt As String = ""
        Dim tW_DateExport As String = Convert.ToDateTime(Now).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
        Dim oExpData As New wExportData
        Dim oExpSaleData As New wExportSale
        Dim oExpCst As New wExportCst
        Dim oExpGrpCst As New wExportGrpCst
        atCaption = tW_AlertMsgData.Split("|")

        'Check ถ้ามีการเลือกข้อมูลทั้งหมด
        If ogdExport.Rows(0).Cells(1).Value = True And
            ogdExport.Rows(1).Cells(1).Value = True And ogdExport.Rows(2).Cells(1).Value = True And bW_StatusExport = False Then
            System.Threading.Thread.Sleep(1000)
            obgWorker.ReportProgress(0)
            oExpGrpCst.W_SEToColumnGroupCustomer()  'สร้าง Column กลุ่มลูกค้า
            oExpGrpCst.W_GETdataGridGrpCst("")  'โหลดข้อมูลกลุ่มลูกค้าลง Grid
            oExpGrpCst.W_EXPxExportDataGroupCustomer() 'Export ข้อมูลกลุ่มลูกค้า
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
            System.Threading.Thread.Sleep(100)
            obgWorker.ReportProgress(100)

            System.Threading.Thread.Sleep(1000)
            obgWorker.ReportProgress(0)
            oExpCst.W_SEToColumnCustomer()  'สร้าง Column ลูกค้า
            oExpCst.W_GETdataGridCst("")    'โหลดข้อมูลลูกค้าลง Grid
            oExpCst.W_EXPxExportDataCustomer() 'Export ข้อมูลลูกค้า
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
            System.Threading.Thread.Sleep(100)
            obgWorker.ReportProgress(100)

            System.Threading.Thread.Sleep(1000)
            obgWorker.ReportProgress(0)
            oExpSaleData.W_SEToColumnSale()  'สร้าง Column Sale
            oExpSaleData.W_SEToColumnReturn() 'สร้าง Column Return
            oExpSaleData.W_GETxDataSale(Date.Now)  'Load Column Sale
            oExpSaleData.W_GETxDataReturn(Date.Now) 'Load Column Return
            oExpSaleData.W_EXPxExportDate(Date.Now)  'Export Process
            oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
            System.Threading.Thread.Sleep(100)
            obgWorker.ReportProgress(100)
            Exit Sub
        End If

        'กรณีเลือกบางรางรายการ 
        If ogdExport.Rows(0).Cells(1).Value = True And bW_StatusExport = False Then
            'กรณีเลือก เฉพาะ รหัสกลุ่มลูกค้า

            oExpGrpCst.W_SEToColumnGroupCustomer()  'สร้าง Column กลุ่มลูกค้า
            'โหลดข้อมูลกลุ่มลูกค้าลง Grid
            If oExpGrpCst.W_GETdataGridGrpCst("") = True Then
                'Export ข้อมูลกลุ่มลูกค้า
                System.Threading.Thread.Sleep(1000)
                obgWorker.ReportProgress(0)
                oExpGrpCst.W_EXPxExportDataGroupCustomer()
                oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
                System.Threading.Thread.Sleep(100)
                obgWorker.ReportProgress(100)
            Else
                MsgBox(atCaption(nLeng), MsgBoxStyle.Information)
                bW_DoworkRuntime = True
                Exit Sub
            End If


        End If

        If ogdExport.Rows(1).Cells(1).Value = True And bW_StatusExport = False Then

            'กรณีเลือก เฉพาะรหัสลูกค้า 
            oExpCst.W_SEToColumnCustomer()  'สร้าง Column ลูกค้า
            If oExpCst.W_GETdataGridCst("") = True Then 'โหลดข้อมูลลูกค้าลง Grid
                System.Threading.Thread.Sleep(1000)
                obgWorker.ReportProgress(0)
                oExpCst.W_EXPxExportDataCustomer() 'Export ข้อมูลลูกค้า
                oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
                System.Threading.Thread.Sleep(100)
                obgWorker.ReportProgress(100)
            Else
                MsgBox(atCaption(nLeng), MsgBoxStyle.Information)
                bW_DoworkRuntime = True
                Exit Sub
            End If

        End If

        If ogdExport.Rows(2).Cells(1).Value = True And bW_StatusExport = False Then

            'กรณีเลือก เฉพาะ Sale

            oExpSaleData.W_SEToColumnSale()  'สร้าง Column Sale
            oExpSaleData.W_SEToColumnReturn() 'สร้าง Column Return
            If oExpSaleData.W_GETxDataSale(Date.Now) Or oExpSaleData.W_GETxDataReturn(Date.Now) = True Then  'Load data Sale/return
                System.Threading.Thread.Sleep(1000)
                obgWorker.ReportProgress(0)
                oExpSaleData.W_EXPxExportDate(Date.Now)  'Export Process
                oExpData.W_SETxWriteSuccess() 'ย้าย File Log Success
                System.Threading.Thread.Sleep(100)
                obgWorker.ReportProgress(100)
            Else
                MsgBox(atCaption(nLeng), MsgBoxStyle.Information)
                bW_DoworkRuntime = True
                Exit Sub
            End If

        End If

        'END กรณีเลือกบางรางรายการ 


        'กรณีเลือกรายการข้างใน 
        'วนหาแถวที่มีการเลือก 
        If bW_StatusExport = True Then
            For nRowMain As Integer = 0 To ogdExport.Rows.Count - 1
                If ogdExport.Rows(nRowMain).Cells(1).Value = True Then
                    Select Case ogdExport.Rows(nRowMain).Cells(0).Value
                        Case "001"
                            'ส่งออกข้อมูลกลุ่มลูกค้า 
                            If wExportGrpCst.oW_oDbtblTmpCgpCstExp IsNot Nothing AndAlso wExportGrpCst.oW_oDbtblTmpCgpCstExp.Rows.Count > 0 Then
                                For nRowCstGrp As Integer = 0 To wExportGrpCst.oW_oDbtblTmpCgpCstExp.Rows.Count - 1
                                    System.Threading.Thread.Sleep(1000)
                                    obgWorker.ReportProgress(0)
                                    tCstCode = wExportGrpCst.oW_oDbtblTmpCgpCstExp.Rows(nRowCstGrp)("FTCgpCode").ToString()
                                    oExpSale.W_SETxSaveStatusExpFirst("ExportGrpFirst")
                                    If oExpSale.C_EXPbExportGrpCustomer(tCstCode) Then
                                        System.Threading.Thread.Sleep(100)
                                        obgWorker.ReportProgress(100)
                                    End If
                                Next

                                'ย้าย File จาก Process มาไว้ใน Log
                                W_SETxWriteSuccess()
                            End If
                        Case "002"
                            'ส่งออกข้อมูลลูกค้า
                            If wExportCst.oW_oDbtblTmpCstExp IsNot Nothing AndAlso wExportCst.oW_oDbtblTmpCstExp.Rows.Count > 0 Then
                                For nRowCst As Integer = 0 To wExportCst.oW_oDbtblTmpCstExp.Rows.Count - 1
                                    'Progress running
                                    System.Threading.Thread.Sleep(1000)
                                    obgWorker.ReportProgress(0)
                                    'END  Progress running
                                    oExpSale.W_SETxSaveStatusExpFirst("ExportCstFirst")
                                    tCstCode = wExportCst.oW_oDbtblTmpCstExp.Rows(nRowCst)("FTCstCode").ToString()
                                    tW_DocNo = tCstCode

                                    If oExpSale.C_EXPbExportCustomer(tCstCode) Then
                                        System.Threading.Thread.Sleep(100)
                                        obgWorker.ReportProgress(100)
                                    End If
                                Next
                                'ย้าย File จาก Process มาไว้ใน Log
                                W_SETxWriteSuccess()
                            End If

                        Case "003"
                            'Export ประเภทขาย  /คืน
                            'Export ประเภทขาย 
                            If wExportSale.oW_oDbtblSaleExp IsNot Nothing AndAlso wExportSale.oW_oDbtblSaleExp.Rows.Count > 0 Then
                                For nRowSale As Integer = 0 To wExportSale.oW_oDbtblSaleExp.Rows.Count - 1
                                    System.Threading.Thread.Sleep(1000)
                                    obgWorker.ReportProgress(0)
                                    tDocNo = wExportSale.oW_oDbtblSaleExp.Rows(nRowSale)("FTDocNo").ToString()
                                    tCstCode = wExportSale.oW_oDbtblSaleExp.Rows(nRowSale)("FTCstCode").ToString()
                                    tBchCode = wExportSale.oW_oDbtblSaleExp.Rows(nRowSale)("FTBchCode").ToString()
                                    tPosCode = wExportSale.oW_oDbtblSaleExp.Rows(nRowSale)("FTPostCode").ToString()
                                    tSaleAmt = wExportSale.oW_oDbtblSaleExp.Rows(nRowSale)("FTSumNet").ToString()


                                    ' Dim tSendDate As String = Convert.ToDateTime(tDate).ToString("yyyyMMdd", New System.Globalization.CultureInfo("en-US"))

                                    tW_DocNo = tDocNo
                                    If oExpSale.C_EXPbExportSaleCustomer("Sale", tDocNo, tCstCode, tBchCode, tPosCode, tDate, tSaleAmt) Then
                                        System.Threading.Thread.Sleep(100)
                                        obgWorker.ReportProgress(100)
                                    End If
                                Next
                                'ย้าย File จาก Process มาไว้ใน Log
                                If wExportSale.oW_oDbtblReturnExp.Rows.Count = 0 Then
                                    W_SETxWriteSuccess()
                                End If
                            End If


                            'Export ประเภทคืน

                            If wExportSale.oW_oDbtblReturnExp IsNot Nothing AndAlso wExportSale.oW_oDbtblReturnExp.Rows.Count > 0 Then
                                For nRowReturn As Integer = 0 To wExportSale.oW_oDbtblReturnExp.Rows.Count - 1

                                    System.Threading.Thread.Sleep(1000)
                                    obgWorker.ReportProgress(0)

                                    tDocNo = wExportSale.oW_oDbtblReturnExp.Rows(nRowReturn)("FTDocNo").ToString()
                                    tCstCode = wExportSale.oW_oDbtblReturnExp.Rows(nRowReturn)("FTCstCode").ToString()
                                    tBchCode = wExportSale.oW_oDbtblReturnExp.Rows(nRowReturn)("FTBchCode").ToString()
                                    tPosCode = wExportSale.oW_oDbtblReturnExp.Rows(nRowReturn)("FTPostCode").ToString()
                                    tSaleAmt = wExportSale.oW_oDbtblReturnExp.Rows(nRowReturn)("FTSumNet").ToString()
                                    tW_DocNo = tDocNo
                                    If oExpSale.C_EXPbExportReturnCustomer("Return", tDocNo, tCstCode, tBchCode, tPosCode, tDate, tSaleAmt) Then
                                        System.Threading.Thread.Sleep(100)
                                        obgWorker.ReportProgress(100)
                                    End If
                                Next

                                'ย้าย File จาก Process มาไว้ใน Log
                                W_SETxWriteSuccess()
                            End If

                    End Select
                End If

                nW_Success += 1
            Next
        End If

        'END กรณีเลือกรายการข้างใน 


    End Sub

    Private Sub obgWorker_DoWork(sender As Object, e As System.ComponentModel.DoWorkEventArgs) Handles obgWorker.DoWork
        W_EXPxExportData()
    End Sub

    Private Sub obgWorker_ProgressChanged(sender As Object, e As System.ComponentModel.ProgressChangedEventArgs) Handles obgWorker.ProgressChanged
        Dim tStatusTxt As String = ""
        If cCNVB.nVB_CutLng = 1 Then
            tStatusTxt = "ระบบกำลังส่งออกข้อมูล "
        ElseIf cCNVB.nVB_CutLng = 2 Then
            tStatusTxt = "In process. "
        End If

        For i = 1 To 100
            'obgWorker.ReportProgress(100)
            ' System.Threading.Thread.Sleep(10)
            opgProcess.Value = i
            olaProcess.Text = tStatusTxt + " |" + CType(i, String) + " %"
            olaProcess.Refresh()
        Next
    End Sub

    Private Sub obgWorker_RunWorkerCompleted(sender As Object, e As System.ComponentModel.RunWorkerCompletedEventArgs) Handles obgWorker.RunWorkerCompleted

        'Clear ค่าทั้งหมด 
        nC_CreateFileCst = 0
        nC_CreateFileGrp = 0
        nC_CreateFileSale = 0
        wExportSale.oW_oDbtblReturnExp = Nothing
        wExportSale.oW_oDbtblSaleExp = Nothing
        wExportGrpCst.oW_oDbtblTmpCgpCstExp = Nothing
        wExportCst.oW_oDbtblTmpCstExp = Nothing
        wExportSale.oW_tDate = Nothing
        bW_StatusExport = False

        If bW_DoworkRuntime = False Then
            If cCNVB.nVB_CutLng = 1 Then
                If nW_Success >= 1 Then
                    MsgBox("ส่งออกข้อมูลสำเร็จ", MsgBoxStyle.Information)
                ElseIf nW_Fail >= 1 Then
                    MsgBox("เกิดข้อผิดพลาดกรุณาตรวจสอบ Log", MsgBoxStyle.Information)
                Else
                    MsgBox("ไม่มีการอัพเดทข้อมูลประจำวัน", MsgBoxStyle.Information)
                End If
            Else
                If nW_Success >= 1 Then
                    MsgBox("Export success", MsgBoxStyle.Information)
                ElseIf nW_Fail >= 1 Then
                    MsgBox("Process not found.", MsgBoxStyle.Information)
                Else
                    MsgBox("Today no data update", MsgBoxStyle.Information)
                End If
            End If
        End If

        nW_Fail = 0
        nW_Success = 0


    End Sub

    Public Sub W_SETxWriteSuccess()
        Try
            Dim oHDfile As New StringBuilder
            Dim tPathLog As String = AdaConfig.cConfig.tAppPath & "\Log\Process"
            Dim tPath As New DirectoryInfo(AdaConfig.cConfig.tAppPath & "\Log\Process")
            Dim oGetFile As FileInfo() = tPath.GetFiles()

            For Each oFile As FileInfo In oGetFile
                tPathLog &= "\" & oFile.Name
            Next
            oHDfile.Append("[ExportDate] " + Format(Date.Now, "dd/MM/yyy") + vbTab + vbTab + "[Export by]" + Environment.MachineName)
            oHDfile.Append(Environment.NewLine())
            oHDfile.Append("[ExportTime Finish] " + Format(Date.Now, "HH: mm : ss"))
            oHDfile.Append(Environment.NewLine())

            Dim tName As String = Path.GetFileName(tPathLog)
            Dim tTemp As String = Path.GetTempFileName()
            Using oStreamWriter As StreamWriter = New StreamWriter(tTemp)
                Using oReader As StreamReader = New StreamReader(tPathLog)
                    oStreamWriter.Write(oHDfile)
                    While (Not oReader.EndOfStream)
                        oStreamWriter.WriteLine(oReader.ReadLine())
                    End While
                End Using

            End Using
            File.Copy(tTemp, tPathLog, True)

            W_SETxMoveLog(tPathLog)

        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_SETxMoveLog(ByVal ptPathName As String)
        Try
            Dim tPathName As String = Path.GetFileName(ptPathName)
            FileCopy(ptPathName, AdaConfig.cConfig.tAppPath & "\Log\" & tPathName)
            If File.Exists(ptPathName) Then
                File.Delete(ptPathName)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub


End Class