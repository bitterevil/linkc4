﻿Imports System.IO
Imports System.Text

Public Class wExportCst
    Dim tW_DocNo As String = ""
    Dim oW_oDbtblTmpCst As DataTable
    Public Shared oW_oDbtblTmpCstExp As DataTable  'สำหรับส่งข้อมูลที่เลือกไปยังหน้าแรก
    Dim bStatus As Boolean = False
    Private Sub wExportCst_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_FrmSetCapControl(Me)
        W_GETdataGridCst("")
    End Sub

    ''' <summary>
    ''' สร้าง column สำหรับ Table ลูกค้า
    ''' </summary>
    ''' <returns></returns>
    Function W_SEToColumnCustomer() As DataTable
        Try
            Dim oDbTbl As New DataTable()
            oDbTbl.Columns.Add("FTSelect", GetType(Boolean))
            oDbTbl.Columns.Add("FTCstCode", GetType(String))
            oDbTbl.Columns.Add("FTFirstname", GetType(String))
            oDbTbl.Columns.Add("FTPoint", GetType(String))
            oDbTbl.Columns.Add("FTDateUpt", GetType(String))
            oDbTbl.Columns.Add("FTTimeUpt", GetType(String))

            Return oDbTbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    ''' <summary>
    ''' Load ข้อมูลลูกค้าลง grid
    ''' </summary>
    Public Function W_GETdataGridCst(ByVal ptCstCode As String) As Boolean
        Try
            Dim oSql As New StringBuilder
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtbl As New DataTable
            Dim tExportFirst = AdaConfig.cConfig.oConfigAPI.tExportCst

            oSql.AppendLine(" SELECT TOP(10) FTCstCode, FTCstName,FTCstTel, ")
            oSql.AppendLine(" FTCstEmail,FTCstFax,FTCstResTel, CASE FTAreCode WHEN '' THEN 'TH' ELSE FTAreCode END AS FTAreCode ,")
            oSql.AppendLine(" FTCstAddr,FTCstPostCode,FTCstCode,")
            oSql.AppendLine(" FTCstCardID,FTCstCardID,FDCstCrdExpire, ")
            oSql.AppendLine(" FTBchCode,FTCstSex,FDCstDob, ")
            oSql.AppendLine(" FCCstRetPoint,FTCpgCode,FTTimeUpd,FDDateUpd ")
            oSql.AppendLine(" FROM TCNMCst")

            If tExportFirst <> 0 Then
                oSql.AppendLine(" WHERE (FDDateUpd = CONVERT(VARCHAR(10), GETDATE(), 121)) ")
            Else
                If tExportFirst = 0 And ptCstCode <> "" Then
                    oSql.AppendLine(" WHERE FTCstCode = '" + ptCstCode + "' ")
                ElseIf ptCstCode <> "" Then
                    oSql.AppendLine(" AND FTCstCode = '" + ptCstCode + "' ")
                End If
            End If



            oDbtbl = oDatabase.C_CALoExecuteReader(oSql.ToString())

            If oDbtbl.Rows.Count > 0 Then
                oW_oDbtblTmpCst = W_SEToColumnCustomer()
                For nRow As Integer = 0 To oDbtbl.Rows.Count - 1
                    Dim oRow As DataRow = oW_oDbtblTmpCst.NewRow()
                    oRow("FTSelect") = True
                    oRow("FTFirstname") = oDbtbl.Rows(nRow).Item("FTCstName")
                    oRow("FTCstCode") = oDbtbl.Rows(nRow).Item("FTCstCode")

                    oRow("FTPoint") = oDbtbl.Rows(nRow).Item("FCCstRetPoint")
                    oRow("FTDateUpt") = Convert.ToDateTime(oDbtbl.Rows(nRow).Item("FDDateUpd")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    oRow("FTTimeUpt") = oDbtbl.Rows(nRow).Item("FTTimeUpd")
                    oW_oDbtblTmpCst.Rows.Add(oRow)
                Next
                ogdCst.DataSource = oW_oDbtblTmpCst

                With ogdCst
                    .Columns("FTSelect").HeaderText = ""
                    .Columns("FTSelect").Width = 50
                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTCstCode").HeaderText = "รหัสลูกค้า"
                        .Columns("FTFirstname").HeaderText = "ชื่อ"
                        .Columns("FTCstCode").HeaderText = "รหัสลูกค้า"
                        .Columns("FTPoint").HeaderText = "แต้มคงเหลือ"
                        .Columns("FTDateUpt").HeaderText = "วันที่ Update ล่าสุด "
                        .Columns("FTTimeUpt").HeaderText = "เวลาที่ Update ล่าสุด"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTCstCode").HeaderText = "Customer Code"
                        .Columns("FTFirstname").HeaderText = "Firstname"
                        .Columns("FTCstCode").HeaderText = "CustomerCode"
                        .Columns("FTPoint").HeaderText = "Point"
                        .Columns("FTDateUpt").HeaderText = "DateUpdate"
                        .Columns("FTTimeUpt").HeaderText = "TimeUpdate"
                    End If

                    ' .Columns("FTSelect").HeaderText = DataGridViewCheckBoxColumn.
                    .Columns("FTFirstname").ReadOnly = True
                    .Columns("FTCstCode").ReadOnly = True
                    .Columns("FTDateUpt").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .Columns("FTTimeUpt").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    .AllowUserToAddRows = False
                    .RowHeadersVisible = False

                End With

                W_SETxCheckboxCstAllow()
                Return True
            Else
                Return False
            End If



        Catch ex As Exception

        End Try
    End Function


    Private oHeaderBox As CheckBox
    Private Sub W_SETxCheckboxCstAllow()
        Dim oCheckboxHeader As CheckBox = New CheckBox()
        Dim oRect As Rectangle = ogdCst.GetCellDisplayRectangle(4, -1, True)
        oRect.X = 20
        oRect.Y = 7
        With oCheckboxHeader
            .BackColor = Color.Transparent
        End With
        oCheckboxHeader.Name = "checkboxHeader"
        oCheckboxHeader.Size = New Size(14, 14)
        oCheckboxHeader.Location = oRect.Location
        oCheckboxHeader.Checked = True
        AddHandler oCheckboxHeader.CheckedChanged, AddressOf checkboxHeader_CheckedChanged
        ogdCst.Controls.Add(oCheckboxHeader)
    End Sub

    Private Sub checkboxHeader_CheckedChanged(ByVal sender As Object, ByVal e As EventArgs)
        oHeaderBox = DirectCast(ogdCst.Controls.Find("checkboxHeader", True)(0), CheckBox)
        For Each oRow As DataGridViewRow In ogdCst.Rows
            oRow.Cells(0).Value = oHeaderBox.Checked
        Next
        ogdCst.RefreshEdit()
    End Sub


    Private Sub ocmClose_Click(sender As Object, e As EventArgs) Handles ocmClose.Click
        Me.Close()
    End Sub

    Public Sub W_EXPxExportDataCustomer()
        Try

            Dim tStatusReturn As String = ""
            Dim tDate As String = Convert.ToDateTime(Now).ToString(Date.Now, New System.Globalization.CultureInfo("en-US"))
            Dim oExpSale As New cExportSale
            Dim tSelect As String = ""
            Dim tType As String = ""

            'Export ข้อมูลลูกค้า
            tStatusReturn = ""
            'Export first = 0 : ครั้งแรกต้อง Export แบบ Manual ทั้งหมดก่อน / ครั้งต่อไป Export เฉพาะข้อมูลที่มีการ Update
            Dim tCstCode As String = ""
            'If tExportFirst = 0 Then
            If oW_oDbtblTmpCst.Rows.Count Then
                For nRowCst As Integer = 0 To oW_oDbtblTmpCst.Rows.Count - 1
                    'ถ้ามีการเลือกให้เข้าเงื่อนไข 
                    If ogdCst.Rows(nRowCst).Cells(0).Value = True Then
                        oExpSale.W_SETxSaveStatusExpFirst("ExportCstFirst")
                        tCstCode = ogdCst.Rows(nRowCst).Cells(1).Value
                        If oExpSale.C_EXPbExportCustomer(tCstCode) Then
                        End If
                    End If
                Next
            End If


        Catch ex As Exception

        End Try
    End Sub


    Private Sub ocmSearch_Click(sender As Object, e As EventArgs) Handles ocmSearch.Click
        W_GETdataGridCst(otbCst.Text)
    End Sub

    Private Sub ocmSelect_Click(sender As Object, e As EventArgs) Handles ocmSelect.Click
        Dim bChoose As Boolean = False
        For nrow As Integer = 0 To ogdCst.Rows.Count - 1
            If ogdCst.Rows(nrow).Cells(0).Value = True Then
                bChoose = True
            End If
        Next

        If bChoose = True Then
            wExportData.bW_StatusExport = True 'สถานะข้อมูลถูกเลิอก
            oW_oDbtblTmpCstExp = W_SEToColumnCustomer()
            oW_oDbtblTmpCstExp.NewRow()
            For nRow As Integer = 0 To oW_oDbtblTmpCst.Rows.Count - 1

                If ogdCst.Rows(nRow).Cells(0).Value = True Then
                    Dim oRow As DataRow = oW_oDbtblTmpCstExp.NewRow()
                    oRow("FTSelect") = True
                    oRow("FTFirstname") = oW_oDbtblTmpCst.Rows(nRow).Item("FTFirstname")
                    oRow("FTCstCode") = oW_oDbtblTmpCst.Rows(nRow).Item("FTCstCode")
                    oRow("FTPoint") = oW_oDbtblTmpCst.Rows(nRow).Item("FTPoint")
                    oRow("FTDateUpt") = Convert.ToDateTime(oW_oDbtblTmpCst.Rows(nRow).Item("FTDateUpt")).ToString("dd/MM/yyyy", New System.Globalization.CultureInfo("en-US"))
                    oRow("FTTimeUpt") = oW_oDbtblTmpCst.Rows(nRow).Item("FTTimeUpt")
                    oW_oDbtblTmpCstExp.Rows.Add(oRow)

                End If
            Next

            If cCNVB.nVB_CutLng = 1 Then
                MsgBox("ระบบทำรายการเลือกข้อมูลสำเร็จ", MsgBoxStyle.Information)
            Else
                MsgBox("Select information success", MsgBoxStyle.Information)
            End If

            Me.Close()
        Else
            If cCNVB.nVB_CutLng = 1 Then
                MsgBox("กรุณาเลือกรายการที่ต้องการส่งออก", MsgBoxStyle.Information)
            Else
                MsgBox("Please select information for export", MsgBoxStyle.Information)
            End If
        End If


    End Sub

    Private Sub otbCst_KeyUp(sender As Object, e As KeyEventArgs) Handles otbCst.KeyUp
        If (e.KeyCode = Keys.Enter) Then
            If otbCst.Text <> "" Then
                W_GETdataGridCst(otbCst.Text)
            Else
                W_GETdataGridCst("")
            End If
        End If
    End Sub
End Class