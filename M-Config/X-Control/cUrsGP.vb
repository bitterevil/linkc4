﻿Imports System.Text

Public Class cUrsGP

    Dim bC_StatusRefresh As Boolean = False
    Dim oXmlCtgAPI As XElement
    Dim oC_DbTbl As DataTable
    Dim oC_DbTblMapping As DataTable
    Dim oC_oDbTblCmb As New DataTable
    Dim oC_oDbTblMapping As New DataTable
    Dim dt As New DataTable()
    Dim oFunc As New AdaConfig.cCNSP
    Dim oCmb As New DataGridViewComboBoxColumn()
    Dim oCmbMap As New DataGridViewComboBoxColumn()
    Dim oTxt As New DataGridViewTextBoxColumn()
    Dim obtn As New DataGridViewButtonColumn()

    'Dim gol As Integer = 0
    'Dim comboBox As ComboBox

    Public Function C_GENoColumnPmt() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTPmtCode", GetType(String))
            oDbtbl.Columns.Add("FTPmtName", GetType(String))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub W_GETDataGridPromotion(ByVal ptPmhCode As String)
        Try

            Dim oSQL As New StringBuilder
            Dim ockExp As New DataGridViewCheckBoxColumn()
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtblPmt As New DataTable
            Dim tCondition As String = ""


            oSQL.Clear()
            oSQL.AppendLine(" SELECT TOP(50) FTPmhCode,FTPmhName ")
            oSQL.AppendLine(" FROM TCNTPmtHD HD  ")
            oSQL.AppendLine(" WHERE NOT EXISTS ")
            oSQL.AppendLine(" (SELECT FTPmhCode  ")
            oSQL.AppendLine(" FROM TLNKMappingGP GP ")
            oSQL.AppendLine(" WHERE HD.FTPmhCode = GP.FTPmhCode) ")
            If ptPmhCode <> "" Then
                oSQL.AppendLine(" AND FTPmhCode = '" + ptPmhCode + "'  ")

            End If

            oDbtblPmt = oDatabase.C_CALoExecuteReader(oSQL.ToString())

            If oDbtblPmt.Rows.Count > 0 Then
                oC_DbTbl = C_GENoColumnPmt()
                For nRow As Integer = 0 To oDbtblPmt.Rows.Count - 1
                    Dim oDatarow As DataRow = oC_DbTbl.NewRow()
                    oDatarow("FTPmtCode") = oDbtblPmt(nRow)("FTPmhCode").ToString
                    oDatarow("FTPmtName") = oDbtblPmt(nRow)("FTPmhName").ToString
                    oC_DbTbl.Rows.Add(oDatarow)
                Next

                ogdPmt.DataSource = oC_DbTbl
                With ogdPmt
                    If cCNVB.nVB_CutLng = 1 Then
                        .Columns("FTPmtCode").HeaderText = "รหัสโปรโมชั่น"
                        .Columns("FTPmtName").HeaderText = "ชื่อโปรโมชั่น"
                    Else cCNVB.nVB_CutLng = 2
                        .Columns("FTPmtCode").HeaderText = "Promotion code"
                        .Columns("FTPmtName").HeaderText = "Promotion name"
                    End If

                    .Columns("FTPmtCode").ReadOnly = True
                    .Columns("FTPmtName").ReadOnly = True
                    ogdPmt.RowHeadersVisible = False

                    .Columns("FTPmtCode").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                    .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                    .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                    If bC_StatusRefresh = False Then


                        oCmb.HeaderText = "Share Code"
                        oCmb.Name = "Share Code"
                        oCmb.Width = 200
                        If ogdPmt.Columns("FTShareCode") Is Nothing Then
                            ogdPmt.Columns.Add(oCmb)
                        End If

                        oCmb.DataSource = C_DATxLoadShareDiscount()
                        oCmb.DisplayMember = "Detail"
                        oCmb.ValueMember = "ID"


                        If ogdPmt.Columns("FTGp") Is Nothing Then
                            ogdPmt.Columns.Add(oTxt)
                        End If
                        oTxt.HeaderText = "GP(%)"
                        oTxt.Name = "GP"
                        oTxt.Width = 50


                        If ogdPmt.Columns("FTBtn") Is Nothing Then
                            ogdPmt.Columns.Add(obtn)
                        End If
                        obtn.HeaderText = "Save"
                        obtn.Name = "Save"
                        obtn.Width = 40

                    End If

                    ogdPmt.AllowUserToAddRows = False
                End With

            Else
                ogdPmt.Columns.Remove("FTPmtCode")
                ogdPmt.Columns.Remove("FTPmtName")
                ogdPmt.Columns.Remove(oCmb)
                ogdPmt.Columns.Remove(oTxt)
                ogdPmt.Columns.Remove(obtn)
                ogdPmt.DataSource = Nothing

            End If


        Catch ex As Exception

        End Try
    End Sub

    Public Sub W_GETDataGridMapping(ByVal ptPmhCode As String)
        Try

            Dim oSQL As New StringBuilder
            Dim ockExp As New DataGridViewCheckBoxColumn()
            Dim oDatabase As New cDatabaseLocal
            Dim oDbtblPmt As New DataTable
            Dim tCondition As String = ""


            oSQL.Clear()
            oSQL.AppendLine(" SELECT  FTPmhCode, FTPmhName, FTLnkShrCode, FTLnkShrName, FCLnkGP ")
            oSQL.AppendLine(" FROM    TLNKMappingGP  ")
            If ptPmhCode <> "" Then
                oSQL.AppendLine(" WHERE FTPmhCode = '" + ptPmhCode + "' ")
            End If

            oDbtblPmt = oDatabase.C_CALoExecuteReader(oSQL.ToString())

            If oDbtblPmt.Rows.Count > 0 Then
                oC_DbTblMapping = C_GENoColumnMapping()
                For nRow As Integer = 0 To oDbtblPmt.Rows.Count - 1
                    Dim oDatarow As DataRow = oC_DbTblMapping.NewRow()
                    oDatarow("FTPmtCode") = oDbtblPmt(nRow)("FTPmhCode").ToString
                    oDatarow("FTPmtName") = oDbtblPmt(nRow)("FTPmhName").ToString
                    oDatarow("FTShareCode") = oDbtblPmt(nRow)("FTLnkShrName").ToString
                    oDatarow("FTGp") = oDbtblPmt(nRow)("FCLnkGP").ToString
                    oC_DbTblMapping.Rows.Add(oDatarow)
                Next

            End If

            ogdMapping.DataSource = oC_DbTblMapping

            With ogdMapping
                If cCNVB.nVB_CutLng = 1 Then
                    .Columns("FTPmtCode").HeaderText = "รหัสโปรโมชั่น"
                    .Columns("FTPmtName").HeaderText = "ชื่อโปรโมชั่น"
                    .Columns("FTShareCode").HeaderText = "Share Code"
                    .Columns("FTGp").HeaderText = "GP(%)"
                Else cCNVB.nVB_CutLng = 2
                    .Columns("FTPmtCode").HeaderText = "Promotion code"
                    .Columns("FTPmtName").HeaderText = "Promotion name"
                    .Columns("FTShareCode").HeaderText = "Share Code"
                    .Columns("FTGp").HeaderText = "GP(%)"
                End If

                .Columns("FTPmtCode").ReadOnly = True
                .Columns("FTPmtName").ReadOnly = True
                .Columns("FTGp").Width = 50
                .Columns("FTShareCode").Width = 200
                ogdMapping.RowHeadersVisible = False

                .Columns("FTPmtCode").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
                .ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
                .RowsDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter

                If bC_StatusRefresh = False Then
                    bC_StatusRefresh = True
                    Dim obtR As New DataGridViewButtonColumn()
                    If ogdMapping.Columns("FTBtn") Is Nothing Then
                        ogdMapping.Columns.Add(obtR)
                    End If
                    obtR.HeaderText = "Edit"
                    obtR.Name = "Edit"
                    obtR.Width = 40
                End If


                'End If

                ogdMapping.AllowUserToAddRows = False
            End With
        Catch ex As Exception

        End Try
    End Sub

    Public Function C_GENoColumnMapping() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTPmtCode", GetType(String))
            oDbtbl.Columns.Add("FTPmtName", GetType(String))
            oDbtbl.Columns.Add("FTShareCode", GetType(String))
            oDbtbl.Columns.Add("FTGp", GetType(String))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function C_DATxLoadShareDiscount() As DataTable

        Try
            oC_oDbTblCmb.Columns.Add("Detail", GetType(String))
            oC_oDbTblCmb.Columns.Add("ID", GetType(String))

            oXmlCtgAPI = XElement.Load(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")

            Dim oCompenSate = From c In oXmlCtgAPI.Elements("CompenSate") Select c
            For Each oItem In oCompenSate
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next


            Dim oShareDis = From c In oXmlCtgAPI.Elements("ShareDis") Select c
            For Each oItem In oShareDis
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnnet = From c In oXmlCtgAPI.Elements("MarginOnnet") Select c
            For Each oItem In oMarginOnnet
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnSale = From c In oXmlCtgAPI.Elements("MarginOnSale") Select c
            For Each oItem In oMarginOnSale
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount30 = From c In oXmlCtgAPI.Elements("ShareDiscount30") Select c
            For Each oItem In oShareDiscount30
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount40 = From c In oXmlCtgAPI.Elements("ShareDiscount40") Select c
            For Each oItem In oShareDiscount40
                oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Dim oShareDiscount40_1 = From c In oXmlCtgAPI.Elements("ShareDiscount40_1") Select c
            For Each oItem In oShareDiscount40_1
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_2 = From c In oXmlCtgAPI.Elements("ShareDiscount40_2") Select c
            For Each oItem In oShareDiscount40_2
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_3 = From c In oXmlCtgAPI.Elements("ShareDiscount40_3") Select c
            For Each oItem In oShareDiscount40_3
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_4 = From c In oXmlCtgAPI.Elements("ShareDiscount40_4") Select c
            For Each oItem In oShareDiscount40_4
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_5 = From c In oXmlCtgAPI.Elements("ShareDiscount40_5") Select c
            For Each oItem In oShareDiscount40_5
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblCmb.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Return oC_oDbTblCmb
        Catch ex As Exception

        End Try

    End Function

    Public Function C_DATxLoadShareDiscountMapping() As DataTable

        Try
            oC_oDbTblMapping.Columns.Add("Detail", GetType(String))
            oC_oDbTblMapping.Columns.Add("ID", GetType(String))

            oXmlCtgAPI = XElement.Load(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")

            Dim oCompenSate = From c In oXmlCtgAPI.Elements("CompenSate") Select c
            For Each oItem In oCompenSate
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next


            Dim oShareDis = From c In oXmlCtgAPI.Elements("ShareDis") Select c
            For Each oItem In oShareDis
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnnet = From c In oXmlCtgAPI.Elements("MarginOnnet") Select c
            For Each oItem In oMarginOnnet
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oMarginOnSale = From c In oXmlCtgAPI.Elements("MarginOnSale") Select c
            For Each oItem In oMarginOnSale
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount30 = From c In oXmlCtgAPI.Elements("ShareDiscount30") Select c
            For Each oItem In oShareDiscount30
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            Dim oShareDiscount40 = From c In oXmlCtgAPI.Elements("ShareDiscount40") Select c
            For Each oItem In oShareDiscount40
                oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Dim oShareDiscount40_1 = From c In oXmlCtgAPI.Elements("ShareDiscount40_1") Select c
            For Each oItem In oShareDiscount40_1
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_2 = From c In oXmlCtgAPI.Elements("ShareDiscount40_2") Select c
            For Each oItem In oShareDiscount40_2
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_3 = From c In oXmlCtgAPI.Elements("ShareDiscount40_3") Select c
            For Each oItem In oShareDiscount40_3
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_4 = From c In oXmlCtgAPI.Elements("ShareDiscount40_4") Select c
            For Each oItem In oShareDiscount40_4
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            Dim oShareDiscount40_5 = From c In oXmlCtgAPI.Elements("ShareDiscount40_5") Select c
            For Each oItem In oShareDiscount40_5
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    oC_oDbTblMapping.Rows.Add(oItem.Attribute("Detail").Value, oItem.Attribute("ID").Value)
                End If
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Return oC_oDbTblMapping
        Catch ex As Exception

        End Try

    End Function

    Private Sub cUrsGP_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'SetCaption TH ,EN
        cCNSP.SP_UsrSetCapControl(Me)
        C_GENxTableMapping()
        W_GETDataGridPromotion("")
        W_GETDataGridMapping("")
        C_DATxLoadShareDiscount()

    End Sub

    Private Sub ogdPmt_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdPmt.CellContentClick
        If ogdPmt.Columns(e.ColumnIndex).Name = "Save" AndAlso e.RowIndex >= 0 Then
            Dim tPmtCode As String = ogdPmt.Rows(e.RowIndex).Cells(3).Value
            Dim tPmtName As String = ogdPmt.Rows(e.RowIndex).Cells(4).Value
            '   Dim tPtLnkGP As Decimal = ogdPmt.Rows(e.RowIndex).Cells(1).Value
            Dim tPtLnkGP As String = ogdPmt.Rows(e.RowIndex).Cells(1).Value

            If (olaID.Text = "" Or tPtLnkGP = "") Then
                MsgBox("กรุณากรอกข้อมูลให้ครบ", MsgBoxStyle.Critical)
            Else
                If C_DATbSaveDataMapping(tPmtCode, tPmtName, tPtLnkGP) Then
                    W_GETDataGridPromotion("")
                    W_GETDataGridMapping("")

                Else
                    MsgBox("ข้อมูลซ้ำ", MsgBoxStyle.Critical)
                End If

            End If
        End If
    End Sub

    Public Function C_DATbSaveDataMapping(ByVal ptPmhCode As String, ByVal ptPmhName As String,
                                          ByVal ptLnkGP As Decimal) As Boolean
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Dim cGP As Decimal = String.Format("{0:F2}", ptLnkGP)
        oSql = New StringBuilder()
        Try
            oSql.AppendLine("  INSERT  ")
            oSql.AppendLine("  INTO TLNKMappingGP ")
            oSql.AppendLine("  (FTPmhCode, FTPmhName, FTLnkShrCode, ")
            oSql.AppendLine("  FTLnkShrName, FCLnkGP, FDDateUpd,FTTimeUpd, ")
            oSql.AppendLine("  FTWhoUpd, FDDateIns, FTTimeIns, FTWhoIns) ")
            oSql.AppendLine("  VALUES ('" + ptPmhCode + "',")
            oSql.AppendLine("   '" + ptPmhName + "' ,")
            oSql.AppendLine("   '" + olaID.Text + "' ,")
            oSql.AppendLine("   '" + olaSharCode.Text + "' ,")
            oSql.AppendLine("   '" + CType(cGP, String) + "' ,")
            oSql.AppendLine("   CONVERT(VARCHAR(10), GETDATE(), 121),")
            oSql.AppendLine("   CONVERT(VARCHAR(8),GETDATE(),108) ,")
            oSql.AppendLine("   'AdaLink',")
            oSql.AppendLine("   CONVERT(VARCHAR(10), GETDATE(), 121),")
            oSql.AppendLine("   CONVERT(VARCHAR(8),GETDATE(),108) ,")
            oSql.AppendLine("   'AdaLink' ")
            oSql.AppendLine("  )")
            Dim nSucc As Integer = oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
            If nSucc = 0 Then
                Return False
            End If
            Return True
        Catch ex As Exception

        End Try
    End Function

    Private Sub ogdPmt_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles ogdPmt.CellPainting
        If ogdPmt.Columns(e.ColumnIndex).Name = "Save" AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)

            Dim img As Image = Image.FromFile(AdaConfig.cConfig.tAppPath & "\img\if_floppy_285657.ico")
            e.Graphics.DrawImage(img, CInt((e.CellBounds.Width / 2) - (img.Width / 2)) + e.CellBounds.X, CInt((e.CellBounds.Height / 2) - (img.Height / 2)) + e.CellBounds.Y)
            e.Handled = True
        End If
    End Sub

    Public Sub C_GENxTableMapping()
        Dim oSql As StringBuilder
        Dim oDatabase As New cDatabaseLocal()
        Try
            oSql = New StringBuilder()

            oSql.AppendLine("IF NOT EXISTS (SELECT * FROM sys.objects")
            oSql.AppendLine("WHERE object_id = OBJECT_ID(N'TLNKMappingGP') AND type in (N'U'))")
            oSql.AppendLine("BEGIN")
            oSql.AppendLine(" CREATE TABLE [dbo].[TLNKMappingGP](")
            oSql.AppendLine(" [FTPmhCode] [varchar](20) NOT NULL,")
            oSql.AppendLine(" [FTPmhName] [varchar](200) NULL, ")
            oSql.AppendLine(" [FTLnkShrCode] [varchar](20) NULL,")
            oSql.AppendLine(" [FTLnkShrName] [varchar](200) NULL, ")
            oSql.AppendLine(" [FCLnkGP] [float] NULL, ")
            oSql.AppendLine(" [FDDateUpd] [datetime] NULL, ")
            oSql.AppendLine(" [FTTimeUpd] [varchar](8) NULL,")
            oSql.AppendLine(" [FTWhoUpd] [varchar](50) NULL, ")
            oSql.AppendLine(" [FDDateIns] [datetime] NULL,")
            oSql.AppendLine(" [FTTimeIns] [varchar](8) NULL, ")
            oSql.AppendLine(" [FTWhoIns] [varchar](50) NULL, ")
            oSql.AppendLine(" CONSTRAINT [PK_TLNKMappingGP] PRIMARY KEY CLUSTERED ")
            oSql.AppendLine(" ([FTPmhCode] ASC")
            oSql.AppendLine(" )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY] ")
            oSql.AppendLine(" ) ON [PRIMARY] ")
            oSql.AppendLine("END")
            oDatabase.C_CALnExecuteNonQuery(oSql.ToString())
        Catch ex As Exception

        End Try
    End Sub

    Private Sub ogdPmt_EditingControlShowing(sender As Object, e As DataGridViewEditingControlShowingEventArgs) Handles ogdPmt.EditingControlShowing
        If ogdPmt.CurrentCell.ColumnIndex = 1 AndAlso TypeOf e.Control Is TextBox Then
            RemoveHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf CellKeyPress
            AddHandler DirectCast(e.Control, TextBox).KeyPress, AddressOf CellKeyPress
        End If

        Dim oCombo As ComboBox = TryCast(e.Control, ComboBox)

        If oCombo IsNot Nothing Then
            RemoveHandler oCombo.SelectionChangeCommitted, AddressOf ComboBox_SelectionChangeCommitted
            AddHandler oCombo.SelectionChangeCommitted, AddressOf ComboBox_SelectionChangeCommitted
        End If
    End Sub

    Private Sub ComboBox_SelectionChangeCommitted(sender As Object, e As EventArgs)
        Dim cb As ComboBox = DirectCast(sender, ComboBox)
        olaSharCode.Text = cb.Text
        olaID.Text = cb.SelectedValue
    End Sub

    Private Sub CellKeyPress(ByVal sender As Object, ByVal e As KeyPressEventArgs)

        If oFunc.SP_SHWbInputNumberOnly(e.KeyChar) = True Then
            bC_StatusRefresh = True
            MsgBox("กรอกได้เฉพาะตัวเลข และ จุดทศนิยมเท่านั้น ", MsgBoxStyle.Critical)
            W_GETDataGridPromotion("")
        End If
    End Sub

    Private Sub ocmSearch_Click(sender As Object, e As EventArgs) Handles ocmSearch.Click
        olaSharCode.Text = ""
        olaID.Text = ""
        W_GETDataGridPromotion(otbCode.Text)
    End Sub

    Private Sub ogdMapping_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdMapping.CellContentClick
        If ogdMapping.Columns(e.ColumnIndex).Name = "Edit" AndAlso e.RowIndex >= 0 Then
            Dim oFrm As New wEditSharing()
            If e.ColumnIndex = 0 Then
                'Dim tValue As String = ogdShare.Rows(e.RowIndex).Cells(1).Value
                'Dim tDetail As String = ogdShare.Rows(e.RowIndex).Cells(2).Value
                cLog.C_CALxWriteLog("wInsertSharing")
                oFrm.olaKey.Text = ogdMapping.Rows(e.RowIndex).Cells(1).Value
                oFrm.otbGp.Text = ogdMapping.Rows(e.RowIndex).Cells(4).Value
                If oFrm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                    W_GETDataGridMapping("")
                    oFrm.Dispose()
                End If

            End If

        End If

    End Sub

    Private Sub ogdMapping_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles ogdMapping.CellPainting
        If ogdMapping.Columns(e.ColumnIndex).Name = "Edit" AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)

            Dim img As Image = Image.FromFile(AdaConfig.cConfig.tAppPath & "\img\if_pen_1814074.ico")
            e.Graphics.DrawImage(img, CInt((e.CellBounds.Width / 2) - (img.Width / 2)) + e.CellBounds.X, CInt((e.CellBounds.Height / 2) - (img.Height / 2)) + e.CellBounds.Y)
            e.Handled = True
        End If
    End Sub

    Private Sub ocmMBtnSearch_Click(sender As Object, e As EventArgs) Handles ocmMBtnSearch.Click
        W_GETDataGridMapping(otbPmtMap.Text)
    End Sub

    Private Sub otbCode_KeyUp(sender As Object, e As KeyEventArgs) Handles otbCode.KeyUp
        If (e.KeyCode = Keys.Enter) Then
            If otbCode.Text <> "" Then
                W_GETDataGridPromotion(otbCode.Text)
            Else
                W_GETDataGridPromotion("")
            End If
        End If
    End Sub

    Private Sub otbPmtMap_KeyUp(sender As Object, e As KeyEventArgs) Handles otbPmtMap.KeyUp
        If (e.KeyCode = Keys.Enter) Then
            If otbPmtMap.Text <> "" Then
                W_GETDataGridMapping(otbPmtMap.Text)
            Else
                W_GETDataGridMapping("")
            End If
        End If
    End Sub


    'Public Sub bindcombo()
    '    Dim oCmbMap As New DataGridViewComboBoxColumn()
    '    oCmbMap.HeaderText = "test"
    '    Dim oArr As New ArrayList()
    '    For Each row As DataRow In dt.Rows
    '        oArr.Add(dt(0)("FTLnkShrName").ToString)
    '    Next

    '    oCmbMap.Items.AddRange(oArr.ToArray())
    '    ogdMapping.Columns.Add(oCmbMap)

    'End Sub

    'Public Sub bindcombo()
    '    Dim oCmbMap As New DataGridViewComboBoxCell()
    '    Dim oArr As New ArrayList()
    '    For Each row As DataRow In dt.Rows
    '        oArr.Add(dt(0)("FTLnkShrName").ToString)
    '    Next

    '    oCmbMap.Items.AddRange(oArr.ToArray())
    'End Sub


    'Public Function loaddata() As DataTable
    '    Dim oSQL As New StringBuilder()
    '    Dim oDatabase As New cDatabaseLocal()

    '    oSQL.Clear()
    '    oSQL.AppendLine(" SELECT  FTPmhCode, FTPmhName, FTLnkShrCode, FTLnkShrName, FCLnkGP ")
    '    oSQL.AppendLine(" FROM    TLNKMappingGP  ")

    '    dt = oDatabase.C_CALoExecuteReader(oSQL.ToString())
    'End Function
End Class
