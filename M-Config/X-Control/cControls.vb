﻿Imports System.IO

Public Class cControls
    Private oW_ListCtl As New Hashtable
    Private oW_DesCtl As New Hashtable
    Private tW_CurDesClt As String = ""
    Public Shared Property tAppPath As String = Application.StartupPath
    Public Enum eType
        [Connection] = 1
        [Configuration] = 2
        [Other] = 3
        [ShareDiscount] = 4 'Add sFTP Config '*CH 22-06-2017
    End Enum

    Sub New()
        'Load Caption
        oW_DesCtl.Add(eType.Connection, Split("เลือกการเชื่อมต่อที่คุณต้องการปรับปรุง;Select the connection for which you want to update.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Configuration, Split("เลือกการเชื่อมต่อข้อมูลไปยัง Web API ;Select the connection for Web Api.", ";")(cCNVB.nVB_CutLng - 1))
        oW_DesCtl.Add(eType.Other, Split("การจัดการข้อมูล;Mapping", ";")(cCNVB.nVB_CutLng - 1))
        '   oW_DesCtl.Add(eType.sFTP, Split("sFTP;sFTP", ";")(cCNVB.nVB_CutLng - 1)) 'Add sFTP Config '*CH 22-06-2017

        'Load Instance Menu
        oW_ListCtl.Add(eType.Connection, New cUsrConnection)
        oW_ListCtl.Add(eType.Configuration, New cUsrConfiguration)
        'oW_ListCtl.Add(eType.Other, New cUsrMappingBranch)
        ' oW_ListCtl.Add(eType.Configuration, New cUsrConfiguration)
        ' oW_ListCtl.Add(eType.Other, New cUsrMapping)
        ' oW_ListCtl.Add(eType.sFTP, New cUsrFTP)

        'สร้างไฟล์ ShareDiscount
        '  W_DATxSaveConfigShareDiscount()
        'Load Dts Config ถ้ากรณีที่มีไฟล์ Config 
        If File.Exists(Application.StartupPath & "\Config\AdaConfig.xml") = True Then
            Me.W_SETxDataCtl()
        End If

    End Sub

    Private Sub W_SetxCurDes(ByVal ptDes As String)
        Me.tW_CurDesClt = ptDes
    End Sub

    Public Function W_GETtCurDes() As String
        Return Me.tW_CurDesClt
    End Function

    Public Sub W_GETxCurCtl(ByVal poControl As Control, ByVal pnIndex As eType)
        If Not Me.oW_ListCtl(pnIndex) Is Nothing Then
            poControl.Controls.Clear()
            poControl.Controls.Add(Me.oW_ListCtl(pnIndex))
            Me.W_SetxCurDes(Me.oW_DesCtl(pnIndex))
        End If
    End Sub


    'รับค่าจาก File Config มาโยนใส่ตัวแปร แล้ว เก็บค่าเอาไว้ 
    Public Sub W_SETxDataCtl()
        Dim oObjConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjConn
            .W_DATtServer = AdaConfig.cConfig.oConnSource.tServer
            .W_DATtUser = AdaConfig.cConfig.oConnSource.tUser
            .W_DATtPasswd = AdaConfig.cConfig.oConnSource.tPassword
            .W_DATtDbName = AdaConfig.cConfig.oConnSource.tCatalog
            .W_DATnTimeOut = AdaConfig.cConfig.oConnSource.nTimeOut
        End With

        'Member Database *CH 18-10-2014
        Dim oObjMemConn = oW_ListCtl(eType.Connection)
        'Set Connection
        With oObjMemConn
            .W_DATtMemServer = AdaConfig.cConfig.oMemConnSource.tServer
            .W_DATtMemUser = AdaConfig.cConfig.oMemConnSource.tUser
            .W_DATtMemPasswd = AdaConfig.cConfig.oMemConnSource.tPassword
            .W_DATtMemDbName = AdaConfig.cConfig.oMemConnSource.tCatalog
            .W_DATnMemTimeOut = AdaConfig.cConfig.oMemConnSource.nTimeOut
        End With

        'Set ConfigAPI
        Dim oObjAPI = oW_ListCtl(eType.Configuration)

        With oObjAPI
            .W_DATtKey = AdaConfig.cConfig.oConfigAPI.tKey
            .W_DATtMethod = AdaConfig.cConfig.oConfigAPI.tMethod
            .W_DATtContentType = AdaConfig.cConfig.oConfigAPI.tContenttype
            .W_DATtMember = AdaConfig.cConfig.oConfigAPI.tUrlMember
            .W_DATtMemberGrp = AdaConfig.cConfig.oConfigAPI.tUrlGrpMember
            .W_DATtSaleTran = AdaConfig.cConfig.oConfigAPI.tUrlSaleTran
            .W_DATtStatusExpCst = AdaConfig.cConfig.oConfigAPI.tExportCst
            .W_DATtStatusExpGrp = AdaConfig.cConfig.oConfigAPI.tExportGrp
            .W_DATtStatusExpSale = AdaConfig.cConfig.oConfigAPI.tExportSale
            .W_DATtCusKey = AdaConfig.cConfig.oConfigAPI.tCusKey
            .W_DATtCusSecret = AdaConfig.cConfig.oConfigAPI.tCusSecret
            .W_DATtTokenSecret = AdaConfig.cConfig.oConfigAPI.tTokenSecret
        End With
    End Sub

    Public Function W_DATbChkDb() As Boolean
        W_DATbChkDb = False
        Dim oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        Dim tConn As String = String.Format("Data Source={0};User ID={1};Password={2};Initial Catalog={3};Persist Security Info=True;Connect Timeout=15",
                                          oObj.W_DATtServer, oObj.W_DATtUser, oObj.W_DATtPasswd, oObj.W_DATtDbName)
        Using oSQLConn As New SqlClient.SqlConnection(tConn)
            Try
                oSQLConn.Open()
                W_DATbChkDb = True
            Catch ex As Exception
            End Try
        End Using
    End Function


    'Save Xml file 
    Public Sub W_DATxSaveConfig()
        Try

            'เช็คว่ามีโฟลเดอไหม ถ้าไม่มีให้สร้าง
            If Directory.Exists(tAppPath & "\Config") = False Then
                Directory.CreateDirectory(tAppPath & "\Config")
            End If

            Dim oXEAdaConfig As XElement =
                  <config>
                      <Application User="EUrhNOJbKWw=" Pass="EUrhNOJbKWw=" Language="2"></Application>
                      <Connection Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></Connection>
                      <ConnectionMem Server="(local)\SQLEXPRESS" User="QSWVRqteAVo=" Password="foKZD3M9rwl1cFlJgFuA4w==" Catalog="master" TimeOut="500"></ConnectionMem>
                      <Separator Index="0" Define=""></Separator>
                  </config>

            Dim oAdaConfigXml As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXEAdaConfig)
            oAdaConfigXml.Save(tAppPath & "\Config\AdaConfig.xml")
            oAdaConfigXml = Nothing
            oXEAdaConfig = Nothing

            Dim oObj
            oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
            'Set ค่าให้กับ xml
            With AdaConfig.cConfig.oConnSource
                .tServer = oObj.W_DATtServer
                .tUser = oObj.W_DATtUser
                .tPassword = oObj.W_DATtPasswd
                .tCatalog = oObj.W_DATtDbName
                .nTimeOut = oObj.W_DATnTimeOut
            End With
            'Member Database *CH 18-10-2014
            With AdaConfig.cConfig.oMemConnSource
                .tServer = oObj.W_DATtMemServer
                .tUser = oObj.W_DATtMemUser
                .tPassword = oObj.W_DATtMemPasswd
                .tCatalog = oObj.W_DATtMemDbName
                .nTimeOut = oObj.W_DATnMemTimeOut
            End With
            AdaConfig.cConfig.C_SETbUpdateXml()

            'Mapping 
            'oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
            'With AdaConfig.cConfig.oSeparator
            '    .nIndex = oObj.W_GETnIndex
            '    .tDefine = oObj.W_GETtDefine
            'End With

            Dim oXeConnectAPI As XElement =
                   <Configurations>
                       <APIDetail Method="XXX" ContentType="XXX" ApplicationKey="" CONSUMER_KEY="" CONSUMER_SECRET="" TOKEN_SECRET=""></APIDetail>
                       <APIUrlSale URL="XXX"></APIUrlSale>
                       <APIUrlMember URL="XXX"></APIUrlMember>
                       <APIUrlMemberGrp URL="XXX"></APIUrlMemberGrp>
                       <ExportCstFirst Type="0"></ExportCstFirst>
                       <ExportGrpFirst Type="0"></ExportGrpFirst>
                       <ExportSaleFirst Type="0"></ExportSaleFirst>
                   </Configurations>

            Dim oAdaConfigAPI As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXeConnectAPI)
            oAdaConfigAPI.Save(tAppPath & "\Config\AdaConfigAPI.xml")
            oAdaConfigAPI = Nothing
            oAdaConfigAPI = Nothing

            Dim oConfigAPI = CType(Me.oW_ListCtl.Item(eType.Configuration), cUsrConfiguration)
            With AdaConfig.cConfig.oConfigAPI
                .tKey = oConfigAPI.W_DATtKey
                .tMethod = oConfigAPI.W_DATtMethod
                .tContenttype = oConfigAPI.W_DATtContentType
                .tUrlMember = oConfigAPI.W_DATtMember
                .tUrlGrpMember = oConfigAPI.W_DATtMemberGrp
                .tUrlSaleTran = oConfigAPI.W_DATtSaleTran
                .tExportCst = oConfigAPI.W_DATtStatusExpCst
                .tExportGrp = oConfigAPI.W_DATtStatusExpGrp
                .tExportSale = oConfigAPI.W_DATtStatusExpSale
                .tCusKey = oConfigAPI.W_DATtCusKey
                .tCusSecret = oConfigAPI.W_DATtCusSecret
                .tTokenSecret = oConfigAPI.W_DATtTokenSecret
            End With
            AdaConfig.cConfig.C_SETbUpdateXmlAPI()

        Catch ex As Exception
        End Try
    End Sub

    Public Sub W_DATxSaveConfigShareDiscount()
        Dim oXeDiscount As XElement =
              <Configurations>
                  <CompenSate ID="1" Detail="Compensate on retail price"></CompenSate>
                  <ShareDis ID="2" Detail="Share discount 50"></ShareDis>
                  <MarginOnnet ID="3" Detail="Margin on net sale  from Vendor Master"></MarginOnnet>
                  <MarginOnSale ID="4" Detail="New margin on net sale "></MarginOnSale>
                  <ShareDiscount30 ID="5" Detail="Share discount 30"></ShareDiscount30>
                  <ShareDiscount40 ID="6" Detail="Share discount 40"></ShareDiscount40>
                  <ShareDiscount40_1 ID="7" Detail="XXX"></ShareDiscount40_1>
                  <ShareDiscount40_2 ID="8" Detail="XXX"></ShareDiscount40_2>
                  <ShareDiscount40_3 ID="9" Detail="XXX"></ShareDiscount40_3>
                  <ShareDiscount40_4 ID="10" Detail="XXX"></ShareDiscount40_4>
                  <ShareDiscount40_5 ID="11" Detail="XXX"></ShareDiscount40_5>
              </Configurations>

        Dim oAdaConfigDis As New XDocument(New XDeclaration("1.0", "utf-8", "yes"), oXeDiscount)
        oAdaConfigDis.Save(tAppPath & "\Config\AdaConfigShareDiscount.xml")
        AdaConfig.cConfig.C_GETxSharingDiscount()
    End Sub

    Public Function W_DATbConnChage() As Boolean
        W_DATbConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oConnSource.tServer = oObj.W_DATtServer And AdaConfig.cConfig.oConnSource.tUser = oObj.W_DATtUser And AdaConfig.cConfig.oConnSource.tPassword = oObj.W_DATtPasswd And AdaConfig.cConfig.oConnSource.tCatalog = oObj.W_DATtDbName And AdaConfig.cConfig.oConnSource.nTimeOut = oObj.W_DATnTimeOut Then
            W_DATbConnChage = False
        End If
    End Function

    'Member Database '*CH 18-10-2014
    Public Function W_DATbMemConnChage() As Boolean
        W_DATbMemConnChage = True
        Dim oObj
        oObj = CType(Me.oW_ListCtl.Item(eType.Connection), cUsrConnection)
        If AdaConfig.cConfig.oMemConnSource.tServer = oObj.W_DATtMemServer And AdaConfig.cConfig.oMemConnSource.tUser = oObj.W_DATtMemUser And AdaConfig.cConfig.oMemConnSource.tPassword = oObj.W_DATtMemPasswd And AdaConfig.cConfig.oMemConnSource.tCatalog = oObj.W_DATtMemDbName And AdaConfig.cConfig.oMemConnSource.nTimeOut = oObj.W_DATnMemTimeOut Then
            W_DATbMemConnChage = False
        End If
    End Function

    ' Public Function W_GEToMapping() As DataTable
    ' Dim oObj
    '  oObj = CType(Me.oW_ListCtl.Item(eType.Other), cUsrMapping)
    'Return oObj.W_GEToMapping
    'End Function

End Class

