﻿Public Class cUsrSharingCode

    Dim oXmlCtgAPI As XElement
    Dim oDbTblShr As DataTable
    Dim tC_StatusRefresh As Boolean = False

    Private Sub cUsrSharingCode_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        GETxXmlBingGrid()
    End Sub

    Public Function C_GENoColumnShareDiscount() As DataTable
        Try

            Dim oDbtbl As New DataTable
            oDbtbl.Columns.Add("FTSharingID", GetType(String))
            oDbtbl.Columns.Add("FTSharingDetail", GetType(String))
            Return oDbtbl
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Sub GETxXmlBingGrid()
        Try

            oDbTblShr = New DataTable()
            oDbTblShr = C_GENoColumnShareDiscount()

            oXmlCtgAPI = XElement.Load(AdaConfig.cConfig.tAppPath & "\Config\AdaConfigShareDiscount.xml")

            Dim oCompenSate = From c In oXmlCtgAPI.Elements("CompenSate") Select c
            For Each oItem In oCompenSate
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            Dim oShareDis = From c In oXmlCtgAPI.Elements("ShareDis") Select c
            For Each oItem In oShareDis
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            Dim oMarginOnnet = From c In oXmlCtgAPI.Elements("MarginOnnet") Select c
            For Each oItem In oMarginOnnet
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            Dim oMarginOnSale = From c In oXmlCtgAPI.Elements("MarginOnSale") Select c
            For Each oItem In oMarginOnSale
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            Dim oShareDiscount30 = From c In oXmlCtgAPI.Elements("ShareDiscount30") Select c
            For Each oItem In oShareDiscount30
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            Dim oShareDiscount40 = From c In oXmlCtgAPI.Elements("ShareDiscount40") Select c
            For Each oItem In oShareDiscount40
                Dim oDatarow As DataRow = oDbTblShr.NewRow()
                oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                oDbTblShr.Rows.Add(oDatarow)
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม

            Dim oShareDiscount40_1 = From c In oXmlCtgAPI.Elements("ShareDiscount40_1") Select c
            For Each oItem In oShareDiscount40_1
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    Dim oDatarow As DataRow = oDbTblShr.NewRow()
                    oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                    oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                    oDbTblShr.Rows.Add(oDatarow)
                End If
            Next

            Dim oShareDiscount40_2 = From c In oXmlCtgAPI.Elements("ShareDiscount40_2") Select c
            For Each oItem In oShareDiscount40_2
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    Dim oDatarow As DataRow = oDbTblShr.NewRow()
                    oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                    oDatarow("FTSharingDetail") = oItem.Attribute("Detail").Value
                    oDbTblShr.Rows.Add(oDatarow)
                End If
            Next

            Dim oShareDiscount40_3 = From c In oXmlCtgAPI.Elements("ShareDiscount40_3") Select c
            For Each oItem In oShareDiscount40_3
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    Dim oDatarow As DataRow = oDbTblShr.NewRow()
                    oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                    oDatarow("FTSharingDetail") = oItem.Attribute("Destail").Value
                    oDbTblShr.Rows.Add(oDatarow)
                End If
            Next

            Dim oShareDiscount40_4 = From c In oXmlCtgAPI.Elements("ShareDiscount40_4") Select c
            For Each oItem In oShareDiscount40_4
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    Dim oDatarow As DataRow = oDbTblShr.NewRow()
                    oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                    oDatarow("FTSharingDetail") = oItem.Attribute("Destail").Value
                    oDbTblShr.Rows.Add(oDatarow)
                End If
            Next

            Dim oShareDiscount40_5 = From c In oXmlCtgAPI.Elements("ShareDiscount40_5") Select c
            For Each oItem In oShareDiscount40_5
                If oItem.Attribute("Detail").Value <> "XXX" Then
                    Dim oDatarow As DataRow = oDbTblShr.NewRow()
                    oDatarow("FTSharingID") = oItem.Attribute("ID").Value
                    oDatarow("FTSharingDetail") = oItem.Attribute("Destail").Value
                    oDbTblShr.Rows.Add(oDatarow)
                End If
            Next

            'สร้างไว้ ในกรณีที่ User มีการ Add รายละเอียดเข้ามาเพิ่ม
            ogdShare.DataSource = oDbTblShr

            'Set Header Style
            ogdShare.Columns("FTSharingID").HeaderText = "รหัส Share Discount"
            ogdShare.Columns("FTSharingID").Width = 150
            ogdShare.Columns("FTSharingDetail").HeaderText = "รายละเอียด"
            ogdShare.Columns("FTSharingDetail").AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            ogdShare.AllowUserToAddRows = False

            'Set ReadOnly
            ogdShare.Columns("FTSharingID").ReadOnly = True
            ogdShare.Columns("FTSharingDetail").ReadOnly = True
            ogdShare.RowHeadersVisible = False

            Dim obtn As New DataGridViewButtonColumn()

            If (tC_StatusRefresh = False) Then
                If ogdShare.Columns("แก้ไข") Is Nothing Then
                    ogdShare.Columns.Add(obtn)
                End If
                obtn.Width = 20
                obtn.Text = ""
                obtn.Name = ""
                obtn.UseColumnTextForButtonValue = True
            End If



        Catch ex As Exception

        End Try
    End Sub

    Private Sub btnInsert_Click(sender As Object, e As EventArgs) Handles btnInsert.Click
        cLog.C_CALxWriteLog("wInsertSharing")
        Dim oFrm As New wInsertSharing()
        Dim nRowCount As Integer = ogdShare.RowCount + 1
        oFrm.olaStatus.Text = "Insert"
        oFrm.otbCode.Text = nRowCount
        If oFrm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
            tC_StatusRefresh = True
            GETxXmlBingGrid()
            oFrm.Dispose()
        End If
    End Sub

    Private Sub ogdShare_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles ogdShare.CellContentClick
        Dim oFrm As New wInsertSharing()
        If e.ColumnIndex = 0 Then
            Dim tValue As String = ogdShare.Rows(e.RowIndex).Cells(1).Value
            Dim tDetail As String = ogdShare.Rows(e.RowIndex).Cells(2).Value
            cLog.C_CALxWriteLog("wInsertSharing")
            oFrm.olaStatus.Text = "Edit"
            oFrm.otbCode.Text = tValue
            oFrm.otbDetail.Text = tDetail

            If oFrm.ShowDialog(Me) = Windows.Forms.DialogResult.OK Then
                tC_StatusRefresh = True
                GETxXmlBingGrid()
                oFrm.Dispose()
            End If

        End If


    End Sub

    Private Sub ogdShare_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles ogdShare.CellPainting
        If e.ColumnIndex = 0 AndAlso e.RowIndex >= 0 Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)

            Dim img As Image = Image.FromFile(AdaConfig.cConfig.tAppPath & "\img\if_pen_1814074.ico")
            e.Graphics.DrawImage(img, CInt((e.CellBounds.Width / 2) - (img.Width / 2)) + e.CellBounds.X, CInt((e.CellBounds.Height / 2) - (img.Height / 2)) + e.CellBounds.Y)
            e.Handled = True
        End If
    End Sub
End Class
