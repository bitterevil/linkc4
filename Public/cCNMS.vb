﻿Public Class cCNMS

    'สงวน 001 -009
    Public Const tMS_CN001 = "เกิดความผิดพลาด:;Error occured:"
    Public Const tMS_CN002 = "หมายเลข:;No:"
    Public Const tMS_CN003 = "คุณต้องการออกจากระบบใช่หรือไม่?;Do you  want to exit system?"
    Public Const tMS_CN004 = "ไม่สามารถติดต่อกับฐานข้อมูลได้ กรุณาตรวจสอบฐานข้อมูล หรือติดต่อผู้ควบคุมระบบ;Can not connect the database, please check database service or contact Administrators."
    Public Const tMS_CN005 = "ข้อมูลไม่สมบูรณ์ กรุณาป้อนข้อมูลใหม่;Data is not complete. Please fill in properly."
    Public Const tMS_CN006 = "ไม่พบแฟ้มข้อมูล กรุณาติดต่อผู้ควบคุมระบบ;File not found. Please contact Administrator."

    Public Const tMS_CN010 = "กรุณาป้อนรายการข้อมูลให้ถูกต้อง;Please key in properly."
    Public Const tMS_CN011 = "ไม่พบข้อมูลผู้ใช้ !!! ;User not found !!!"

    'reserve system
    Public Const tMS_CN101 = "คุณต้องการบันทึกค่าเริ่มต้นใหม่ ใช่หรือไม่?;Do you want to save initial setting?"
    Public Const tMS_CN102 = "ไม่สามารถติดต่อฐานข้อมูลได้;Can not connect to database."
    Public Const tMS_CN103 = "ข้อมูลไม่สมบูรณ์ กรุณาใส่ใหม่;Data is not complete. Please fill in properly."
    Public Const tMS_CN104 = "ข้อมูลในการเชื่อมต่อฐานข้อมูลไม่ถูกต้อง" & vbCrLf & vbCrLf & "โปรดตรวจสอบอีกครั้ง!!;Database Connection Data incorrect " & vbCrLf & vbCrLf & "Please check again."
    Public Shared tMS_CN105 = "กรุณาเลือก ช่วงรหัสสาขา;Please select interval branch. "
    Public Shared tMS_CN106 = "กรุณาเลือก รหัสสาขา;Please select branch. "
    Public Const tMS_CN107 = "โปรแกรมนี้กำลังถูกใช้โดย {0}  " & vbCrLf & " คุณต้องการเข้าระบบอีกครั้งหรือไม่ ;This programe has been working by {0}.  " & vbCrLf & " Do you want to try again? "
    Public Const tMS_CN108 = "การตั้งค่าการเชื่อมต่อมีการเปลี่ยนแปลง " & vbCrLf & " คุณต้องการเริ่มโปรแกรมใหม่เพื่อบันทึกการเปลึ่ยนแปลงหรือไม่ ;Connection has been changed. " & vbCrLf & " Do you want to restart this program for save change? "
    Public Const tMS_CN109 = "ดำเนินการสำเร็จ;Successfully implemented."
    Public Const tMS_CN110 = "ไม่พบตาราง Temp กรุณาติดต่อผู้ควบคุมระบบ;Table not found. Please contact Administrator."
    Public Const tMS_CN111 = "ไม่อนุญาตใช้งาน;Out of service"
    Public Const tMS_CN112 = "มีเอกสารการขายไม่สมบูรณ์ กรุณาตรวจสอบจากประวัติ ({0})" & vbCrLf & vbCrLf & "คุณต้องการทำงานต่อหรือไม่(Y/N);Document is not complete, Please check from history of ({0})" & vbCrLf & vbCrLf & "Do you want to continue?(Y/N)"
    Public Const tMS_CN113 = "มีเอกสารการขายไม่สมบูรณ์ กรุณาตรวจสอบจากประวัติ;Document is not complete, Please check from history."
    Public Const tMS_CN114 = "ไม่มีอยู่ในฐานข้อมูล;Does not exist in the database."
    Public Const tMS_CN115 = "เป็นสินค้าเครื่องชั่ง;This is product scales."
    Public Const tMS_CN116 = "ไม่สามารถบันทึกข้อมูลได้;Can not save data."
    Public Const tMS_CN117 = "ซ้ำในฐานข้อมูล;Duplicate in the database."
    Public Const tMS_CN118 = "รูปแบบวันที่ผิด;Invalid format date."
    Public Const tMS_CN119 = "รูปแบบราคาผิด;Invalid format price"
    Public Const tMS_CN120 = "บาร์โค้ดซ้ำ;Duplicate barcode"
    Public Const tMS_CN121 = "ดำเนินการไม่สำเร็จ;implemented unsuccess."
    Public Const tMS_CN122 = "ไม่มีอยู่จริงในฐานข้อมูล;Does not exist in the database."
    Public Const tMS_CN123 = "ซ้ำกันในไฟล์;Duplicate in file."

    'Report 700-799
    Public Const tMS_CN701 = "ไม่ได้กำหนดรายงานในระบบ กรุณาติดต่อผู้ควบคุมระบบ;Report does not defined. Please contact Administrator."
    Public Const tMS_CN702 = "ไม่สามารถติดต่อฐานข้อมูลรายงานได้ กรุณาติดต่อผู้ควบคุมระบบ;Can not connect report database. Please contact Administrator."
    Public Const tMS_CN703 = "ไม่พบข้อมูล;Data not found."

End Class
