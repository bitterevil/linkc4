﻿Public Class QueryParameter
    Private tC_Name As String = Nothing
    Private tC_Value As String = Nothing

    Public Sub New(ByVal ptName As String, ByVal ptValue As String)
        Me.tC_Name = ptName
        Me.tC_Value = ptValue
    End Sub

    Public ReadOnly Property Name As String
        Get
            Return Me.tC_Name
        End Get
    End Property
    Public ReadOnly Property Value As String
        Get
            Return Me.tC_Value
        End Get
    End Property

End Class
